@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Data Keterangan Organisasi</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('ketorganisasi.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label>Nama Organisasi</label>
                    <input type="text" name="nama_organisasi" class="form-control" placeholder="Nama Organisasi"
                        required>
                </div>
                <div class="form-group">
                    <label>Jabatan</label>
                    <input type="text" name="jabatan" class="form-control" placeholder="Jabatan" required>
                </div>
                <div class="form-group">
                    <label>Tanggal Mulai</label>
                    <input type="date" name="tgl_mulai" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Tanggal Selesai</label>
                    <input type="date" name="tgl_selesai" class="form-control" required>
                </div>

                <div class="form-group">
                    <label>Tempat</label>
                    <input type="text" name="tempat" class="form-control" placeholder="Tempat Organisasi" required>
                </div>
                <div class="form-group">
                    <label>Pemimpin Organisasi</label>
                    <input type="text" name="pemimpin" class="form-control" placeholder="Pemimpin Organisasi" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection