@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Data Kepegawaian</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card bg-light">
                            <div class="card-body">
                                <h5 class="card-title">Pendidikan di dalam dan luar negeri</h5>
                                <p class="card-text">With supporting text below as a natural lead-in to additional
                                    content.</p>
                                <a href="{{route('pendformal.index')}}" class="btn btn-primary">Kelola Data</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="card bg-light">
                            <div class="card-body">
                                <h5 class="card-title">Kursus/ Latihan di dalam luar negeri</h5>
                                <p class="card-text">With supporting text below as a natural lead-in to additional
                                    content.</p>
                                <a href="{{route('pendinformal.index')}}" class="btn btn-primary">Kelola Data</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="card bg-light">
                            <div class="card-body">
                                <h5 class="card-title">Riwayat Pekerjaan</h5>
                                <p class="card-text">With supporting text below as a natural lead-in to additional
                                    content.</p>
                                <a href="{{route('riwayatpekerjaan.index')}}" class="btn btn-primary">Kelola Data</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="card bg-light">
                            <div class="card-body">
                                <h5 class="card-title">Tanda Jasa / Penghargaan</h5>
                                <p class="card-text">With supporting text below as a natural lead-in to additional
                                    content.</p>
                                <a href="{{route('penghargaan.index')}}" class="btn btn-primary">Kelola Data</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="card bg-light">
                            <div class="card-body">
                                <h5 class="card-title">Riwayat Keluarga</h5>
                                <p class="card-text">With supporting text below as a natural lead-in to additional
                                    content.</p>
                                <a href="{{route('riwayatkeluarga.index')}}" class="btn btn-primary">Kelola Data</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="card bg-light">
                            <div class="card-body">
                                <h5 class="card-title">Keterangan Organisasi</h5>
                                <p class="card-text">With supporting text below as a natural lead-in to additional
                                    content.</p>
                                <a href="{{route('ketorganisasi.index')}}" class="btn btn-primary">Kelola Data</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="card bg-light">
                            <div class="card-body">
                                <h5 class="card-title">Keterangan Lain-lain</h5>
                                <p class="card-text">With supporting text below as a natural lead-in to additional
                                    content.</p>
                                <a href="{{route('ketlain.index')}}" class="btn btn-primary">Kelola Data</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@endsection