<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\M_guru;
use App\Models\M_Siswa;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

use App\Models\User as ModelsUser;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 'guru') {
            $data = M_guru::where('id_user', Auth::user()->id)
                ->join('users', 'users.id', '=', 'guru.id_user')
                ->first();
            return view('guru.profileguru')->with([
                'data' => $data
            ]);
        }
        if (Auth::user()->role == 'siswa') {
            $data = M_siswa::where('id_user', Auth::user()->id)
                ->join('users', 'users.id', '=', 'siswa.id_user')
                ->first();
            return view('siswa.profilesiswa')->with([
                'data' => $data
            ]);
        }
        if (Auth::user()->role == 'admin') {
            return view('guru.profileadmin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except(['_token']);
        $request->validate([
            'foto' => 'mimes:png,jpeg,jpg|max:1024',
        ]);

        if (Auth::user()->role == 'guru') {
            $guru = M_guru::where('id_user', $id)->first();
            if ($request->hasFile('foto')) {
                $fileName = time() . '.' . $request->foto->extension();

                $res = $request->foto->move(public_path('uploads'), $fileName);
                if ($res) {
                    $data['foto'] = $fileName;
                }
            } else {
                $fileName = $guru->foto;
            }
            $dataGuru = [
                // 'id_user' => $user->id,
                'foto' => $fileName,
                'nama' => $data['nama'],
                'nip' => $data['nip'],
                'nik' => $data['nik'],
                'tmp_lahir' => $data['tmp_lahir'],
                'tgl_lahir' => $data['tgl_lahir'],
                'jk' => $data['jk'],
                'alamat' => $data['alamat'],
                'hobi' => $data['hobi'],
                'no_hp' => $data['no_hp'],
                'email' => $data['email'],
                'agama' => $data['agama']
            ];
            $guru->update($dataGuru);

            $user = User::where('id', $id)->first();
            $dataUser = [
                'name' => $data['nama'],
                'username' => $data['username']
                // 'password' => bcrypt($data['password'])
            ];
            $user->update($dataUser);
        }


        $data = $request->except(['_token']);
        if (Auth::user()->role == 'siswa') {
            $siswa = M_Siswa::where('id_user', $id)->first();
            if ($request->hasFile('foto')) {
                $fileName = time() . '.' . $request->foto->extension();

                $res = $request->foto->move(public_path('uploads'), $fileName);
                if ($res) {
                    $data['foto'] = $fileName;
                }
            } else {
                $fileName = $siswa->foto;
            }
            $dataSiswa = [
                // 'id_user' => $user->id,
                'foto' => $fileName,
                'nisn' => $data['nisn'],
                'email' => $data['email'],
                'nama' => $data['nama'],
                'tempat_lahir' => $data['tempat_lahir'],
                'tgl_lahir' => $data['tgl_lahir'],
                'jk' => $data['jk'],
                'agama' => $data['agama'],
                'anak_ke' => $data['anak_ke'],
                'jml_saudara' => $data['jml_saudara'],
                'akhir_pendidikan' => $data['akhir_pendidikan'],
                'asal_sekolah' => $data['asal_sekolah'],
                'thn_terima' => $data['thn_terima'],
                'penyakit_diderita' => $data['penyakit_diderita'],
                'alamat' => $data['alamat'],
                'kodepos' => $data['kodepos'],
            ];
            $siswa->update($dataSiswa);

            $user = User::where('id', $id)->first();
            $dataUser = [
                'name' => $data['nama'],
                'username' => $data['username']
                // 'password' => bcrypt($data['password'])
            ];
            $user->update($dataUser);
        }
        return redirect('/profile')->with(['success' => 'Data Berhasil Di Perbarui']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
