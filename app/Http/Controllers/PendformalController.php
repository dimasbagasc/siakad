<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\M_guru;
use App\Models\M_Pendformal;

class PendformalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guru = M_guru::where('id_user', Auth::user()->id)->first();
        $data = M_Pendformal::where('id_guru', $guru->id)->get();
        return view('guru.pendformal')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('guru.tambahpendformal');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $guru = M_guru::where('id_user', Auth::user()->id)->first();
        $data = $request->except(['_token']);
        $data['id_guru'] = $guru->id;
        M_Pendformal::insert($data);
        return redirect('/pendformal')->with(['success' => 'Data Berhasil Di Tambahkan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = M_Pendformal::findOrFail($id);
        return view('guru/editpendformal')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = M_Pendformal::findOrFail($id);
        $data = $request->except(['_token']);
        $item->update($data);
        return redirect('/pendformal')->with(['success' => 'Data Berhasil Di Perbarui']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = M_Pendformal::findOrFail($id);
        $item->delete();
        return redirect('/pendformal')->with(['success' => 'Data Berhasil Dihapus']);
    }
}
