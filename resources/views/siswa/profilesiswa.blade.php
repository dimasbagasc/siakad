@extends('layout.master')
@extends('layout.sidebarsiswa')

@section('container')


<section class="section">
    <br>
    <div class="section-header">
        <h1>Profile</h1>
    </div>
    <div class="section-body">
        <div class="row mt-sm-12">
            <div class="col-6 col-md-12">

                <form method="post" class="needs-validation"
                    action="{{route('profile.update', ['profile'=>$data->id_user])}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')


            </div>
            <div class="col-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit Profile</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-6 col-12">
                                <label>Email</label>
                                <input type="text" name="email" class="form-control" value="{{$data->email}}"
                                    required="">
                                <div class="invalid-feedback">
                                    Please fill in the first name
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-12">
                                <label>Username</label>
                                <input type="text" name="username" class="form-control" value="{{$data->username}}"
                                    required="">
                                <div class="invalid-feedback">
                                    Please fill in the last name
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>NISN</label>
                                    <input type="text" name="nisn" class="form-control" value="{{$data->nisn}}"
                                        required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama" class="form-control" value="{{$data->nama}}"
                                        required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tempat Lahir</label>
                                    <input type="text" name="tempat_lahir" class="form-control"
                                        value="{{$data->tempat_lahir}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="date" name="tgl_lahir" class="form-control"
                                        value="{{$data->tgl_lahir}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="d-block">Jenis Kelamin</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="jk" id="jk1"
                                            value="laki-laki" {{$data->jk == 'laki-laki' ? 'checked' : ''}}>
                                        <label class="form-check-label" for="jk1">
                                            Laki-laki
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="jk" id="jk2"
                                            value="perempuan" {{$data->jk == 'perempuan' ? 'checked' : ''}}>
                                        <label class="form-check-label" for="jk2">
                                            Perempuan
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Agama</label>
                                    <select name="agama" class="form-control">
                                        <option></option>
                                        <option value="islam" {{$data->agama == 'islam' ? 'selected' : ''}}>Islam
                                        </option>
                                        <option value="katholik" {{$data->agama == 'katholik' ? 'selected' :
                                            ''}}>Katholik</option>
                                        <option value="kristen" {{$data->agama == 'kristen' ? 'selected' :
                                            ''}}>Kristen</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea name="alamat" class="form-control" id="alamat">
                                            {{$data->alamat}}
                                        </textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kodepos</label>
                                    <input type="text" name="kodepos" class="form-control" value="{{$data->kodepos}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Anak Ke-</label>
                                    <input type="text" name="anak_ke" class="form-control" value="{{$data->anak_ke}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jumlah Saudara</label>
                                    <input type="text" name="jml_saudara" class="form-control"
                                        value="{{$data->jml_saudara}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Pendidikan Terakhir</label>
                                    <input type="text" name="akhir_pendidikan" class="form-control"
                                        value="{{$data->akhir_pendidikan}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Asal Sekolah</label>
                                    <input type="text" name="asal_sekolah" class="form-control"
                                        value="{{$data->asal_sekolah}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tahun diterima</label>
                                    <input type="text" name="thn_terima" class="form-control"
                                        value="{{$data->thn_terima}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Riwayat Penyakit</label>
                                    <input type="text" name="penyakit_diderita" class="form-control"
                                        value="{{$data->penyakit_diderita}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Foto</label>
                                    <input type="file" name="foto" class="form-control">
                                    <img alt="image" src="{{URL::to('/uploads').'/'.$data->foto}}"
                                        class="profile-widget-picture" style="width:35%">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div>
                        </form>
                    </div>
                </div>

            </div>
</section>



@endsection