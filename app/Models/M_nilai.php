<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_nilai extends Model
{
    use HasFactory;

    protected $table = 'nilai';
    protected $fillable = [
        'id_rombel',
        'id_gurumapel',
        'np',
        'nuts',
        'nuas',
        'bobot_np',
        'bobot_nuts',
        'bobot_uas',
        'nilai_praktik',
        'nilai_porto',
        'nilai_proyek',
        'bobot_praktik',
        'bobot_porto',
        'bobot_proyek',
        'nilai_sikapmapel'
    ];

    protected $hidden;
}
