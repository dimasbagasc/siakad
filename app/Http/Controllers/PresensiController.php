<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\M_guru;
use App\Models\M_Sesi;
use App\Models\M_Presensi;

class PresensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahunAktif = getTahunAktif();
        $guru = M_guru::where('id_user', Auth::user()->id)->first();
        $datajadwal = DB::table('jadwal')
            ->join('gurumapel', 'jadwal.id_guru', '=', 'gurumapel.id')
            ->join('sesi', 'jadwal.id_sesi', '=', 'sesi.id')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->join('angkatan', 'jadwal.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('jadwal.*', 'guru.nama', 'sesi.sesi', 'sesi.jam_mulai', 'sesi.jam_selesai', 'mapel.matapelajaran', 'kelas.nama_kelas')
            ->where('guru.id', '=', $guru->id)
            ->where('tahunajaran.id', $tahunAktif->id)
            ->where('jadwal.hari', '=', date('N'))
            ->orderBy('sesi.sesi', 'asc')
            ->orderBy('jadwal.hari', 'asc')
            ->get()
            ->toArray();

        $sesi = M_Sesi::all();

        // $jadwal = [];
        // foreach ($datajadwal as $key => $value) {
        //     $jadwal[$value->sesi][$value->hari][] = $datajadwal[$key];
        // }
        // echo '<pre>';
        // print_r($datajadwal);
        // exit;

        return view('guru.presensi')->with([
            'jadwal' => $datajadwal,
            'sesi' => $sesi,
            'tahunAktif' => $tahunAktif
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $datarombel = DB::table('rombel')
            ->join('siswa', 'rombel.id_siswa', '=', 'siswa.id')
            ->select('rombel.*', 'siswa.nama', 'siswa.nisn')
            ->where('rombel.id_angkatan', '=', $request['id_angkatan'])
            ->orderBy('siswa.nama', 'asc')
            ->get()
            ->toArray();

        $datapresensi = DB::table('presensi')
            ->select('presensi.*')
            ->where('presensi.id_jadwal', '=', $request['id_jadwal'])
            ->where('presensi.tanggal', '=', date('Y-m-d'))
            ->orderBy('presensi.id_rombel', 'asc')
            ->get()
            ->toArray();

        $presensi = [];
        foreach ($datapresensi as $dp) {
            $presensi[$dp->id_rombel] = [
                'status' => $dp->status,
                'keterangan' => $dp->keterangan
            ];
        }

        $guru = M_guru::where('id_user', Auth::user()->id)->first();
        $datajadwal = DB::table('jadwal')
            ->join('gurumapel', 'jadwal.id_guru', '=', 'gurumapel.id')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->join('angkatan', 'jadwal.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->select('jadwal.*', 'guru.nama', 'mapel.matapelajaran', 'kelas.nama_kelas')
            ->where('guru.id', '=', $guru->id)
            ->where('jadwal.id', '=', $request['id_jadwal'])
            ->where('jadwal.hari', '=', date('N'))
            ->first();

        // echo "<pre>";
        // print_r($datajadwal);
        // exit;

        return view('guru.inputpresensi')->with([
            'siswa' => $datarombel,
            'id_jadwal' => $request['id_jadwal'],
            'presensi' => $presensi,
            'jadwal' => $datajadwal
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);

        foreach ($data['presensi'] as $k => $v) {
            $presensi = [
                'id_rombel' => $k,
                'id_jadwal' => $data['id_jadwal'],
                'tanggal' => date('Y-m-d'),
                'keterangan' => $v['keterangan'],
                'status' => $v['status']
            ];

            $datapresensi = DB::table('presensi')
                ->select('presensi.*')
                ->where('presensi.id_rombel', '=', $k)
                ->where('presensi.id_jadwal', '=', $data['id_jadwal'])
                ->where('presensi.tanggal', '=', date('Y-m-d'))
                ->first();

            if (!empty($datapresensi)) {
                $item = M_Presensi::findOrFail($datapresensi->id);
                $item->update($presensi);
            } else {
                M_Presensi::insert($presensi);
            }
        }
        return redirect('/presensi')->with(['success' => 'Data Berhasil Di Simpan.']);
    }

    public function rekapharian(Request $request)
    {
        $tahunAktif = getTahunAktif();
        $data = $request->except(['_token']);

        $datapresensi = DB::table('presensi')
            ->select('presensi.*', 'siswa.nisn', 'siswa.nama')
            ->join('rombel', 'presensi.id_rombel', '=', 'rombel.id')
            ->join('siswa', 'rombel.id_siswa', '=', 'siswa.id')
            ->where('presensi.id_jadwal', '=', $data['id_jadwal'])
            ->where('presensi.tanggal', '=', date('Y-m-d'))
            ->get();

        $countpresensi = DB::table('presensi')
            ->selectRaw("COUNT(CASE WHEN status='hadir' THEN 1 END) AS hadir,COUNT(CASE WHEN status='alpha' THEN 1 END) AS alpha,COUNT(CASE WHEN status='ijin' THEN 1 END) AS ijin")
            ->where('presensi.id_jadwal', '=', $data['id_jadwal'])
            ->where('presensi.tanggal', '=', date('Y-m-d'))
            ->first();
        // echo "<pre>";
        // print_r($countpresensi);
        // exit;

        $guru = M_guru::where('id_user', Auth::user()->id)->first();
        $datajadwal = DB::table('jadwal')
            ->join('gurumapel', 'jadwal.id_guru', '=', 'gurumapel.id')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->join('angkatan', 'jadwal.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('jadwal.*', 'guru.nama', 'mapel.matapelajaran', 'kelas.nama_kelas')
            ->where('guru.id', '=', $guru->id)
            ->where('tahunajaran.id', $tahunAktif->id)
            ->where('jadwal.id', '=', $request['id_jadwal'])
            ->where('jadwal.hari', '=', date('N'))
            ->first();

        return view('guru.rekappresensi')->with([
            'id_jadwal' => $request['id_jadwal'],
            'presensi' => $datapresensi,
            'jadwal' => $datajadwal,
            'tahunAktif' => $tahunAktif,
            'countpresensi' => $countpresensi
        ]);
    }

    public function indexallrekap()
    {
        $tahunAktif = getTahunAktif();
        $guru = M_guru::where('id_user', Auth::user()->id)->first();
        $datajadwal = DB::table('jadwal')
            ->join('gurumapel', 'jadwal.id_guru', '=', 'gurumapel.id')
            ->join('sesi', 'jadwal.id_sesi', '=', 'sesi.id')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->join('angkatan', 'jadwal.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('jadwal.id_guru', 'jadwal.id_angkatan', 'guru.nama', 'mapel.matapelajaran', 'kelas.nama_kelas')
            ->where('guru.id', '=', $guru->id)
            ->where('tahunajaran.id', $tahunAktif->id)
            // ->where('jadwal.hari', '=', date('N'))
            ->orderBy('sesi.sesi', 'asc')
            ->orderBy('jadwal.hari', 'asc')
            ->groupBy('jadwal.id_guru', 'jadwal.id_angkatan', 'guru.nama', 'mapel.matapelajaran', 'kelas.nama_kelas')
            ->get()
            ->toArray();

        // echo '<pre>';
        // print_r($datajadwal);
        // exit;

        return view('guru.allrekappresensi')->with([
            'jadwal' => $datajadwal,
            'tahunAktif' => $tahunAktif
        ]);
    }

    public function allrekap(Request $request)
    {
        $tahunAktif = getTahunAktif();
        $data = $request->except(['_token']);

        $datapresensi = DB::table('presensi')
            ->select('presensi.id_rombel', 'siswa.nisn', 'siswa.nama', DB::raw("COUNT(CASE WHEN presensi.status='hadir' THEN 1 END) AS hadir,COUNT(CASE WHEN presensi.status='alpha' THEN 1 END) AS alpha,COUNT(CASE WHEN presensi.status='ijin' THEN 1 END) AS ijin"))
            ->join('rombel', 'presensi.id_rombel', '=', 'rombel.id')
            ->join('siswa', 'rombel.id_siswa', '=', 'siswa.id')
            ->join('jadwal', 'presensi.id_jadwal', '=', 'jadwal.id')
            ->where('jadwal.id_guru', '=', $data['id_guru'])
            ->where('jadwal.id_angkatan', '=', $data['id_angkatan'])
            ->groupBy('presensi.id_rombel', 'siswa.nisn', 'siswa.nama')
            ->get();

        // echo "<pre>";
        // print_r($datapresensi);
        // exit;

        $countpresensi = DB::table('presensi')
            ->selectRaw("COUNT(CASE WHEN status='hadir' THEN 1 END) AS hadir,COUNT(CASE WHEN status='alpha' THEN 1 END) AS alpha,COUNT(CASE WHEN status='ijin' THEN 1 END) AS ijin")
            ->join('jadwal', 'presensi.id_jadwal', '=', 'jadwal.id')
            ->where('jadwal.id_guru', '=', $data['id_guru'])
            ->where('jadwal.id_angkatan', '=', $data['id_angkatan'])
            ->first();

        // $guru = M_guru::where('id_user', Auth::user()->id)->first();
        $datajadwal = DB::table('jadwal')
            ->join('gurumapel', 'jadwal.id_guru', '=', 'gurumapel.id')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->join('angkatan', 'jadwal.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('jadwal.*', 'guru.nama', 'mapel.matapelajaran', 'kelas.nama_kelas')
            ->where('jadwal.id_guru', '=', $data['id_guru'])
            ->where('jadwal.id_angkatan', '=', $request['id_angkatan'])
            ->where('tahunajaran.id', $tahunAktif->id)
            // ->where('jadwal.hari', '=', date('N'))
            ->first();

        // echo "<pre>";
        // print_r($datajadwal);
        // exit;

        return view('guru.dataallrekap')->with([
            'id_jadwal' => $request['id_jadwal'],
            'presensi' => $datapresensi,
            'jadwal' => $datajadwal,
            'tahunAktif' => $tahunAktif,
            'countpresensi' => $countpresensi
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
