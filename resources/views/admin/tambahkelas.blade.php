@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
  <br>
  <div class="section-header">
    <h1>Tambah Data Kelas</h1>
  </div>

  <div class="card">
    <div class="card-body">
      <form action="{{route('kelas.store')}}" method="POST">
        @csrf
        <div class="form-group">
          <label>Nama Kelas</label>
          <input type="text" name="nama_kelas" class="form-control" placeholder="Nama Kelas" required>
        </div>
        <div class="form-group">
          <a href="{{route('kelas.index')}}" class="btn btn-icon icon-left btn-warning">Kembali</a>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</section>

@endsection