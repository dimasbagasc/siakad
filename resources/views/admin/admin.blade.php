@extends('layout.master')
@extends('layout.sidebar')

@section('container')
<section class="section">
  <br>
  <div class="section-header">
    <h1>Dashboard</h1>
  </div>
  @if(session('error_akses'))
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{session('error_akses')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  @endif

  <div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-primary">
          <i class="far fa-user"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Total Guru</h4>
          </div>
          <div class="card-body">
            {{$guru}}
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-danger">
          <i class="fas fa-user-graduate"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Total Siswa</h4>
          </div>
          <div class="card-body">
            {{$siswa}}
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-warning">
          <i class="fas fa-users"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Total Kelas</h4>
          </div>
          <div class="card-body">
            {{$kelas}}
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-success">
          <i class="fas fa-bookmark"></i>
          {{-- <i class="fas fa-circle"><a href="{{route('mapel.index')}}"></a></i> --}}
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Total Mapel</h4>
          </div>
          <div class="card-body">
            {{$mapel}}
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <h4>Grafik Penerimaan Siswa per Tahun</h4>
        </div>
        <div class="card-body">
          <canvas id="chartSiswa" width="400" height="100"></canvas>
        </div>
      </div>
    </div>
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <h4>Grafik Jumlah Laki-laki dan Perempuan</h4>
        </div>
        <div class="card-body">
          <canvas id="chartjk" width="400" height="100"></canvas>
        </div>
      </div>
    </div>
  </div>

  @endsection

  @push('js_script')
  <script>
    $(document).ready(function(){
      var siswa = <?= $countsiswa ?>;
      var jmlSiswa = [];
      var labelSiswa = [];
      for (var i in siswa) {
        labelSiswa.push(siswa[i].thn_terima);
        jmlSiswa.push(siswa[i].jml);
      }

      var ctx = document.getElementById('chartSiswa').getContext('2d');
      var chartSiswa = new Chart(ctx, {
      type: 'bar',
      data: {
      labels: labelSiswa,
      datasets: [{
      label: '# of Jumlah Penerimaan Siswa Per Tahun',
      data: jmlSiswa,
      backgroundColor: [
      'rgba(255, 99, 132, 0.2)',
      'rgba(54, 162, 235, 0.2)',
      'rgba(255, 206, 86, 0.2)',
      'rgba(75, 192, 192, 0.2)',
      'rgba(153, 102, 255, 0.2)',
      'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
      'rgba(255, 99, 132, 1)',
      'rgba(54, 162, 235, 1)',
      'rgba(255, 206, 86, 1)',
      'rgba(75, 192, 192, 1)',
      'rgba(153, 102, 255, 1)',
      'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
      }]
      },
      options: {
      scales: {
      yAxes: [{
      ticks: {
      beginAtZero: true
      }
      }]
      }
      }
      });

      var jk = <?= $countjk ?>;
      var jmljk = [];
      var labeljk = [];
      for (var i in jk) {
        labeljk.push(jk[i].jk);
        jmljk.push(jk[i].jml);
      }

      var ctx = document.getElementById('chartjk').getContext('2d');
      var chartjk = new Chart(ctx, {
      type: 'pie',
      data: {
      labels: labeljk,
      datasets: [{
      label: '# of Jumlah siswa laki-laki dan perempuan ',
      data: jmljk,
      backgroundColor: [
      'rgba(255, 99, 132, 0.2)',
      'rgba(54, 162, 235, 0.2)',
      'rgba(255, 206, 86, 0.2)',
      'rgba(75, 192, 192, 0.2)',
      'rgba(153, 102, 255, 0.2)',
      'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
      'rgba(255, 99, 132, 1)',
      'rgba(54, 162, 235, 1)',
      'rgba(255, 206, 86, 1)',
      'rgba(75, 192, 192, 1)',
      'rgba(153, 102, 255, 1)',
      'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
      }]
      },
      options: {
      scales: {
      yAxes: [{
      ticks: {
      beginAtZero: true
      }
      }]
      }
      }
      });
  })
  </script>

  @endpush