@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Pendidikan Informal</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('kepegawaian.index')}}" class="btn btn-warning fas fa-angle-left"> Kembali</a>
                <a href="{{route('pendinformal.create')}}" class="btn btn-primary fa fa-plus"
                    style="position: absolute; right:25px;"> Tambah Data</a>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Kursus / Latihan</th>
                                <th>Nomor Sertifikat</th>
                                <th>Tempat</th>
                                <th>Institusi Penyelenggara</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($data as $informal)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$informal->nama_kursus }}</td>
                                <td>{{$informal->nomor_kursus}}</td>
                                <td>{{$informal->tempat}}</td>
                                <td>{{$informal->institusi}}</td>
                                <td>
                                    <form method="POST"
                                        action="{{route('pendinformal.destroy',['pendinformal'=>$informal->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{route('pendinformal.edit',['pendinformal'=>$informal->id])}}"
                                            class="btn btn-icon btn-left btn-warning"><i class="far fa-edit"></i>
                                            Edit</a>
                                        <button type="submit" class="btn btn-icon btn-left btn-danger"><i
                                                class="fas fa-times"></i>
                                            Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>



@endsection