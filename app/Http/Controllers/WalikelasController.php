<?php

namespace App\Http\Controllers;

use App\Models\M_Angkatan;
use App\Models\M_guru;
use App\Models\M_kelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\M_Walikelas;

class WalikelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = DB::table('walikelas')
            ->join('guru', 'walikelas.id_guru', '=', 'guru.id')
            ->join('angkatan', 'walikelas.id_angkatan', '=', 'angkatan.id')
            ->select('walikelas.*', 'angkatan.id_kelas', 'guru.nama')
            ->where('walikelas.id_angkatan', $request['id_angkatan'])
            ->get();

        return view('admin.datawalikelas')->with([
            'data' => $data,
            'id_angkatan' => $request['id_angkatan']
        ]);
        // return view('admin.datawalikelas')->with([
        //     'id_angkatan'=>$request['id_angkatan']
        // ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tahunAktif = getTahunAktif();
        $angkatan = DB::table('angkatan')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('angkatan.*', 'kelas.nama_kelas', 'tahunajaran.tahun')
            ->where('angkatan.id', $request['id_angkatan'])
            ->where('tahunajaran.id', $tahunAktif->id)
            ->get();

        $walikelas = DB::table('walikelas')
            ->join('angkatan', 'angkatan.id', '=', 'walikelas.id_angkatan')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('walikelas.*')
            // ->where('angkatan.id', $request['id_angkatan'])
            ->where('tahunajaran.id', $tahunAktif->id)
            ->get();
        // echo '<pre>';
        // print_r($walikelas);
        // exit;
        $array_wk = [];
        foreach ($walikelas as $wk) {
            array_push($array_wk, $wk->id_guru);
        }

        $guru = DB::table('guru')
            ->whereNotIn('id', $array_wk)
            ->select('*')
            ->get();
        // echo "<pre>";
        // print_r($array_wk);
        // exit;
        return view('admin.tambahwalikelas')->with([
            'id_guru' => $guru,
            'id_angkatan' => $angkatan,
            'angkatan' => $request['id_angkatan']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);

        $cekData = DB::table('walikelas')
            ->where('id_angkatan', $data['id_angkatan'])
            ->where('status', 'aktif')
            ->get();

        // echo '<pre>';
        // print_r($cekData);
        // exit;

        if (empty($cekData->first())) {
            M_Walikelas::insert($data);
            return redirect('/walikelas?id_angkatan=' . $data['id_angkatan'])->with(['success' => 'Data Berhasil Diproses.']);
        } else {
            return redirect('/walikelas?id_angkatan=' . $data['id_angkatan'])->with(['warning' => 'Data Guru walikelas sudah ada!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $data = M_Walikelas::findOrFail($id);
        // $kelas = M_kelas::findOrfail($id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = M_Walikelas::findOrFail($id);
        $item->delete();
        return redirect('/walikelas')->with(['success' => 'Data Berhasil Di Hapus']);
    }
}
