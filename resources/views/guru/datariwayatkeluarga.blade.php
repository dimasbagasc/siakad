@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Riwayat Keluarga</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">

                <a href="{{route('kepegawaian.index')}}" class="btn btn-warning fas fa-angle-left"> Kembali</a>
                <a href="{{route('riwayatkeluarga.create')}}" class="btn btn-primary fa fa-plus"
                    style="position: absolute; right:25px;"> Tambah Data</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>Pekerjaan</th>
                                <th>Perusahaan</th>
                                <th>Status Hidup</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($data as $rk)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$rk->nik }}</td>
                                <td>{{$rk->nama}}</td>
                                <td>{{$rk->tempat}}</td>
                                <td>{{$rk->tgl}}</td>
                                <td>{{$rk->pekerjaan}}</td>
                                <td>{{$rk->perusahaan}}</td>
                                <td>{{$rk->status_hidup}}</td>
                                <td>
                                    <form method="POST"
                                        action="{{route('riwayatkeluarga.destroy',['riwayatkeluarga'=>$rk->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{route('riwayatkeluarga.edit',['riwayatkeluarga'=>$rk->id])}}"
                                            class="btn btn-icon btn-left btn-warning"><i class="far fa-edit"></i>
                                            Edit</a>
                                        <button type="submit" class="btn btn-icon btn-left btn-danger"><i
                                                class="fas fa-times"></i>
                                            Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>



@endsection