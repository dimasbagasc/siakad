@extends('layout.master')
@extends('layout.sidebar')

@section('container')

@php
$hari = getHari();
@endphp

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Jadwal</h1>
    </div>

    <div class="card">
        <div class="card-body">
            @if(session('error_jadwal'))
            <div class="alert alert-danger" role="alert">
                {{ session('error_jadwal') }}
            </div>
            @endif

            <form action="{{route('jadwal.store')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="d-block" for="id_tahunajaran">Tahun Ajaran</label>
                            <input type="text" class="form-control"
                                value="<?= $angkatan->tahun.'('.$angkatan->semester.')' ?>" readonly />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="d-block" for="id_kelas">Kelas</label>
                            <input type="text" class="form-control" value="<?= $angkatan->nama_kelas ?>" readonly />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="d-block" for="id_sesi">Sesi</label>
                            <select class="form-control select2_dropdown" name="id_sesi" id="id_sesi" required>
                                <option></option>
                                @foreach ($sesi as $s)
                                <option value="{{ $s->id }}">Sesi {{ $s->sesi . ' | ' . $s->jam_mulai .
                                    '-'.$s->jam_selesai
                                    }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="d-block" for="hari">Hari</label>
                            <select class="form-control select2_dropdown" name="hari" id="hari" required>
                                <option></option>
                                @foreach ($hari as $k => $h)
                                <option value="{{ $k }}">{{ $h }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="d-block" for="id_guru">Mapel - Guru</label>
                            <select class="form-control select2_dropdown" name="id_guru" id="id_guru" required>
                                <option></option>
                                @foreach ($gurumapel as $gm)
                                <option value="{{ $gm->id }}">{{ $gm->gurumapel }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="id_angkatan" class="form-control" value="<?= $angkatan->id ?>" />
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

                <hr>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-md datatable_table">
                            <thead>
                                <tr>
                                    <th>Sesi</th>
                                    <th>Waktu</th>
                                    <th>Senin</th>
                                    <th>Selasa</th>
                                    <th>Rabu</th>
                                    <th>Kamis</th>
                                    <th>Jumat</th>
                                    <th>Sabtu</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sesi as $s)
                                <tr>
                                    <td>{{ $s->sesi }}</td>
                                    <td>{{ $s->jam_mulai.'-'.$s->jam_selesai }}</td>
                                    @for ($i = 1; $i < 7; $i++) @if (isset($jadwal[$s->sesi]))
                                        @if (isset($jadwal[$s->sesi][$i]))
                                        <td>{{$jadwal[$s->sesi][$i][0]->nama.' |
                                            '.$jadwal[$s->sesi][$i][0]->matapelajaran.'|'}}
                                            <a style="color:black"
                                                href="{{ route('jadwal.hapus', ['id' =>$jadwal[$s->sesi][$i][0]->id]) }}">
                                                delete
                                            </a>

                                        </td>
                                        @else
                                        <td></td>
                                        @endif
                                        @else
                                        <td></td>
                                        @endif
                                        @endfor
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            </form>
        </div>
    </div>
</section>

@endsection