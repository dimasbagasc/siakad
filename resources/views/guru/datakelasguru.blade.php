@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Kelas {{$angkatan[0]->nama_kelas.' '.$angkatan[0]->tahun.'('.$angkatan[0]->semester.')'}}</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>NISN</th>
                                <th>Nama</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($angkatan as $a)
                            <tr>
                                <td>{{ $a->nisn }}</td>
                                <td>{{ $a->nama }}</td>
                                <td>
                                    <a href="{{route('datanilaisiswa.show',['datanilaisiswa'=>$a->id_siswa])}}"
                                        class="btn btn-icon btn-left btn-info"><i class="fas fa-info-circle"></i>
                                        Nilai
                                    </a>
                                    <a href="{{route('infokontak.show',['infokontak'=>$a->id_siswa])}}"
                                        class="btn btn-icon btn-left btn-primary"><i class="fas fa-eye"></i>
                                        Detail
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>



@endsection