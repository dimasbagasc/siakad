@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Angkatan</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('angkatan.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label class="d-block" for="id_tahunajaran">Tahun Ajaran</label>
                    <select class="form-control select2_dropdown" name="id_tahunajaran" id="id_tahunajaran" required>
                        <option></option>
                        @foreach ($tahunAjaran as $v)
                        <option value="{{ $v->id }}">{{ $v->tahun.'('.$v->semester.')' }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="d-block" for="id_kelas">Kelas</label>
                    <select class="form-control select2_dropdown" name="id_kelas" id="id_kelas" required>
                        <option></option>
                        @foreach ($kelas as $k)
                        <option value="{{ $k->id }}">{{ $k->nama_kelas }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <a href="{{route('angkatan.index')}}" class="btn btn-icon icon-left btn-warning">Kembali</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection