<?php

namespace App\Http\Controllers;

use App\Models\M_Deskripsi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\M_guru;
use App\Models\M_Matapelajaran;
use App\Models\M_Siswa;
use App\Models\M_Sesi;
use App\Models\M_nilai;
use App\Models\M_Rombel;

class NilaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahunAktif = getTahunAktif();
        $guru = M_guru::where('id_user', Auth::user()->id)->first();
        $datajadwal = DB::table('jadwal')
            ->join('gurumapel', 'jadwal.id_guru', '=', 'gurumapel.id')
            ->join('sesi', 'jadwal.id_sesi', '=', 'sesi.id')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->join('angkatan', 'jadwal.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('jadwal.id_guru', 'jadwal.id_angkatan', 'guru.nama', 'mapel.matapelajaran', 'kelas.nama_kelas')
            ->where('guru.id', '=', $guru->id)
            ->where('tahunajaran.id', $tahunAktif->id)
            // ->where('jadwal.hari', '=', date('N'))
            ->orderBy('sesi.sesi', 'asc')
            ->orderBy('jadwal.hari', 'asc')
            ->groupBy('jadwal.id_guru', 'jadwal.id_angkatan', 'guru.nama', 'mapel.matapelajaran', 'kelas.nama_kelas')
            ->get()
            ->toArray();

        $sesi = M_Sesi::all();


        return view('guru.nilai')->with([
            'jadwal' => $datajadwal,
            'sesi' => $sesi,
            'tahunAktif' => $tahunAktif
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $datarombel = DB::table('rombel')
            ->join('siswa', 'rombel.id_siswa', '=', 'siswa.id')
            ->select('rombel.*', 'siswa.nama', 'siswa.nisn')
            ->where('rombel.id_angkatan', '=', $request['id_angkatan'])
            ->orderBy('siswa.nama', 'asc')
            ->get()
            ->toArray();

        $datanilai = DB::table('nilai')
            ->select('nilai.*')
            ->where('nilai.id_gurumapel', '=', $request['id_gurumapel'])
            ->orderBy('nilai.id_rombel', 'asc')
            ->get()
            ->toArray();

        // echo '<pre>';
        // print_r($request);
        // exit;
        $bobot_np = 0;
        $bobot_nuts = 0;
        $bobot_nuas = 0;
        $bobot_praktik = 0;
        $bobot_porto = 0;
        $bobot_proyek = 0;

        $nilai = [];
        foreach ($datanilai as $dn) {
            $nilai[$dn->id_rombel] = [
                'np' => $dn->np,
                'bobot_np' => $dn->bobot_np,
                'nuts' => $dn->nuts,
                'bobot_nuts' => $dn->bobot_nuts,
                'nuas' => $dn->nuas,
                'bobot_nuas' => $dn->bobot_nuas,
                'nilai_praktik' => $dn->nilai_praktik,
                'bobot_praktik' => $dn->bobot_praktik,
                'nilai_porto' => $dn->nilai_porto,
                'bobot_porto' => $dn->bobot_porto,
                'nilai_proyek' => $dn->nilai_proyek,
                'bobot_proyek' => $dn->bobot_proyek,
                'nilai_sikapmapel' => $dn->nilai_sikapmapel
                // 'keterangan' => $dn->keterangan

            ];
            $bobot_np = $dn->bobot_np;
            $bobot_nuts = $dn->bobot_nuts;
            $bobot_nuas = $dn->bobot_nuas;
            $bobot_praktik = $dn->bobot_praktik;
            $bobot_porto = $dn->bobot_porto;
            $bobot_proyek = $dn->bobot_proyek;
        }

        // echo "<pre>";
        // print_r($bobot_np);
        // exit;

        $datajadwal = DB::table('jadwal')
            ->join('gurumapel', 'jadwal.id_guru', '=', 'gurumapel.id')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->join('angkatan', 'jadwal.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->select('jadwal.*', 'guru.nama', 'mapel.matapelajaran', 'kelas.nama_kelas')
            ->where('jadwal.id_guru', '=', $request['id_gurumapel'])
            ->where('jadwal.id_angkatan', '=', $request['id_angkatan'])
            // ->where('jadwal.hari', '=', date('N'))
            ->first();

        // echo "<pre>";
        // print_r($datajadwal);
        // exit;

        return view('guru.inputnilai')->with([
            'siswa' => $datarombel,
            'id_gurumapel' => $request['id_gurumapel'],
            'jadwal' => $datajadwal,
            'nilai' => $nilai,
            'bobot_np' => $bobot_np,
            'bobot_nuts' => $bobot_nuts,
            'bobot_nuas' => $bobot_nuas,
            'bobot_praktik' => $bobot_praktik,
            'bobot_porto' => $bobot_porto,
            'bobot_proyek' => $bobot_proyek
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);

        $jumlah_bobot_pengetahuan = $data['bobot_np'] + $data['bobot_nuts'] + $data['bobot_nuas'];
        if ($jumlah_bobot_pengetahuan != 4) {
            return redirect('/nilai')->with(['error' => 'Jumlah Bobot Kurang Dari atau Lebih Dari 4.']);
        }

        foreach ($data['nilai'] as $k => $v) {
            $nilai = [
                'id_rombel' => $k,
                'id_gurumapel' => $data['id_gurumapel'],
                'np' => $v['np'],
                'bobot_np' => $data['bobot_np'],
                'nuts' => $v['nuts'],
                'bobot_nuts' => $data['bobot_nuts'],
                'nuas' => $v['nuas'],
                'bobot_nuas' => $data['bobot_nuas'],
                'nilai_praktik' => $v['nilai_praktik'],
                'bobot_praktik' => $data['bobot_praktik'],
                'nilai_porto' => $v['nilai_porto'],
                'bobot_porto' => $data['bobot_porto'],
                'nilai_proyek' => $v['nilai_proyek'],
                'bobot_proyek' => $data['bobot_proyek'],
                'nilai_sikapmapel' => $v['nilai_sikapmapel']
                // 'keterangan' => $v['keterangan']
            ];

            $datanilai = DB::table('nilai')
                ->select('nilai.*')
                ->where('nilai.id_rombel', '=', $k)
                ->where('nilai.id_gurumapel', '=', $data['id_gurumapel'])
                ->first();

            if (!empty($datanilai)) {
                $item = M_nilai::findOrFail($datanilai->id);
                $item->update($nilai);
            } else {
                M_nilai::insert($nilai);
            }
        }
        return redirect('/nilai')->with(['success' => 'Data Berhasil Di Simpan.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tahunAktif = getTahunAktif();
        $datarombel = DB::table('rombel')
            ->join('angkatan', 'rombel.id_angkatan', '=', 'angkatan.id')
            ->join('siswa', 'rombel.id_siswa', '=', 'siswa.id')
            ->select('rombel.*', 'siswa.nama', 'siswa.nisn')
            ->where('angkatan.id_tahunajaran', '=', $tahunAktif->id)
            ->where('rombel.id_siswa', '=', $id)
            ->orderBy('siswa.nama', 'asc')
            ->first();

        $datanilai = DB::table('nilai')
            ->join('gurumapel', 'nilai.id_gurumapel', '=', 'gurumapel.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->join('rombel', 'nilai.id_rombel', '=', 'rombel.id')
            ->join('angkatan', 'rombel.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('mapel.matapelajaran', 'nilai.nuts', 'nilai.nuas')
            ->where('rombel.id_siswa', '=', $datarombel->id_siswa)
            ->where('tahunajaran.id', $tahunAktif->id)
            ->get();

        $datadeskripsi = DB::table('deskripsi')
            ->select('deskripsi.*')
            ->where('deskripsi.id_rombel', '=', $datarombel->id)
            ->first();

        // 

        //dd($datarombel->id);
        // $datanilai = M_nilai::where('id_rombel', $data->id)->get();
        return view('guru.datanilaisiswa')->with([
            'data' => $datarombel,
            'nilai' => $datanilai,
            'deskripsi' => $datadeskripsi
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // public function create_deskripsi()
    // {

    // }

    public function deskripsi(Request $request)
    {

        $data = $request->except(['_token']);

        $datadeskripsi = DB::table('deskripsi')
            ->select('deskripsi.*')
            ->where('deskripsi.id_rombel', '=', $data['id_rombel'])
            // ->where('nilai.id_gurumapel', '=', $data['id_gurumapel'])
            ->first();

        // echo"<pre>";
        // print_r($datadeskripsi);
        // exit;

        if (!empty($datadeskripsi)) {
            $item = M_Deskripsi::findOrFail($datadeskripsi->id);
            $item->update($data);
        } else {
            M_Deskripsi::insert($data);
        }
        return redirect('/kelasguru')->with(['success' => 'Data Berhasil Di Simpan.']);
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
