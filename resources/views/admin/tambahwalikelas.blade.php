@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Guru Walikelas</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('walikelas.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label class="d-block" for="id_mapel">Kelas/Angkatan</label>
                    <select class="form-control" name="id_angkatan" id="id_angkatan" required readonly />
                    {{-- <option></option> --}}
                    @foreach ($id_angkatan as $k)
                    <option value="{{ $k->id }}">{{ $k->nama_kelas . ' - ' . $k->tahun }}</option>
                    @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="d-block" for="id_guru">Guru</label>
                    <select class="form-control select2_dropdown" name="id_guru" id="id_guru" required>
                        <option></option>
                        @foreach ($id_guru as $v)
                        <option value="{{ $v->id }}">{{ $v->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="d-block" for="status">status</label>
                    <select class="form-control select2_dropdown" name="status" id="status" required>
                        <option></option>
                        <option value="aktif">aktif</option>
                        <option value="nonaktif">non aktif</option>
                    </select>
                </div>
                <div class="form-group">
                    <a href="{{route('walikelas.index',['id_angkatan'=>$angkatan])}}"
                        class="btn btn-icon icon-left btn-warning"> Kembali</a>
                    <button type="submit" class="btn btn-primary"> Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection