<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\M_kelas;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\TryCatch;

class DatakelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = M_kelas::orderBy('nama_kelas', 'desc')->get();
        return view('admin.datakelas')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tambahkelas');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);
        $cek = DB::table('kelas')->where('nama_kelas', 'like', '%' . $data['nama_kelas'] . '%')
            ->count();
        if ($cek > 0) {
            return redirect('/kelas')->with(['error' => 'Data Sudah Ada.']);
        }
        M_kelas::insert($data);
        return redirect('/kelas')->with(['success' => 'Data Berhasil Di Tambahkan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = M_kelas::findOrFail($id);
        return view('admin/showkelas')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = M_kelas::findOrFail($id);
        return view('admin/editkelas')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = M_kelas::findOrFail($id);
        $data = $request->except(['_token']);
        $item->update($data);
        return redirect('/kelas')->with(['success' => 'Data Berhasil Di Perbarui']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $item = M_kelas::findOrFail($id);
            $item->delete();
            return redirect('/kelas')->with(['success' => 'Data Berhasil Di Hapus']);
        } catch (\Throwable $th) {
            return redirect('/kelas')->with(['error' => 'Data Kelas Sudah Digunakan.']);
        }
    }
}
