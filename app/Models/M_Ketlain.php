<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Ketlain extends Model
{
    use HasFactory;

    protected $table = 'keterangan_lain';
    protected $fillable = [
        'id_guru',
        'nama_keterangan',
        'nomor_sk',
        'tgl_sk',
        'pejabat'
    ];
}
