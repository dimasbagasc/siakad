<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>SIAKAD &mdash; SMP</title>

  @stack('before-style')
  @include('includes.style')
  @stack('after-style')

</head>
@php
use App\Models\M_guru;
use App\Models\M_siswa;
@endphp

<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i
                  class="fas fa-search"></i></a></li>
          </ul>

        </form>
        <ul class="navbar-nav navbar-right">

          <li class="dropdown"><a href="#" data-toggle="dropdown"
              class="nav-link dropdown-toggle nav-link-lg nav-link-user">
              @php
              if (Auth::user()->role=='guru') {
              $data=M_guru::where('id_user',Auth::user()->id)->first();
              $foto=URL::to('/uploads').'/'.$data->foto;
              }
              if (Auth::user()->role=='siswa') {
              $data=M_siswa::where('id_user',Auth::user()->id)->first();
              $foto=URL::to('/uploads').'/'.$data->foto;
              }
              if (Auth::user()->role=='admin') {

              $foto=asset('template/assets/img/avatar/avatar-1.png');
              }
              @endphp
              <img alt="image" src="{{$foto}}" class="rounded-circle mr-1">
              <div class="d-sm-none d-lg-inline-block">{{auth()->user ()->name}}</div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-title">Logged in 5 min ago</div>
              <a href="{{route('profile.index')}}" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <a href="" class="dropdown-item has-icon">
                <i class="fas fa-key"></i> Lupa Password
              </a>
              <div class="dropdown-divider"></div>
              <a href="{{url('logout')}}" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar">
        @yield('sidebar')
      </div>

      <!-- Main Content -->
      <div class="main-content">
        @yield('container')
      </div>
    </div>
  </div>

  @stack('before-script')
  @include('includes.script')
  @stack('after-script')
</body>

</html>