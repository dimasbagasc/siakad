<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Peninformal extends Model
{
    use HasFactory;

    protected $table = 'pend_informal';
    protected $fillable = [
        'id_guru',
        'nama_kursus',
        'tgl_mulai',
        'tgl_selesai',
        'nomor_kursus',
        'tempat',
        'institusi'
    ];
}
