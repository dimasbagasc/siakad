<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_user');
            $table->string('nisn')->unique();
            $table->string('email');
            $table->string('foto');
            $table->string('nama');
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->enum('jk', ['laki-laki', 'perempuan']);
            $table->string('agama');
            $table->integer('anak_ke');
            $table->integer('jml_saudara');
            $table->string('akhir_pendidikan');
            $table->string('asal_sekolah');
            $table->string('thn_terima');
            $table->string('penyakit_diderita');
            $table->string('alamat');
            $table->integer('kodepos');
            $table->set('status', ['aktif', 'nonaktif']);
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
