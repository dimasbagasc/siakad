@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
  <br>
  <div class="section-header">
    <h1>Edit Data MataPelajaran</h1>
  </div>

  <div class="card">
    <div class="card-body">
      <form action="{{route('mapel.update',['mapel'=>$data->id])}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label>MataPelajaran</label>
          <input type="text" name="matapelajaran" class="form-control" placeholder="Nama Matapelajaran" value="{{$data->matapelajaran}}">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</section>

@endsection