@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Data Riwayat Pekerjaan</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('riwayatpekerjaan.update',['riwayatpekerjaan'=>$data->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Nama Instansi / Perusahaan</label>
                    <input type="text" name="instansi" class="form-control" placeholder="Nama Instansi /Perusahaan"
                        value="{{$data->instansi}}">
                </div>
                <div class="form-group">
                    <label>Jabatan</label>
                    <input type="text" name="jabatan" class="form-control" placeholder="Jabatan"
                        value="{{$data->jabatan}}">
                </div>
                <div class="form-group">
                    <label>Tanggal Mulai</label>
                    <input type="date" name="tgl_mulai" class="form-control" value="{{$data->tgl_mulai}}">
                </div>
                <div class="form-group">
                    <label>Tanggal Selesai</label>
                    <input type="date" name="tgl_selesai" class="form-control" value="{{$data->tgl_selesai}}">
                </div>

                <div class="form-group">
                    <label>Gaji Pokok</label>
                    <input type="text" name="gaji_pokok" class="form-control" placeholder="Gaji Pokok"
                        value="{{$data->gaji_pokok}}">
                </div>
                <div class="form-group">
                    <label>Nomor Surat Keputasan</label>
                    <input type="text" name="nomor_sk" class="form-control" placeholder="Nomor Surat Keputusan"
                        value="{{$data->nomor_sk}}">
                </div>
                <div class="form-group">
                    <label>Tanggal Surat Keputasan</label>
                    <input type="date" name="tgl_sk" class="form-control" placeholder="Tanggal Surat Keputusan"
                        value="{{$data->tgl_sk}}">
                </div>
                <div class="form-group">
                    <label>Pejabat Penandatangan SK</label>
                    <input type="text" name="pejabat_ttd" class="form-control"
                        placeholder="Pejabat Penandatangan Surat Keputusan" value="{{$data->pejabat_ttd}}">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection