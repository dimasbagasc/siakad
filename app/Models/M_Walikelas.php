<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Walikelas extends Model
{
    use HasFactory;

    protected $table = 'walikelas';
    protected $fillable = [
        'id_angkatan',
        'id_guru',
        'status'
    ];
}
