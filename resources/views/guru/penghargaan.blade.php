@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tanda Jasa/Penghargaan</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('kepegawaian.index')}}" class="btn btn-warning fas fa-angle-left"> Kembali</a>
                <a href="{{route('penghargaan.create')}}" class="btn btn-primary fa fa-plus"
                    style="position: absolute; right:25px;"> Tambah Data</a>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Bintang/ Lencana Penghargaan</th>
                                <th>Nomor SK</th>
                                <th>Tanggal SK</th>
                                <th>Tahun Perolehan</th>
                                <th>Nama Negara/Instansi yang Memberikan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($data as $penghargaan)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$penghargaan->nama_penghargaan }}</td>
                                <td>{{$penghargaan->nomor_sk}}</td>
                                <td>{{$penghargaan->tgl_sk}}</td>
                                <td>{{$penghargaan->tahun}}</td>
                                <td>{{$penghargaan->nama_instansi}}</td>
                                <td>
                                    <form method="POST"
                                        action="{{route('penghargaan.destroy',['penghargaan'=>$penghargaan->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{route('penghargaan.edit',['penghargaan'=>$penghargaan->id])}}"
                                            class="btn btn-icon btn-left btn-warning"><i class="far fa-edit"></i>
                                            Edit</a>
                                        <button type="submit" class="btn btn-icon btn-left btn-danger"><i
                                                class="fas fa-times"></i>
                                            Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>



@endsection