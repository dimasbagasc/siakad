<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = DB::table('users')->count();
        $guru = DB::table('guru')->count();
        $siswa = DB::table('siswa')->count();
        $mapel = DB::table('mapel')->count();
        $kelas = DB::table('kelas')->count();

        $countjk = DB::table('siswa')
            ->select(DB::raw("COUNT(siswa.id) as jml"), 'siswa.jk')
            ->groupBy('siswa.jk')
            ->get();

        $countsiswa = DB::table('siswa')
            ->select(DB::raw("COUNT(siswa.id) as jml"), 'siswa.thn_terima')
            ->groupBy('siswa.thn_terima')
            ->get();

        $countjk = json_encode($countjk);
        $countsiswa = json_encode($countsiswa);

        // print_r($countsiswa);
        // exit;

        // $countsiswa = DB::table('rombel')
        //     ->select(DB::raw("COUNT(rombel.id) as jml"), DB::raw("CONCAT(tahunajaran.tahun,'-',tahunajaran.semester) as angkatan"))
        //     ->join('angkatan', 'rombel.id_angkatan', '=', 'angkatan.id')
        //     ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
        //     ->groupBy('angkatan.id_tahunajaran', 'tahunajaran.tahun', 'tahunajaran.semester')
        //     ->get();


        return view('admin.admin', compact('user', 'guru', 'siswa', 'mapel', 'kelas', 'countjk', 'countsiswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
