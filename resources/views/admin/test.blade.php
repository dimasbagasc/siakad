@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Detail Data Kepegawaian</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <a href="{{route('guru.index')}}" class="btn btn-icon icon-left btn-warning"><i
                        class="fas fa-arrow-left"></i>Kembali</a>
                <br>
                <br>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tbody>
                            <tr>
                                <h3>I. PENDIDIKAN</h3>
                            </tr>
                            <tr>
                                <h4>1. Pendidikan didalam dan luar negeri</h4>
                            </tr>
                            {{-- <tr>
                                <td colspan="2" style="text-align:center">
                                    <img src="{{URL::to('/uploads').'/'.$data->foto}}" width="30%">
                                </td>
                            </tr> --}}
                            <div class="table-responsive">
                                <table class="table table-striped table-md datatable_table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tingkat</th>
                                            <th>Nama Sekolah</th>
                                            <th>Akreditasi</th>
                                            <th>Tempat</th>
                                            {{-- <th>STTB/IJAZAH</th>
                                            <th>Gelar</th> --}}
                                            <th>Nomor</th>
                                            <th>Tanggal</th>
                                            <th>Pejabat Penandatangan</th>
                                            <th>Depan</th>
                                            <th>Belakang</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $no = 1; @endphp
                                        @foreach ($pend_formal as $pf)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{ $pf->tingkat }}</td>
                                            <td>{{ $pf->nama_sekolah }}</td>
                                            <td>{{ $pf->akreditasi }}</td>
                                            <td>{{ $pf->tempat }}</td>
                                            <td>{{ $pf->nomor_ijazah }}</td>
                                            <td>{{ $pf->tanggal }}</td>
                                            <td>{{ $pf->pejabat_ttd }}</td>
                                            <td>{{ $pf->gelar_depan}}</td>
                                            <td>{{ $pf->gelar_belakang}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <tr>
                                <h4>2. Pendidikan didalam dan luar negeri</h4>
                            </tr>
                            <div class="table-responsive">
                                <table class="table table-striped table-md datatable_table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Kursus/Latihan</th>
                                            <th>Tanggal Mulai</th>
                                            <th>Tanggal Selesai</th>
                                            <th>Nomor</th>
                                            <th>Tempat</th>
                                            <th>Institusi Penyelenggara</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $no = 1; @endphp
                                        @foreach ($pend_informal as $pinf)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{ $pinf->nama_kursus }}</td>
                                            <td>{{ $pinf->tgl_mulai }}</td>
                                            <td>{{ $pinf->tgl_selesai }}</td>
                                            <td>{{ $pinf->nomor_kursus }}</td>
                                            <td>{{ $pinf->tempat }}</td>
                                            <td>{{ $pinf->institusi }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <tr>
                                <th>
                                    <br>
                                </th>
                            </tr>
                            <tr>
                                <h3>II. RIWAYAT PEKERJAAN</h3>
                            </tr>
                            <tr>
                                <h4>1. Riwayat Kepangkatan dan Golongan ruang penggajian</h4>
                            </tr>
                            <div class="table-responsive">
                                <table class="table table-striped table-md datatable_table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Instansi</th>
                                            <th>Jabatan</th>
                                            <th>Tanggal Mulai</th>
                                            <th>Tanggal Selesai</th>
                                            <th>Gaji Pokok</th>
                                            <th>Nomor</th>
                                            <th>Tanggal</th>
                                            <th>Penandatangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $no = 1; @endphp
                                        @foreach ($riwayat_pekerjaan as $rp)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{ $rp->instansi }}</td>
                                            <td>{{ $rp->jabatan }}</td>
                                            <td>{{ $rp->tgl_mulai }}</td>
                                            <td>{{ $rp->tgl_selesai }}</td>
                                            <td>{{ $rp->gaji_pokok }}</td>
                                            <td>{{ $rp->nomor_sk }}</td>
                                            <td>{{ $rp->tgl_sk }}</td>
                                            <td>{{ $rp->pejabat_ttd }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <tr>
                                <th>
                                    <br>
                                </th>
                            </tr>
                            <tr>
                                <h3>III. TANDA JASA/PENGHARGAAN</h3>
                            </tr>
                            <div class="table-responsive">
                                <table class="table table-striped table-md datatable_table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Penghargaan</th>
                                            <th>Nomor</th>
                                            <th>Tanggal</th>
                                            <th>Tahun</th>
                                            <th>Instansi yang Memberikan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $no = 1; @endphp
                                        @foreach ($penghargaan as $peng)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{ $peng->nama_penghargaan }}</td>
                                            <td>{{ $peng->nomor_sk }}</td>
                                            <td>{{ $peng->tgl_sk }}</td>
                                            <td>{{ $peng->tahun }}</td>
                                            <td>{{ $peng->nama_instansi }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <tr>
                                <th>
                                    <br>
                                </th>
                            </tr>
                            <tr>
                                <h3>IV. RIWAYAT KELUARGA</h3>
                            </tr>
                            <tr>
                                <h4>1. Riwayat Kepangkatan dan Golongan ruang penggajian</h4>
                            </tr>
                            <div class="table-responsive">
                                <table class="table table-striped table-md datatable_table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Kursus/Latihan</th>
                                            <th>Tanggal Mulai</th>
                                            <th>Tanggal Selesai</th>
                                            <th>Nomor</th>
                                            <th>Tempat</th>
                                            <th>Institusi Penyelenggara</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $no = 1; @endphp
                                        @foreach ($pend_informal as $pinf)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{ $pinf->nama_kursus }}</td>
                                            <td>{{ $pinf->tgl_mulai }}</td>
                                            <td>{{ $pinf->tgl_selesai }}</td>
                                            <td>{{ $pinf->nomor_kursus }}</td>
                                            <td>{{ $pinf->tempat }}</td>
                                            <td>{{ $pinf->institusi }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <tr>
                                <th>
                                    <br>
                                </th>
                            </tr>
                            <tr>
                                <h3>V. KETERANGAN ORGANISASI</h3>
                            </tr>
                            <div class="table-responsive">
                                <table class="table table-striped table-md datatable_table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Organisasi</th>
                                            <th>Jabatan</th>
                                            <th>Tanggal Mulai</th>
                                            <th>Tanggal Selesai</th>
                                            <th>Tempat</th>
                                            <th>Pemimpin Organisasi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $no = 1; @endphp
                                        @foreach ($organisasi_guru as $og)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{ $og->nama_organisasi }}</td>
                                            <td>{{ $og->jabatan }}</td>
                                            <td>{{ $og->tgl_mulai }}</td>
                                            <td>{{ $og->tgl_selesai }}</td>
                                            <td>{{ $og->tempat }}</td>
                                            <td>{{ $og->pemimpin }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <tr>
                                <th>
                                    <br>
                                </th>
                            </tr>
                            <tr>
                                <h3>VI. KETERANGAN LAIN-LAIN</h3>
                            </tr>
                            <div class="table-responsive">
                                <table class="table table-striped table-md datatable_table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Keterangan</th>
                                            <th>Nomor</th>
                                            <th>Tanggal</th>
                                            <th>Pejabat</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $no = 1; @endphp
                                        @foreach ($keterangan_lain as $kl)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{ $kl->nama_keterangan }}</td>
                                            <td>{{ $kl->nomor_sk }}</td>
                                            <td>{{ $kl->tgl_sk }}</td>
                                            <td>{{ $kl->pejabat }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </tbody>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@endsection