<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MapelController;
use App\Http\Controllers\DataguruController;
use App\Http\Controllers\DatakelasController;
use App\Http\Controllers\TahunajaranController;
use App\Http\Controllers\AngkatanController;
use App\Http\Controllers\DatasiswaController;
use App\Http\Controllers\SesiController;
use App\Http\Controllers\GurumapelController;
use App\Http\Controllers\RombelController;
use App\Http\Controllers\DatajadwalController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\GuruController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\JadwalguruController;
use App\Http\Controllers\PresensiController;
use App\Http\Controllers\DatakepegController;
use App\Http\Controllers\PendinformalController;
use App\Http\Controllers\PendformalController;
use App\Http\Controllers\RpekerjaanController;
use App\Http\Controllers\PenghargaanController;
use App\Http\Controllers\KetorganisasiController;
use App\Http\Controllers\KetlainController;
use App\Http\Controllers\WalikelasController;
use App\Http\Controllers\NilaiController;
use App\Http\Controllers\DatauserController;
use App\Http\Controllers\JadwalsiswaController;
use App\Http\Controllers\KelasguruController;
use App\Http\Controllers\KontaksiswaController;
use App\Http\Controllers\NilaisiswaController;
use App\Http\Controllers\PresensisiswaController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RkeluargaController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

//login
// Route::resource('login', AuthController::class);
Route::get('login', 'App\Http\Controllers\AuthController@index')->name('login');
Route::post('proses_login', 'App\Http\Controllers\AuthController@proses_login')->name('proses_login');
Route::get('logout', 'App\Http\Controllers\AuthController@logout')->name('logout');
Route::get('aktivasi_akun', 'App\Http\Controllers\AuthController@aktivasi_akun')->name('aktivasi_akun');
Route::get('lupapassword', 'App\Http\Controllers\AuthController@index_lupapassword')->name('lupapassword');
Route::post('forgot_password', 'App\Http\Controllers\AuthController@forgot_password')->name('forgot_password');
Route::get('reset_password', 'App\Http\Controllers\AuthController@reset_password')->name('reset_password');
Route::post('new_password', 'App\Http\Controllers\AuthController@new_password')->name('new_password');


//auth
//auth -> admin || guru || siswa

Route::group(['middleware' => ['auth']], function () {
    Route::resource('profile', ProfileController::class);
    Route::group(['middleware' => ['Cek_login:admin']], function () {
        Route::get('dashboardadmin', 'App\Http\Controllers\AdminController@index');
        //route admin
        Route::resource('dashboardadmin', AdminController::class);
        //admin : Mapel
        Route::resource('mapel', MapelController::class);
        //admin : guru
        Route::resource('guru', DataguruController::class);
        //admin : siswa
        Route::resource('siswa', DatasiswaController::class);
        //admin : kelas
        Route::resource('kelas', DatakelasController::class);
        //admin : angkatan
        Route::resource('angkatan', AngkatanController::class);
        //admin : tahun ajaran
        Route::resource('tahunajaran', TahunajaranController::class);
        //admin : sesi
        Route::resource('sesi', SesiController::class);
        //admin : guru mapel
        Route::resource('gurumapel', GurumapelController::class);
        //admin : rombel
        Route::resource('rombel', RombelController::class);
        //admin : jadwal
        Route::resource('jadwal', DatajadwalController::class);
        Route::get('hapus', 'App\Http\Controllers\DataJadwalController@hapus')->name('jadwal.hapus');
        //admin : walikelas
        Route::resource('walikelas', WalikelasController::class);
        //admin : kepegawaian
        Route::resource('kepeg', DatakepegController::class);
        //admin : user
        Route::resource('datauser', DatauserController::class);
    });
    Route::group(['middleware' => ['Cek_login:guru']], function () {
        Route::get('dashboardguru', 'App\Http\Controllers\GuruController@index')->name('dashboardguru.index');
        Route::resource('jadwalguru', JadwalguruController::class);
        Route::resource('presensi', PresensiController::class);
        Route::get('rekapharian', 'App\Http\Controllers\PresensiController@rekapharian')->name('presensi.rekapharian');
        Route::get('allrekap', 'App\Http\Controllers\PresensiController@allrekap')->name('presensi.allrekap');
        Route::get('indexallrekap', 'App\Http\Controllers\PresensiController@indexallrekap')->name('presensi.indexallrekap');
        Route::resource('kepegawaian', DatakepegController::class);
        Route::resource('pendinformal', PendinformalController::class);
        Route::resource('pendformal', PendformalController::class);
        Route::resource('riwayatpekerjaan', RpekerjaanController::class);
        Route::resource('penghargaan', PenghargaanController::class);
        Route::resource('ketorganisasi', KetorganisasiController::class);
        Route::resource('ketlain', KetlainController::class);
        Route::resource('nilai', NilaiController::class);
        Route::resource('kelasguru', KelasguruController::class);
        Route::resource('riwayatkeluarga', RkeluargaController::class);
        Route::resource('infokontak', KontaksiswaController::class);
        Route::resource('datanilaisiswa', NilaiController::class);
        Route::post('deskripsi', 'App\Http\Controllers\NilaiController@deskripsi')->name('datanilaisiswa.deskripsi');
    });
    Route::group(['middleware' => ['Cek_login:siswa']], function () {
        Route::get('dashboardsiswa', 'App\Http\Controllers\SiswaController@index')->name('dashboardsiswa.index');
        Route::resource('jadwalsiswa', JadwalsiswaController::class);
        Route::resource('presensisiswa', PresensisiswaController::class);
        Route::resource('nilaisiswa', NilaisiswaController::class);
        Route::get('cetakrapot', 'App\Http\Controllers\NilaisiswaController@cetakpdf')->name('nilaisiswa.cetakrapot');
    });
});
