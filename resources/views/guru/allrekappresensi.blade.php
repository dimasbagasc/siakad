@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>All Rekap Presensi</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>Sesi</th>
                                <th>Mata Pelajaran</th>
                                <th>Kelas</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($jadwal as $k => $j)
                            <tr>
                                <td>{{ ++$k }}</td>
                                <td>{{ $j->matapelajaran }}</td>
                                <td>{{ $j->nama_kelas }}</td>
                                <td>
                                    <a href="{{route('presensi.allrekap', ['id_angkatan'=>$j->id_angkatan, 'id_guru'=>$j->id_guru])}}"
                                        class="btn btn-icon btn-left btn-success"><i class="fas fa-info-circle"></i>
                                        Rekap
                                        Presensi</a>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>



@endsection