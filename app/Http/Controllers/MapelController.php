<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\M_Matapelajaran;

class MapelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = M_Matapelajaran::all();
        return view('admin.datamapel')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tambahmapel');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);
        $cek = DB::table('mapel')->where('matapelajaran', 'like', '%' . $data['matapelajaran'] . '%')
            ->count();
        if ($cek > 0) {
            return redirect('/mapel')->with(['error' => 'Data Sudah Ada.']);
        }
        M_Matapelajaran::insert($data);
        return redirect('/mapel')->with(['success' => 'Data Berhasil Di Tambahkan.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = M_Matapelajaran::findOrFail($id);
        return view('admin.show')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = M_Matapelajaran::findOrFail($id);
        return view('admin.editmapel')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = M_Matapelajaran::findOrFail($id);
        $data = $request->except(['_token']);
        $item->update($data);
        return redirect('/mapel')->with(['success' => 'Data Berhasil Di Perbarui.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $item = M_Matapelajaran::findOrFail($id);
            $item->delete();
            return redirect('/mapel')->with(['success' => 'Data Berhasil Di Hapus.']);
        } catch (\Throwable $th) {
            return redirect('/mapel')->with(['error' => 'Data Mata Pelajaran Sudah Digunakan.']);
        }
    }
}
