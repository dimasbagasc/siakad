<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Gurumapel extends Model
{
    use HasFactory;

    protected $table = 'gurumapel';
    protected $fillable = [
        'id_guru',
        'id_mapel'
    ];

    protected $hidden;
}
