@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
  <br>
  <div class="section-header">
    <h1>Tambah Data Guru</h1>
  </div>

  <div class="col-12 col-md-12 col-lg-12">
    <div class="card">
      <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-biodata-tab" data-toggle="tab" href="#nav-biodata" role="tab"
              aria-controls="nav-biodata" aria-selected="true">Biodata</a>
            <a class="nav-item nav-link" id="nav-akun-tab" data-toggle="tab" href="#nav-akun" role="tab"
              aria-controls="nav-akun" aria-selected="false">Data Akun</a>
          </div>
        </nav>
        <form method="POST" action="{{ route('guru.store') }}" enctype="multipart/form-data">
          @csrf
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-biodata" role="tabpanel" aria-labelledby="nav-biodata-tab">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>NIK</label>
                    <input type="text" name="nik" class="form-control" value="{{ old('nik') }}" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>NIP</label>
                    <input type="text" name="nip" class="form-control" value="{{ old('nip') }}" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" class="form-control" value="{{ old('nama') }}" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Tempat Lahir</label>
                    <input type="text" name="tmp_lahir" class="form-control" value="{{ old('tmp_lahir') }}" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="date" name="tgl_lahir" class="form-control" value="{{ old('tgl_lahir') }}" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="d-block">Jenis Kelamin</label>
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="jk" id="jk1" value="laki-laki" {{
                        old('jk')=='laki-laki' ? 'checked' : '' }}>
                      <label class="form-check-label" for="jk1">
                        Laki-laki
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="jk" id="jk2" value="perempuan" {{
                        old('jk')=='perempuan' ? 'checked' : '' }}>
                      <label class="form-check-label" for="jk2">
                        Perempuan
                      </label>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Agama</label>
                    <select name="agama" class="form-control" required>
                      <option value="islam" {{ old('agama')=='islam' ? 'selected' : '' }}>Islam</option>
                      <option value="katholik" {{ old('agama')=='katholik' ? 'selected' : '' }}>Katholik</option>
                      <option value="kristen" {{ old('agama')=='kristen' ? 'selected' : '' }}>Kristen</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Nomor Handphone</label>
                    <input type="text" name="no_hp" class="form-control" value="{{ old('no_hp') }}" pattern="\d*"
                      maxlength="12" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Alamat</label>
                    <textarea name="alamat" class="form-control" value="{{ old('alamat') }}" required></textarea>
                  </div>
                </div>
                {{-- <div class="col-md-6">
                  <div class="form-group">
                    <label>Keterangan Badan</label>
                    <input type="text" name="ket_badan" class="form-control" required>
                  </div>
                </div> --}}
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Kegemaran / Hobi</label>
                    <input type="text" name="hobi" class="form-control" value="{{ old('hobi') }}" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Status</label>
                    <select name="status" class="form-control" required>
                      <option value="aktif" selected>Aktif</option>
                      <option value="nonaktif">Non Aktif</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Foto</label>
                    <input type="file" name="foto" class="form-control" required>
                  </div>
                </div>
              </div>
            </div>

            <div class="tab-pane fade" id="nav-akun" role="tabpanel" aria-labelledby="nav-akun-tab">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}" required>

                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control" value="{{ old('username') }}" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Password</label>
                    <input type="text" name="password" class="form-control" value="{{ old('password') }}" required>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <a href="{{route('guru.index')}}" class="btn btn-icon icon-left btn-warning">Kembali</a>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>

    </div>
</section>

@endsection