@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Angkatan</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('sesi.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label class="d-block" for="sesi">Sesi</label>
                    <input type="text" name="sesi" class="form-control" placeholder="Sesi" required>
                </div>
                <div class="form-group">
                    <label class="d-block" for="jam_mulai">Jam Mulai</label>
                    <input placeholder="Selected time" type="text" id="jam_mulai" name="jam_mulai"
                        class="form-control timepicker" required>
                </div>
                <div class="form-group">
                    <label class="d-block" for="jam_selesai">Jam Selesai</label>
                    <input placeholder="Selected time" type="text" id="jam_selesai" name="jam_selesai"
                        class="form-control timepicker" required>
                </div>
                <div class="form-group">
                    <a href="{{route('sesi.index')}}" class="btn btn-icon icon-left btn-warning">Kembali</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection