<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class M_Siswa extends Model
{
    use HasFactory;

    protected $table = 'siswa';
    protected $fillable = [
        'id_user',
        'nisn',
        'email',
        'foto',
        'nama',
        'tempat_lahir',
        'tgl_lahir',
        'jk',
        'agama',
        'anak_ke',
        'jml_saudara',
        'akhir_pendidikan',
        'asal_sekolah',
        'thn_terima',
        'penyakit_diderita',
        'kodepos',
        'alamat',
        'status'
    ];

    protected $hidden;
}
