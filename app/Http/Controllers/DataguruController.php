<?php

namespace App\Http\Controllers;

use App\Models\M_guru;
use App\Models\User;
use Illuminate\Http\Request;
use File;

class DataguruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = M_guru::all();
        return view('admin.dataguru')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tambahguru');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|unique:guru',
            'username' => 'required|unique:users',
            'password' => 'required'
        ]);
        $data = $request->except(['_token']);
        $request->validate([
            'foto' => 'required|mimes:png,jpeg,jpg|max:1024',
        ]);

        $fileName = time() . '.' . $request->foto->extension();

        $res = $request->foto->move(public_path('uploads'), $fileName);

        if ($res) {
            $dataUser = [
                'name' => $data['nama'],
                'username' => $data['username'],
                'password' => bcrypt($data['password']),
                'role' => 'guru'
            ];
            $user = User::create($dataUser);

            $dataGuru = [
                'id_user' => $user->id,
                'foto' => $fileName,
                'nama' => $data['nama'],
                'nip' => $data['nip'],
                'nik' => $data['nik'],
                'tmp_lahir' => $data['tmp_lahir'],
                'tgl_lahir' => $data['tgl_lahir'],
                'jk' => $data['jk'],
                'alamat' => $data['alamat'],
                'hobi' => $data['hobi'],
                'no_hp' => $data['no_hp'],
                'email' => $data['email'],
                'agama' => $data['agama'],
                'status' => $data['status']
                // 'ket_badan' => $data['ket_badan']
            ];
            M_guru::insert($dataGuru);
        }

        return redirect('/guru')->with(['success' => 'Data Berhasil Di Tambahkan.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = M_guru::findOrFail($id);
        return view('admin.showguru')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = M_guru::findOrFail($id);
        return view('admin.editguru')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = M_guru::findOrFail($id);
        $data = $request->except(['_token']);
        $request->validate([
            'foto' => 'mimes:png,jpeg,jpg|max:1024',
        ]);

        if ($request->hasFile('foto')) {
            $fileName = time() . '.' . $request->foto->extension();

            $res = $request->foto->move(public_path('uploads'), $fileName);
            if ($res) {
                $data['foto'] = $fileName;
            }
        }

        $item->update($data);
        return redirect('/guru')->with(['success' => 'Data Berhasil Di Perbarui.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $item = M_guru::findOrFail($id);
            $item->delete();
            return redirect('/guru')->with(['success' => 'Data Berhasil Di Hapus.']);
        } catch (\Throwable $th) {
            return redirect('/guru')->with(['error' => 'Data Guru Sudah Digunakan.']);
        }
    }
}
