@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')
@php
use Carbon\Carbon;
setlocale(LC_TIME, 'id_ID');
\Carbon\Carbon::setLocale('id');
\Carbon\Carbon::now()->formatLocalized("%A, %d %B %Y");
@endphp

<section class="section">
    <br>
    <div class="section-header">
        <h1>Presensi Siswa</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4>
                    {{ Carbon::now()->isoFormat('dddd, D MMMM Y'); }}
                </h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>Kelas</th>
                            <td>{{$jadwal->nama_kelas}}</td>
                        </tr>
                        <tr>
                            <th>Guru Mapel</th>
                            <td>{{$jadwal->nama}}</td>
                        </tr>
                        <tr>
                            <th>Mata Pelajaran</th>
                            <td>{{$jadwal->matapelajaran}}</td>
                        </tr>
                    </table>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="card bg-success">
                                <div class="card-body">
                                    <h5 class="card-title">HADIR</h5>
                                    <p class="card-text">{{$countpresensi->hadir}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card bg-danger">
                                <div class="card-body">
                                    <h5 class="card-title">ALFA</h5>
                                    <p class="card-text">{{$countpresensi->alpha}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card bg-warning">
                                <div class="card-body">
                                    <h5 class="card-title">IJIN</h5>
                                    <p class="card-text">{{$countpresensi->ijin}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-md">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NISN</th>
                                <th>Nama</th>
                                <th>Status Presensi</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($presensi as $i=> $s)
                            <tr>
                                <td>{{++$i}}</td>
                                <td>{{ $s->nisn }}</td>
                                <td>{{ $s->nama}}</td>
                                <td>{{$s->status}}</td>
                                <td>{{$s->keterangan}}</td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
    </div>
</section>



@endsection