@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')
@php
use Carbon\Carbon;
setlocale(LC_TIME, 'id_ID');
\Carbon\Carbon::setLocale('id');
\Carbon\Carbon::now()->formatLocalized("%A, %d %B %Y");
@endphp

<section class="section">
    <br>
    <div class="section-header">
        <h1>Presensi Siswa</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4>
                    {{ Carbon::now()->isoFormat('dddd, D MMMM Y'); }}
                </h4>
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>Sesi</th>
                                <th>Waktu</th>
                                <th>Mata Pelajaran</th>
                                <th>Kelas</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($jadwal as $j)
                            <tr>
                                <td>{{ $j->sesi }}</td>
                                <td>{{ $j->jam_mulai.'-'.$j->jam_selesai }}</td>
                                <td>{{ $j->matapelajaran }}</td>
                                <td>{{ $j->nama_kelas }}</td>
                                <td>
                                    <a href="{{route('presensi.create', ['id_angkatan'=>$j->id_angkatan, 'id_jadwal'=>$j->id])}}"
                                        class="btn btn-icon btn-left btn-info"><i class="fas fa-info-circle"></i> Kelola
                                        Presensi</a>

                                    <a href="{{route('presensi.rekapharian', ['id_angkatan'=>$j->id_angkatan, 'id_jadwal'=>$j->id])}}"
                                        class="btn btn-icon btn-left btn-success"><i class="fas fa-info-circle"></i>
                                        Rekap
                                        Presensi</a>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>



@endsection