<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\M_Sesi;

class SesiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = M_Sesi::all();
        return view('admin.sesi')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tambahsesi');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);
        $cek = DB::table('sesi')->where('sesi', 'like', '%' . $data['sesi'] . '%')
            ->count();
        if ($cek > 0) {
            return redirect('/sesi')->with(['error' => 'Data Sudah Ada.']);
        }
        M_Sesi::insert($data);
        return redirect('/sesi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = M_Sesi::findOrFail($id);
        return view('admin.showsesi')->with([
            'data' => $data
        ])->with(['success' => 'Data Berhasil Di Tambahkan']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = M_Sesi::findOrFail($id);
        return view('admin.showsesi')->with([
            'data' => $data
        ])->with(['success' => 'Data Berhasil Di Tambahkan']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = M_Sesi::findOrFail($id);
        $data = $request->except(['_token']);
        $item->update($data);
        return redirect('/sesi')->with(['success' => 'Data Berhasil Di Perbarui']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $item = M_Sesi::findOrFail($id);
            $item->delete();
            return redirect('/sesi')->with(['success' => 'Data Berhasil Di Hapus']);
        } catch (\Throwable $th) {
            return redirect('/sesi')->with(['error' => 'Data Sesi Sudah Digunakan.']);
        }
    }
}
