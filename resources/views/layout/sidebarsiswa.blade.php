@section('sidebar')
<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="index.html">SIAKAD</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="index.html">St</a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Dashboard</li>
        <li class="nav-item {{ request()->is('dashboardsiswa') ? 'active' : ''}}">
            <a href="{{route('dashboardsiswa.index')}}" class=" nav-link "><i
                    class=" fas fa-fire"></i><span>Dashboard</span></a>
        </li>
        <li class="menu-header">Jadwal</li>
        <li class="nav-item {{ request()->is('jadwalsiswa') ? 'active' : ''}}">
            <a href="{{route('jadwalsiswa.index')}}" class="nav-link"><i
                    class="fas fas fa-columns"></i><span>Jadwal</span></a>
        </li>
        <li class="menu-header">Presensi</li>
        <li class="nav-item {{ request()->is('presensisiswa') ? 'active' : ''}}">
            <a href="{{route('presensisiswa.index')}}" class=" nav-link "><i
                    class=" fas fa-book-open"></i><span>Presensi</span></a>
        </li>
        <li class="menu-header">Nilai</li>
        <li class="nav-item {{ request()->is('nilaisiswa') ? 'active' : ''}}">
            <a href="{{route('nilaisiswa.index')}}" class="nav-link"><i
                    class="fas fa-book-reader"></i><span>Nilai</span></a>
        </li>

</aside>
@endsection