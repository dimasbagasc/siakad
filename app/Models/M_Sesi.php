<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Sesi extends Model
{
    use HasFactory;

    protected $table = 'sesi';
    protected $fillable = [
        'sesi',
        'jam_mulia',
        'jam_selesai'
    ];
}
