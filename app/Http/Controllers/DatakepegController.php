<?php

namespace App\Http\Controllers;

use App\Models\M_guru;
use Illuminate\Http\Request;
use App\Models\M_Ketlain;
use App\Models\M_Ketorganisasi;
use App\Models\M_Peninformal;
use App\Models\M_Pendformal;
use App\Models\M_Riwayatpekerjaan;
use App\Models\M_Penghargaan;
use App\Models\M_Riwayatkeluarga;

class DatakepegController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('guru.kepeg');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $guru = M_guru::findOrFail($id);
        $pf = M_Pendformal::where('id_guru', $guru->id)->get();
        $pinf = M_Peninformal::where('id_guru', $guru->id)->get();
        $rp = M_Riwayatpekerjaan::where('id_guru', $guru->id)->get();
        $penghargaan = M_Penghargaan::where('id_guru', $guru->id)->get();
        $kor = M_Ketorganisasi::where('id_guru', $guru->id)->get();
        $kl = M_Ketlain::where('id_guru', $guru->id)->get();
        $rk = M_Riwayatkeluarga::where('id_guru', $guru->id)->get();
        return view('admin.datakepegguru')->with([
            'id_guru' => $guru,
            'pend_formal' => $pf,
            'pend_informal' => $pinf,
            'riwayat_pekerjaan' => $rp,
            'penghargaan' => $penghargaan,
            'riwayat_keluarga' => $rk,
            'organisasi_guru' => $kor,
            'keterangan_lain' => $kl,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
