@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Data Siswa Angkatan</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        @if (empty($walikelas))

        <div class="alert alert-danger alert-dismissible fade show">Wali kelas kosong!, silahkan isi disini <a
                href="{{route('walikelas.index',['id_angkatan'=>$angkatan->id])}}"
                class="btn btn-icon btn-left btn-warning"><i class="fas fa-info-circle"></i>
                Wali Kelas</a></div>
        @else
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{route('angkatan.index')}}" class="btn btn-warning fas fa-angle-left"> Kembali</a>
                        <a href="{{route('rombel.create', ['id_angkatan' => $angkatan->id])}}"
                            class="btn btn-primary fa fa-plus"> Tambah Siswa</a>
                    </div>
                    <br>
                    <br>
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th>Kelas</th>
                                    <td>{{$angkatan->nama_kelas}}</td>
                                </tr>
                                <tr>
                                    <th>Wali Kelas</th>
                                    <td>{{$walikelas->nama}}</td>
                                </tr>
                                <tr>
                                    <th>Tahun Ajaran</th>
                                    <td>{{$angkatan->tahun}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NISN</th>
                                <th>Nama</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($data as $rombel)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{ $rombel->nisn }}</td>
                                <td>{{ $rombel->nama }}</td>

                                <td>
                                    <form method="POST" action="{{route('rombel.destroy',['rombel'=>$rombel->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        <input name="id_angkatan" type="hidden" value="{{$angkatan->id}}">
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        @endif
    </div>
    </div>
</section>



@endsection