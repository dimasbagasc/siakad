<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Ortu extends Model
{
    use HasFactory;

    protected $table = 'ortu';
    protected $fillable = [
        'id_siswa',
        'nama_ayah',
        'nik_ayah',
        'pekerjaan_ayah',
        'nama_ibu',
        'nik_ibu',
        'nama_wali',
        'hubungan',
        'alamat_wali',
        'nohp'
    ];
}
