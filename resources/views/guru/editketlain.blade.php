@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Data Pendidikan Informal</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('ketlain.update',['ketlain'=>$data->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Nama Ketarangan</label>
                    <select name="nama_keterangan" class="form-control">
                        <option></option>
                        <option Surat value="skck" {{$data->nama_keterangan == 'skck' ? 'selected' : ''}}>Surat
                            Keterangan Catatan Kepolisian</option>
                        <option value="sks" {{$data->nama_keterangan == 'sks' ? 'selected' : ''}}>Surat Keterangan Sehat
                        </option>
                        <option value="skbn" {{$data->nama_keterangan == 'skbn' ? 'selected' : ''}}>Surat Keterangan
                            Bebas Napza</option>
                        <option value="lainnya" {{$data->nama_keterangan == 'lainnya' ? 'selected' : ''}}>Keterangan
                            lain yang Dianggap Perlu</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Nomor Surat Keputusan</label>
                    <input type="text" name="nomor_sk" class="form-control" placeholder="Nomor Surat Keputusan"
                        value="{{$data->nomor_sk}}">
                </div>
                <div class="form-group">
                    <label>Tanggal Surat Keputusan</label>
                    <input type="date" name="tgl_sk" class="form-control" value="{{$data->tgl_sk}}">
                </div>

                <div class="form-group">
                    <label>Pejabat</label>
                    <input type="text" name="pejabat" class="form-control" placeholder="Nama Pejabat"
                        value="{{$data->pejabat}}">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection