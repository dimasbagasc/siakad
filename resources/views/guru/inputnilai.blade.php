@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Nilai Siswa</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>Kelas</th>
                            <td>{{$jadwal->nama_kelas}}</td>
                        </tr>
                        <tr>
                            <th>Guru Mapel</th>
                            <td>{{$jadwal->nama}}</td>
                        </tr>
                        <tr>
                            <th>Mata Pelajaran</th>
                            <td>{{$jadwal->matapelajaran}}</td>
                        </tr>
                    </table>
                </div>
                <div class="alert alert-danger bobot_pengetahuan" role="alert" style="display: none">
                    Jumlah Bobot Pengetahuan Kurang Atau Lebih Dari 4!
                </div>
                <div class="alert alert-danger bobot_ketrampilan" role="alert" style="display: none">
                    Jumlah Bobot Ketrampilan Kurang Atau Lebih Dari 4!
                </div>
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-pengetahuan-tab" data-toggle="tab"
                            href="#nav-pengetahuan" role="tab" aria-controls="nav-pengetahuan"
                            aria-selected="true">Nilai Pengetahuan</a>
                        <a class="nav-item nav-link" id="nav-ketrampilan-tab" data-toggle="tab" href="#nav-ketrampilan"
                            role="tab" aria-controls="nav-ketrampilan" aria-selected="false">Nilai Ketrampilan</a>
                        <a class="nav-item nav-link" id="nav-sikap-tab" data-toggle="tab" href="#nav-sikap" role="tab"
                            aria-controls="nav-sikap" aria-selected="false">Nilai Sikap</a>
                    </div>
                </nav>

                <form method="POST" action="{{route('nilai.store')}}">
                    @csrf
                    <input type="hidden" value="{{$id_gurumapel}}" name="id_gurumapel">

                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-pengetahuan" role="tabpanel"
                            aria-labelledby="nav-pengetahuan-tab">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bobot Proses</label>
                                        <input type="number" name="bobot_np" class="form-control bobot_np"
                                            value="{{$bobot_np}}" placeholder="Nilai Bobot" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bobot UTS</label>
                                        <input type="number" name="bobot_nuts" class="form-control bobot_nuts"
                                            value="{{$bobot_nuts}}" placeholder="Nilai Bobot" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bobot UAS</label>
                                        <input type="number" name="bobot_nuas" class="form-control bobot_nuas"
                                            value="{{$bobot_nuas}}" placeholder="Nilai Bobot" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <table class="table table-striped table-md">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NISN</th>
                                            <th>Nama</th>
                                            <th>Nilai Proses</th>
                                            {{-- <th>Bobot</th> --}}
                                            <th>Nilai UTS</th>
                                            {{-- <th>Bobot</th> --}}
                                            <th>Nilai UAS</th>
                                            {{-- <th>Bobot</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($siswa as $i=> $s)
                                        <tr>
                                            <td>{{++$i}}</td>
                                            <td>{{ $s->nisn }}</td>
                                            <td>{{ $s->nama}}</td>
                                            <td>
                                                <input name="nilai[{{$s->id}}][np]" class="form-control" required value="@php
                                        if(isset($nilai[$s->id]['np']))
                                        {
                                            echo $nilai[$s->id]['np'];
                                        }
                                    @endphp" type="number">
                                                {{--
                                            <td>
                                                <input name="nilai[{{$s->id}}][bobot_np]" class="form-control" required
                                                    value="@php
                                        if(isset($nilai[$s->id]['bobot_np']))
                                        {
                                            echo $nilai[$s->id]['bobot_np'];
                                        }
                                    @endphp" type="number">
                                            </td> --}}
                                            <td>
                                                <input name="nilai[{{$s->id}}][nuts]" class="form-control" required
                                                    value="@php
                                        if(isset($nilai[$s->id]['nuts']))
                                        {
                                            echo $nilai[$s->id]['nuts'];
                                        }
                                    @endphp" type="number">
                                                {{--
                                            <td>
                                                <input name="nilai[{{$s->id}}][bobot_nuts]" class="form-control"
                                                    required value="@php
                                        if(isset($nilai[$s->id]['bobot_nuts']))
                                        {
                                            echo $nilai[$s->id]['bobot_nuts'];
                                        }
                                    @endphp" type="number">
                                            </td> --}}
                                            <td>
                                                <input name="nilai[{{$s->id}}][nuas]" class="form-control" required
                                                    value="@php
                                        if(isset($nilai[$s->id]['nuas']))
                                        {
                                            echo $nilai[$s->id]['nuas'];
                                        }
                                    @endphp" type="number">
                                            </td>
                                            {{-- <td>
                                                <input name="nilai[{{$s->id}}][bobot_nuas]" class="form-control"
                                                    required value="@php
                                        if(isset($nilai[$s->id]['bobot_nuas']))
                                        {
                                            echo $nilai[$s->id]['bobot_nuas'];
                                        }
                                    @endphp" type="number">
                                            </td> --}}
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="nav-ketrampilan" role="tabpanel"
                            aria-labelledby="nav-ketrampilan-tab">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bobot Praktik</label>
                                        <input type="number" name="bobot_praktik" class="form-control bobot_praktik"
                                            value="{{$bobot_praktik}}" placeholder="Nilai Bobot" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bobot Portofolio</label>
                                        <input type="number" name="bobot_porto" class="form-control bobot_porto"
                                            value="{{$bobot_porto}}" placeholder="Nilai Bobot" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Bobot Proyek</label>
                                        <input type="number" name="bobot_proyek" class="form-control bobot_proyek"
                                            value="{{$bobot_proyek}}" placeholder="Nilai Bobot" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <table class="table table-striped table-md">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NISN</th>
                                            <th>Nama</th>
                                            <th>Nilai Praktik</th>
                                            {{-- <th>Bobot</th> --}}
                                            <th>Nilai Portofolio</th>
                                            {{-- <th>Bobot</th> --}}
                                            <th>Nilai Proyek</th>
                                            {{-- <th>Bobot</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($siswa as $i=> $s)
                                        <tr>
                                            <td>{{++$i}}</td>
                                            <td>{{ $s->nisn }}</td>
                                            <td>{{ $s->nama}}</td>
                                            <td>
                                                <input name="nilai[{{$s->id}}][nilai_praktik]" class="form-control"
                                                    required value="@php
                                        if(isset($nilai[$s->id]['nilai_praktik']))
                                        {
                                            echo $nilai[$s->id]['nilai_praktik'];
                                        }
                                    @endphp" type="number">
                                                {{--
                                            <td>
                                                <input name="nilai[{{$s->id}}][bobot_praktik]" class="form-control"
                                                    required value="@php
                                        if(isset($nilai[$s->id]['bobot_praktik']))
                                        {
                                            echo $nilai[$s->id]['bobot_praktik'];
                                        }
                                    @endphp" type="number">
                                            </td> --}}
                                            <td>
                                                <input name="nilai[{{$s->id}}][nilai_porto]" class="form-control"
                                                    required value="@php
                                        if(isset($nilai[$s->id]['nilai_porto']))
                                        {
                                            echo $nilai[$s->id]['nilai_porto'];
                                        }
                                    @endphp" type="number">
                                                {{--
                                            <td>
                                                <input name="nilai[{{$s->id}}][bobot_porto]" class="form-control"
                                                    required value="@php
                                        if(isset($nilai[$s->id]['bobot_porto']))
                                        {
                                            echo $nilai[$s->id]['bobot_porto'];
                                        }
                                    @endphp" type="number">
                                            </td> --}}
                                            <td>
                                                <input name="nilai[{{$s->id}}][nilai_proyek]" class="form-control"
                                                    required value="@php
                                        if(isset($nilai[$s->id]['nilai_proyek']))
                                        {
                                            echo $nilai[$s->id]['nilai_proyek'];
                                        }
                                    @endphp" type="number">
                                            </td>
                                            {{-- <td>
                                                <input name="nilai[{{$s->id}}][bobot_proyek]" class="form-control"
                                                    required value="@php
                                        if(isset($nilai[$s->id]['bobot_proyek']))
                                        {
                                            echo $nilai[$s->id]['bobot_proyek'];
                                        }
                                    @endphp" type="number">
                                            </td> --}}
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-sikap" role="tabpanel" aria-labelledby="nav-sikap-tab">
                            <div class="row">
                                <table class="table table-striped table-md">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NISN</th>
                                            <th>Nama</th>
                                            <th>Nilai Sikap Mapel</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($siswa as $i=> $s)
                                        <tr>
                                            <td>{{++$i}}</td>
                                            <td>{{ $s->nisn }}</td>
                                            <td>{{ $s->nama}}</td>
                                            <td>
                                                <input name="nilai[{{$s->id}}][nilai_sikapmapel]" class="form-control"
                                                    required value="@php
                                        if(isset($nilai[$s->id]['nilai_sikapmapel']))
                                        {
                                            echo $nilai[$s->id]['nilai_sikapmapel'];
                                        }
                                    @endphp" type="number">
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn_simpan">Simpan</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</section>



@endsection
@push('js_script')
<script>
    $(document).ready(function(){
        $(".bobot_np, .bobot_nuts, .bobot_nuas, .bobot_praktik, .bobot_porto, .bobot_proyek").keyup(function(){
            var bobot_np = $(".bobot_np").val();
            var bobot_nuts = $(".bobot_nuts").val();
            var bobot_nuas = $(".bobot_nuas").val();
            var bobot_praktik = $(".bobot_praktik").val();
            var bobot_porto = $(".bobot_porto").val();
            var bobot_proyek = $(".bobot_proyek").val();

            var jumlah_bobot_pengetahuan = parseInt(bobot_np) + parseInt(bobot_nuts) + parseInt(bobot_nuas);
            var jumlah_bobot_ketrampilan = parseInt(bobot_praktik) + parseInt(bobot_porto) + parseInt(bobot_proyek);
            
            if(jumlah_bobot_pengetahuan != 4){
                $('.bobot_pengetahuan').show();
                $('.btn_simpan').prop('disabled', true);
            }
            if(jumlah_bobot_pengetahuan == 4){
                $('.bobot_pengetahuan').hide();
                $('.btn_simpan').prop('disabled', false);
            }

            if(jumlah_bobot_ketrampilan != 4){
                $('.bobot_ketrampilan').show();
                $('.btn_simpan').prop('disabled', true);
            }
            if(jumlah_bobot_ketrampilan == 4){
                $('.bobot_ketrampilan').hide();
                $('.btn_simpan').prop('disabled', false);
            }

        })
    })
</script>

@endpush