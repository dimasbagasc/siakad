@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')
<section class="section">
    <br>
    <div class="section-header">
        <h1>Dashboard</h1>
    </div>
    @if(session('error_akses'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{session('error_akses')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <div class="alert alert-success alert-dismissible fade show" role="alert">

        Selamat Datang di Sistem Akademik Muhammadiyah Boarding School Sebagai Guru.
        {{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button> --}}
    </div>


    @if ($countpend_formal == 0 || $countpend_informal == 0 || $countrpekerjaan == 0)
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                Data Kepegawaian Anda Belum Lengkap!
                Silahkan klik tombol berikut untuk melengkapi data.
                <a href="{{route('kepegawaian.index')}}" class="btn btn-warning"> Dapeg</a>

            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                    <i class="fas fa-user-graduate"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Siswa</h4>
                    </div>
                    <div class="card-body">
                        {{$countjumlahsiswa}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                    <i class="fas fa-bookmark"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Mapel</h4>
                    </div>
                    <div class="card-body">
                        {{$countjumlahmapel}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection