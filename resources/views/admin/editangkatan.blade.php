@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Angkatan</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('angkatan.update',['angkatan'=>$data->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label class="d-block" for="id_tahunajaran">Tahun Ajaran</label>
                    <select class="form-control select2_dropdown" name="id_tahunajaran" id="id_tahunajaran">
                        <option></option>
                        @foreach ($tahunAjaran as $v)
                        <option value="{{ $v->id }}" {{ ($v->id == $data->id_tahunajaran ? 'selected' : '') }}>{{
                            $v->tahun }}</option>
                        @endforeach

                    </select>
                </div>
                <div class="form-group">
                    <label class="d-block" for="id_kelas">Kelas</label>
                    <select class="form-control select2_dropdown" name="id_kelas" id="id_kelas">
                        <option></option>
                        @foreach ($kelas as $k)
                        <option value="{{ $k->id }}" {{ ($k->id == $data->id_kelas ? 'selected' : '') }}>{{
                            $k->nama_kelas }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection