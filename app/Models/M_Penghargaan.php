<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Penghargaan extends Model
{
    use HasFactory;

    protected $table = 'penghargaan';
    protected $fillable = [
        'id_guru',
        'nama_penghargaan',
        'nomor_sk',
        'tgl_sk',
        'tahun',
        'nama_instansi'
    ];
}
