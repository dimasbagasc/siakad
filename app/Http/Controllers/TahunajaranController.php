<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\M_Tahunajaran;
use Illuminate\Support\Facades\DB;

class TahunajaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = M_Tahunajaran::orderBy('status', 'asc')->get();
        return view('admin.tahunajaran')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tambahtahun');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);
        if ($data['status'] == 'aktif') {
            DB::table('tahunajaran')
                ->update(['status' => 'nonaktif']);
        }
        $cek = DB::table('tahunajaran')->where('tahun', 'like', '%' . $data['tahun'] . '%')
            ->count();
        if ($cek > 0) {
            return redirect('/tahunajaran')->with(['error' => 'Data Sudah Ada.']);
        }
        M_Tahunajaran::insert($data);
        return redirect('/tahunajaran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = M_Tahunajaran::findOrFail($id);
        return view('admin.showtahun')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = M_Tahunajaran::findOrFail($id);
        $data = $request->except(['_token']);
        if ($data['status'] == 'aktif') {
            DB::table('tahunajaran')
                ->update(['status' => 'nonaktif']);
        }
        $item->update($data);
        return redirect('/tahunajaran')->with(['success' => 'Data Berhasil Di Perbarui']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $item = M_Tahunajaran::findOrFail($id);
            $item->delete();
            return redirect('/tahunajaran')->with(['success' => 'Data Berhasil Di Hapus.']);
        } catch (\Throwable $th) {
            return redirect('/tahunajaran')->with(['error' => 'Data Tahun Ajaran Sudah Digunakan.']);
        }
    }
}
