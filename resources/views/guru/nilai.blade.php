@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Data Nilai Siswa</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>Mata Pelajaran</th>
                                <th>Kelas</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($jadwal as $j)
                            <tr>
                                <td>{{ $j->matapelajaran }}</td>
                                <td>{{ $j->nama_kelas }}</td>
                                <td>
                                    <a href="{{route('nilai.create', ['id_angkatan'=>$j->id_angkatan, 'id_gurumapel'=>$j->id_guru])}}"
                                        class="btn btn-icon btn-left btn-info"><i class="fas fa-info-circle"></i>
                                        Kelola Nilai</a>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</section>



@endsection