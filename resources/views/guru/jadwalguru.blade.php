@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1> Jadwal {{$tahunAktif->tahun . '('.$tahunAktif->semester.')'}}</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md">
                        <thead>
                            <tr>
                                <th>Sesi</th>
                                <th>Waktu</th>
                                <th>Senin</th>
                                <th>Selasa</th>
                                <th>Rabu</th>
                                <th>Kamis</th>
                                <th>Jumat</th>
                                <th>Sabtu</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sesi as $s)
                            <tr>
                                <td>{{ $s->sesi }}</td>
                                <td>{{ $s->jam_mulai.'-'.$s->jam_selesai }}</td>
                                @for ($i = 1; $i < 7; $i++) @if (isset($jadwal[$s->sesi]))
                                    @if (isset($jadwal[$s->sesi][$i]))
                                    <td>{{$jadwal[$s->sesi][$i][0]->nama_kelas.' |
                                        '.$jadwal[$s->sesi][$i][0]->matapelajaran }}
                                    </td>
                                    @else
                                    <td></td>
                                    @endif
                                    @else
                                    <td></td>
                                    @endif
                                    @endfor
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@endsection