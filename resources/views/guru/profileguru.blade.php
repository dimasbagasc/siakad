@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')


<section class="section">
    <br>
    <div class="section-header">
        <h1>Profile</h1>
    </div>
    <div class="section-body">
        <div class="row mt-sm-12">
            <div class="col-6 col-md-12">
                <form method="post" class="needs-validation"
                    action="{{route('profile.update', ['profile'=>$data->id_user])}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
            </div>
            <div class="col-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit Profile</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-6 col-12">
                                <label>Email</label>
                                <input type="text" name="email" class="form-control" value="{{$data->email}}"
                                    required="">
                                <div class="invalid-feedback">
                                    Please fill in the first name
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-12">
                                <label>Username</label>
                                <input type="text" name="username" class="form-control" value="{{$data->username}}"
                                    required="">
                                <div class="invalid-feedback">
                                    Please fill in the last name
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>NIK</label>
                                    <input type="text" name="nik" class="form-control" value="{{$data->nik}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>NIP</label>
                                    <input type="text" name="nip" class="form-control" value="{{$data->nip}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama" class="form-control" value="{{$data->nama}}"
                                        required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tempat Lahir</label>
                                    <input type="text" name="tmp_lahir" class="form-control"
                                        value="{{$data->tmp_lahir}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="date" name="tgl_lahir" class="form-control"
                                        value="{{$data->tgl_lahir}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="d-block">Jenis Kelamin</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="jk" id="jk1"
                                            value="laki-laki" {{$data->jk == 'laki-laki' ? 'checked' : ''}}>
                                        <label class="form-check-label" for="jk1">
                                            Laki-laki
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="jk" id="jk2"
                                            value="perempuan" {{$data->jk == 'perempuan' ? 'checked' : ''}}>
                                        <label class="form-check-label" for="jk2">
                                            Perempuan
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Agama</label>
                                    <select name="agama" class="form-control">
                                        <option></option>
                                        <option value="islam" {{$data->agama == 'islam' ? 'selected' : ''}}>Islam
                                        </option>
                                        <option value="katholik" {{$data->agama == 'katholik' ? 'selected' :
                                            ''}}>Katholik</option>
                                        <option value="kristen" {{$data->agama == 'kristen' ? 'selected' :
                                            ''}}>Kristen</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nomor Handphone</label>
                                    <input type="text" name="no_hp" class="form-control" value="{{$data->no_hp}}"
                                        required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea name="alamat" class="form-control" id="alamat">
                                            {{$data->alamat}}
                                        </textarea>
                                </div>
                            </div>
                            {{-- <div class="col-md-6">
                                <div class="form-group">
                                    <label>Keterangan Badan</label>
                                    <input type="text" name="ket_badan" class="form-control" required>
                                </div>
                            </div> --}}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kegemaran / Hobi</label>
                                    <input type="text" name="hobi" class="form-control" value="{{$data->hobi}}"
                                        required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Foto</label>
                                    <input type="file" name="foto" class="form-control">
                                    <img alt="image" src="{{URL::to('/uploads').'/'.$data->foto}}"
                                        class="profile-widget-picture" style="width:35%">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div>
                        </form>
                    </div>
                </div>

            </div>
</section>



@endsection