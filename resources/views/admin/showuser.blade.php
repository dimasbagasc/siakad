@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Detail User</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <a href="{{route('datauser.index')}}" class="btn btn-icon icon-left btn-warning"><i
                        class="fas fa-arrow-left"></i>Kembali</a>
                <br>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                        <tbody>
                            <tr>
                                <h2>Detail Data User</h2>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:center">
                                    <img src="{{URL::to('/uploads').'/'.$data->foto}}" width="30%">
                                </td>
                            </tr>

                            <tr>
                                <th>Nama</th>
                                <td>{{$users->name}}</td>
                            </tr>
                            <tr>
                                <th>username</th>
                                <td>{{$users->username}}</td>
                            </tr>
                            <tr>
                                <th>Role</th>
                                <td>{{$users->role}}</td>
                            </tr>
                            <tr>
                                <th>status</th>
                                <td>{{$data->status}}</td>
                            </tr>
                        </tbody>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@endsection