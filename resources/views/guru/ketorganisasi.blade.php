@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Keterangan Organisasi</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('kepegawaian.index')}}" class="btn btn-warning fas fa-angle-left"> Kembali</a>
                <a href="{{route('ketorganisasi.create')}}" class="btn btn-primary fa fa-plus"
                    style="position: absolute; right:25px;"> Tambah Data</a>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Organisasi</th>
                                <th>Jabatan Organisasi</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($data as $ko)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$ko->nama_organisasi }}</td>
                                <td>{{$ko->jabatan}}</td>
                                <td>{{$ko->tgl_mulai}}</td>
                                <td>{{$ko->tgl_selesai}}</td>
                                <td>
                                    <form method="POST"
                                        action="{{route('ketorganisasi.destroy',['ketorganisasi'=>$ko->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{route('ketorganisasi.edit',['ketorganisasi'=>$ko->id])}}"
                                            class="btn btn-icon btn-left btn-warning"><i class="far fa-edit"></i>
                                            Edit</a>
                                        <button type="submit" class="btn btn-icon btn-left btn-danger"><i
                                                class="fas fa-times"></i>
                                            Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>



@endsection