<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Pendformal extends Model
{
    use HasFactory;

    protected $table = 'pend_formal';
    protected $fillable = [
        'id_guru',
        'tingkat',
        'nama_sekolah',
        'akreditasi',
        'tempat',
        'nomor_ijazah',
        'tanggal',
        'pejabat_ttd',
        'gelar_depan',
        'gelar_belakang'
    ];
}
