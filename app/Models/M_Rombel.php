<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Rombel extends Model
{
    use HasFactory;

    protected $table = 'rombel';
    protected $fillable = [
        'id_siswa',
        'id_angkatan'
    ];
}
