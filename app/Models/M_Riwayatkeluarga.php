<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Riwayatkeluarga extends Model
{
    use HasFactory;

    protected $table = 'riwayat_keluarga';
    protected $fillable = [
        'id_guru',
        'nik',
        'nip',
        'nama',
        'tempat',
        'tgl',
        'pekerjaan',
        'perusahaan',
        'status_perkawinan',
        'akta_nikah',
        'tgl_menikah',
        'status_hidup',
        'role'
    ];
}
