<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Tahunajaran extends Model
{
    use HasFactory;

    protected $table = 'tahunajaran';
    protected $fillable = [
        'tahun',
        'semester',
        'status'
    ];

    protected $hidden;
}
