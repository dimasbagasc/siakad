<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\M_guru;
use App\Models\M_Siswa;

class DatauserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = user::all();
        // echo '<pre>';
        // print_r($data->toArray());
        // exit;
        return view('admin.datauser')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = user::findOrFail($id);

        if ($user->role == 'siswa') {
            $data = DB::table('users')
                ->join('siswa', 'users.id', 'siswa.id_user')
                ->select('users.*', 'siswa.nisn', 'siswa.foto', 'siswa.status')
                ->where('users.id', '=', $id)
                ->first();
        }

        if ($user->role == 'guru') {
            $data = DB::table('users')
                ->join('guru', 'users.id', 'guru.id_user')
                ->select('guru.*', 'guru.nip', 'guru.foto', 'guru.status')
                ->where('users.id', '=', $id)
                ->first();
        }

        if ($user->role == 'admin') {
            $data = DB::table('users')
                ->join('admin', 'users.id', 'admin.id_user')
                ->select('admin.*', 'admin.nama')
                ->where('users.id', '=', $id)
                ->first();
        }

        

        return view('admin.showuser')->with([
            'data' => $data,
            'users' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = user::findOrFail($id);
        $guru = M_guru::findOrFail($id);
        $siswa = M_Siswa::findOrfail($id);
        return view('admin.edituser')->with([
            'data' => $data,
            'guru' => $guru,
            'siswa' => $siswa
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = user::findOrFail($id);
        $data = $request->except(['_token']);
        $item->update($data);
        return redirect('/datauser')->with(['success' => 'Data Berhasil Di Perbarui.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $item = user::findOrFail($id);
            $item->delete();
            return redirect('/datauser')->with(['success' => 'Data Berhasil Di Hapus.']);
        } catch (\Throwable $th) {
            return redirect('/datauser')->with(['error' => 'Data Sudah Digunakan.']);
        }
    }
}
