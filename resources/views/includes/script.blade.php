<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
  integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
  integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="template/assets/js/stisla.js"></script>

<!-- JS Libraies -->
<script src="{{asset('template/node_modules/simpleweather/jquery.simpleWeather.min.js')}}"></script>
{{-- <script src="{{asset('template/node_modules/chart.js/dist/Chart.min.js')}}"></script> --}}
<script src="{{asset('template/node_modules/jqvmap/dist/jquery.vmap.min.js')}}"></script>
<script src="{{asset('template/node_modules/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
<script src="{{asset('template/node_modules/summernote/dist/summernote-bs4.js')}}"></script>
<script src="{{asset('template/node_modules/chocolat/dist/js/jquery.chocolat.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>


<!-- Template JS File -->
<script src="{{asset('template/assets/js/scripts.js')}}"></script>
<script src="{{asset('template/assets/js/custom.js')}}"></script>

<!-- Page Specific JS File -->
<script src="{{asset('template/assets/js/page/index-0.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js">
</script>
<script type="text/javascript" charset="utf8"
  src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.11.2/jquery.mask.min.js"
  integrity="sha512-Y/GIYsd+LaQm6bGysIClyez2HGCIN1yrs94wUrHoRAD5RSURkqqVQEU6mM51O90hqS80ABFTGtiDpSXd2O05nw=="
  crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.all.min.js"></script>

<script>
  $(document).ready(function() {
    $('.select2_dropdown').select2({
      placeholder: "--Pilih Data--",
      allowClear: true
    });

    $('.datatable_table').DataTable({
        autoWidth: false,
        responsive: true,
    });

    $('.timepicker').mask('00:00');
    
});
</script>

@if(session('success'))
<script>
  $( document ).ready(function() {
        Swal.fire({
            title: 'Sukses!',
            text: "{{session('success')}}",
            icon: 'success',
        });

        
    });
</script>
@endif

@if(session('error'))
<script>
  $( document ).ready(function() {
        Swal.fire({
            title: 'Gagal!',
            text: "{{session('error')}}",
            icon: 'error',
        });

        
    });
</script>
@endif

@if(session('warning'))
<script>
  $( document ).ready(function() {
        Swal.fire({
            title: 'Perhatian!',
            text: "{{session('warning')}}",
            icon: 'warning',
        });

        
    });
</script>
@endif


<script>
  $(document).ready(function(){
  @stack('js')
})
</script>

@stack('js_script')