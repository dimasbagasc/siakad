@extends('layout.master')
@extends('layout.sidebarsiswa')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Nilai</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                @if ($nilai->isNotEmpty())
                <a href="{{route('nilaisiswa.cetakrapot')}}" class="btn btn-primary" target="_blank"
                    style="position: absolute; right:25px;">
                    Cetak Raport</a>
                @else
                <a href="#" class="btn btn-primary disabled" style="position: absolute; right:25px;">
                    Cetak Raport</a>
                @endif

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Mata Pelajaran</th>
                                <th>UTS</th>
                                <th>UAS</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($nilai as $n)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$n->matapelajaran}}</td>
                                <td>{{$n->nuts}}</td>
                                <td>{{$n->nuas}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@endsection