@php
use Illuminate\Support\Facades\DB;
@endphp
<!DOCTYPE html>
<html>

<head>
    <style>
        .with-border {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        .with-border td {
            border: 1px solid black;
            text-align: left;
            padding: 8px;
        }

        /* tr:nth-child(even) {
  background-color: #dddddd;
} */
        /* table,
        th,
        td {
            font-family: arial, sans-serif;
            border: 1px solid black;
            border-collapse: collapse;
            padding: 5px;
        } */
    </style>
    <title>Hi</title>
</head>

<body>
    <div class="card-body">
        <div class="card-header">
            <div>
                <table style="width: 50%; float: left">
                    <tr>
                        <th style="border: 0px; text-align:left">Nama Sekolah</th>
                        <td>: Muhammadiyah Boarding School</td>
                    </tr>
                    <tr>
                        <th style="border: 0px; text-align:left">Alamat</th>
                        <td>: Jetis - Ponorogo</td>
                    </tr>
                    <tr>
                        <th style="border: 0px; text-align:left">Nama</th>
                        <td>: {{$siswa->nama}}</td>
                    </tr>
                    <tr>
                        <th style="border: 0px; text-align:left">NISN</th>
                        <td>: {{$siswa->nisn}}</td>
                    </tr>
                </table>
                <table style="width: 50%; float: right; text-align:left">
                    <tr>
                        <th style="border: 0px; text-align:left">Kelas</th>
                        <td>: {{$siswa->nama_kelas}}</td>
                    </tr>
                    <tr>
                        <th style="border: 0px; text-align:left">Semester</th>
                        <td>: {{$siswa->semester}}</td>
                    </tr>
                    <tr>
                        <th style="border: 0px; text-align:left">Tahun pelajaran</th>
                        <td>: {{$siswa->tahun}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <br>
        <br>
        <div class="table">
            <table class="with-border" style="margin-top: 150px">
                <thead>
                    <tr>
                        <td colspan="2" rowspan="2" style="text-align: center">Mata Pelajaran</td>
                        <td rowspan="2" colspan="2" style="text-align: center">Pengetahuan</td>
                        <td rowspan="2" colspan="2" style="text-align: center">Keterampilan</td>
                        <td colspan="2" style="text-align: center">Sikap Spiritual dan Sosial</td>
                    </tr>
                    <tr>
                        <td>Dalam Mapel</td>
                        <td>Antar Mapel</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="2">Kelompok 1</td>
                        <td>Angka</td>
                        <td>Huruf</td>
                        <td>Angka</td>
                        <td>Huruf</td>
                        <td>SB/B/C/K</td>
                        <td>Deskripsi</td>
                    </tr>
                    @php
                    $jumlah = 0;
                    foreach($data as $i => $item){
                    $jumlah++;
                    }
                    $isideskripsi = '';
                    @endphp
                    @foreach ($data as $i => $item)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $item->matapelajaran }}</td>
                        <td>
                            @php
                            $jumlah_bobot_pengetahuan = $item->bobot_np + $item->bobot_nuts + $item->bobot_nuas;
                            @endphp
                            {{ $rapot =
                            ((($item->np*$item->bobot_np)+($item->nuts*$item->bobot_nuts)+($item->nuas*$item->bobot_nuas))/$jumlah_bobot_pengetahuan)/100*$jumlah_bobot_pengetahuan
                            }}
                            @php
                            $konversi = DB::table('konversi_nilai')
                            ->where([
                            ['batas_bawah', '<', $rapot], ['batas_atas', '>=' , number_format($rapot,2)], ])->
                                where('jenis','nm')
                                ->first();
                                @endphp</td>
                        <td>{{$konversi->konversi}}</td>
                        <td>
                            @php
                            $jumlah_bobot_ketrampilan = $item->bobot_praktik + $item->bobot_porto + $item->bobot_proyek;
                            @endphp
                            {{ $rapot =
                            ((($item->nilai_praktik*$item->bobot_praktik)+($item->nilai_porto*$item->bobot_porto)+($item->nilai_proyek*$item->bobot_proyek))/$jumlah_bobot_ketrampilan)/100*$jumlah_bobot_ketrampilan}}
                            @php
                            $konversi = DB::table('konversi_nilai')
                            ->where([
                            ['batas_bawah', '<', $rapot], ['batas_atas', '>=' , number_format($rapot,2)], ])->
                                where('jenis','nm')
                                ->first();
                                @endphp

                        </td>
                        <td>{{$konversi->konversi}}</td>
                        <td>
                            @php
                            $konversi = DB::table('konversi_nilai')
                            ->where([
                            ['batas_bawah', '<=', $item->nilai_sikapmapel], ['batas_atas', '>' ,
                                $item->nilai_sikapmapel], ]) ->where('jenis','ns')
                                ->first();
                                // print_r($konversi);exit;
                                echo $konversi->konversi;
                                @endphp
                        </td>
                        @if ($i==1)
                        <td rowspan={{$countdatanilai}}>
                            @php
                            if($isideskripsi == ''){
                            echo $deskripsi->deskripsi;
                            $isideskripsi=$deskripsi->deskripsi;
                            }
                            @endphp
                        </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="table">
                <table class="with-border" style="margin-top: 30px">
                    <thead>
                        <tr>
                            <td style="text-align: center">Kegiatan Ektrakurikuler</td>
                            <td style="text-align: center">Nilai</td>
                            <td style="text-align: center">Keterangan</td>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i = 0; $i < 3; $i++) <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
            </div>

            <div class="table" style="width: 50%">
                <table class="with-border" style="margin-top: 30px">
                    <thead>
                        <tr>
                            <td style="text-align: center">Ketidakhadiran</td>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $alpha=0;
                        $ijin=0;
                        @endphp
                        @foreach ($countpresensi as $p)
                        $alpha += $p->alpha;
                        $ijin += $p->ijin;
                        @endforeach
                        <tr>
                            <td>Ijin/Sakit<span style="padding: 100px">= {{$ijin}}</span></td>
                        </tr>
                        <tr>
                            <td>Alpha<span style="padding: 120px">= {{$alpha}}</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="row">
                <div>
                    <table style="width: 500px; float: left; margin-top: 30px">
                        <tr>
                            <td style="border: 0px; text-align:left">Mengetahui :</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="border: 0px; text-align:left">Orangtua/Wali</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="border: 0px; text-align:left">
                                <hr style="width: 170px; float:left; margin-top: 90px">
                            </td>
                            <td></td>
                        </tr>
                    </table>
                    <table style="width: 350px; float: left; text-align:left; margin-top: 30px">
                        <tr>
                            <td style="border: 0px; text-align:left">
                                .....................,..............20....</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="border: 0px; text-align:left;">Wali Kelas,</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="border: 0px; height:20px;;"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="border: 0px; height:140px;;">
                                {{$walikelas->nama}}
                                <hr style="width: 170px; float:left; margin-top:70px"><br>
                                NIP {{$walikelas->nip}}
                            </td>
                            <td></td>
                        </tr>
                    </table>

                </div>
            </div>
            {{-- <div class="row">
                <div class="col-sm-12">
                    <hr style="width: 50%; float:left; margin-top: 80px">
                    <div style="border: 0px; text-align:left; margin-top: 90px">
                        NIP ....................................
                    </div>
                </div>
            </div> --}}
        </div>


        {{-- @foreach ($data as $item)
        Mapel : {{ $item->matapelajaran }} ||
        Pengetahuan : {{ $rapot =
        (($item->np*$item->bobot_np)+($item->nuts*$item->bobot_nuts)+($item->nuas*$item->bobot_nuas))/4 }} || Konversi :
        {{
        $rapot/100*4 }}
        <br />
        @endforeach --}}
</body>

</html>