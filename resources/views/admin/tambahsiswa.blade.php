@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Data Siswa</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-biodata-tab" data-toggle="tab" href="#nav-biodata"
                            role="tab" aria-controls="nav-biodata" aria-selected="true">Biodata</a>
                        <a class="nav-item nav-link" id="nav-akun-tab" data-toggle="tab" href="#nav-akun" role="tab"
                            aria-controls="nav-akun" aria-selected="false">Data Akun</a>
                        <a class="nav-item nav-link" id="nav-ortu-tab" data-toggle="tab" href="#nav-ortu" role="tab"
                            aria-controls="nav-ortu" aria-selected="false">Data Orangtua/Wali</a>
                    </div>
                </nav>
                <form method="POST" action="{{ route('siswa.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-biodata" role="tabpanel"
                            aria-labelledby="nav-biodata-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>NISN</label>
                                        <input type="text" name="nisn" class="form-control" value="{{ old('nisn') }}"
                                            required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" name="nama" class="form-control" value="{{ old('nama') }}"
                                            required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tempat Lahir</label>
                                        <input type="text" name="tempat_lahir" class="form-control"
                                            value="{{ old('tempat_lahir') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <input type="date" name="tgl_lahir" class="form-control"
                                            value="{{ old('tgl_lahir') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Jenis Kelamin</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="jk" id="jk1"
                                                value="laki-laki" {{ old('jk')=='laki-laki' ? 'checked' : '' }}>
                                            <label class="form-check-label" for="jk1">
                                                Laki-laki
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="jk" id="jk2"
                                                value="perempuan" {{ old('jk')=='perempuan' ? 'checked' : '' }}>
                                            <label class="form-check-label" for="jk2">
                                                Perempuan
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Agama</label>
                                        <select name="agama" class="form-control" required>
                                            <option value="islam" {{ old('agama')=='islam' ? 'selected' : '' }}>Islam
                                            </option>
                                            <option value="katholik" {{ old('agama')=='katholik' ? 'selected' : '' }}>
                                                Katholik</option>
                                            <option value="kristen" {{ old('agama')=='kristen' ? 'selected' : '' }}>
                                                Kristen</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea name="alamat" class="form-control" value="{{ old('alamat') }}"
                                            required></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Kodepos</label>
                                        <input type="text" name="kodepos" class="form-control"
                                            value="{{ old('kodepos') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Anak Ke-</label>
                                        <input type="text" name="anak_ke" class="form-control"
                                            value="{{ old('anak_ke') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jumlah Saudara</label>
                                        <input type="text" name="jml_saudara" class="form-control"
                                            value="{{ old('jml_saudara') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pendidikan Terakhir</label>
                                        <input type="text" name="akhir_pendidikan" class="form-control"
                                            value="{{ old('akhir_pendidikan') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Asal Sekolah</label>
                                        <input type="text" name="asal_sekolah" class="form-control"
                                            value="{{ old('asal_sekolah') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tahun diterima</label>
                                        <input type="text" name="thn_terima" class="form-control"
                                            value="{{ old('thn_terima') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Riwayat Penyakit</label>
                                        <input type="text" name="penyakit_diderita" class="form-control"
                                            value="{{ old('penyakit_diderita') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select name="status" class="form-control">
                                            <option value="aktif" selected>Aktif</option>
                                            <option value="nonaktif">Non Aktif</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Foto</label>
                                        <input type="file" name="foto" class="form-control"
                                            required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-akun" role="tabpanel" aria-labelledby="nav-akun-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                                            required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" name="username" class="form-control"
                                            value="{{ old('username') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="text" name="password" class="form-control"
                                            value="{{ old('password') }}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-ortu" role="tabpanel" aria-labelledby="nav-akun-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>NIK Ayah</label>
                                        <input type="text" name="nik_ayah" class="form-control"
                                            value="{{ old('nik_ayah') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Ayah</label>
                                        <input type="text" name="nama_ayah" class="form-control"
                                            value="{{ old('nama_ayah') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pekerjaan Ayah</label>
                                        <input type="text" name="pekerjaan_ayah" class="form-control"
                                            value="{{ old('pekerjaan_ayah') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>NIK Ibu</label>
                                        <input type="text" name="nik_ibu" class="form-control"
                                            value="{{ old('nik_ibu') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Ibu</label>
                                        <input type="text" name="nama_ibu" class="form-control"
                                            value="{{ old('nama_ibu') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Wali</label>
                                        <input type="text" name="nama_wali" class="form-control"
                                            value="{{ old('nama_wali') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Hubungan Wali</label>
                                        <input type="text" name="hubungan" class="form-control"
                                            value="{{ old('hubungan') }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Alamat Wali</label>
                                        <textarea name="alamat_wali" class="form-control"
                                            value="{{ old('alamat_wali') }}" required></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nomor Handphone Wali</label>
                                        <input type="text" name="nohp" class="form-control" value="{{ old('nohp') }}"
                                            pattern="\d*" maxlength="12" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <a href="{{route('siswa.index')}}" class="btn btn-icon icon-left btn-warning">Kembali</a>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>

        </div>
</section>

@endsection