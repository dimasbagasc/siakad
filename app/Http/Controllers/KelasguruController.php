<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\M_guru;
use App\Models\M_Siswa;
use App\Models\M_ortu;
use App\Models\M_Walikelas;
use App\Models\M_kelas;

class KelasguruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahunAktif = getTahunAktif();
        $guru = M_guru::where('id_user', Auth::user()->id)->first();
        $angkatan = DB::table('walikelas')
            ->join('angkatan', 'walikelas.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->join('rombel', 'rombel.id_angkatan', '=', 'angkatan.id')
            ->join('siswa', 'rombel.id_siswa', '=', 'siswa.id')
            ->select('angkatan.*', 'kelas.nama_kelas', 'siswa.id as id_siswa', 'siswa.nisn', 'siswa.nama', 'tahunajaran.tahun', 'tahunajaran.semester')
            ->where('walikelas.id_guru', '=', $guru->id)
            ->where('tahunajaran.id', $tahunAktif->id)
            ->get();

        // echo "<pre>";
        // print_r($angkatan);
        // exit;

        return view('guru.datakelasguru')->with([
            'angkatan' => $angkatan,
            'tahunAktif' => $tahunAktif
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = M_Siswa::findOrFail($id);
        // echo "<pre>";
        // print_r($data);
        // exit;
        $dataOrtu = M_Ortu::findOrFail($id);
        return view('guru.showkontaksiswa')->with([
            'data' => $data,
            'ortu' => $dataOrtu
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
