@extends('layout.master')
@extends('layout.sidebarsiswa')

@section('container')
<section class="section">
    <br>
    <div class="section-header">
        <h1>Dashboard</h1>
    </div>
    @if(session('error_akses'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{session('error_akses')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <div class="alert alert-success alert-dismissible fade show" role="alert">

        Selamat Datang di Sistem Akademik Muhammadiyah Boarding School sebagai siswa.
        {{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button> --}}
    </div>

    {{-- <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                    <i class="fas fa-book-open"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Absensi</h4>
                    </div>
                    <div class="card-body">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                    <i class="fas fas fa-bookmark"><a href="{{route('mapel.index')}}"></a></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Mapel</h4>
                    </div>
                    <div class="card-body">

                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    @endsection