<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mail;
use App\Mail\NotifyMail;
use App\Models\User;
use Illuminate\Support\Facades\Mail as FacadesMail;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }

    public function proses_login(Request $request)
    {
        request()->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $kredensil = $request->only('username', 'password');

        if (Auth::attempt($kredensil)) {
            $user = Auth::user();
            if ($user->role == 'admin') {
                return redirect()->intended('dashboardadmin');
            } elseif ($user->role == 'guru') {
                $data = DB::table('users')
                    ->join('guru', 'users.id', 'guru.id_user')
                    ->select('guru.status')
                    ->where('users.id', '=', $user['id'])
                    ->first();
                if ($data->status != 'aktif') {
                    return redirect('/login')->with(['error_status' => 'Akun Anda Belum Aktif, Silahkan Hubungi Admin']);
                }
                return redirect()->intended('dashboardguru');
            } elseif ($user->role == 'siswa') {
                $data = DB::table('users')
                    ->join('siswa', 'users.id', 'siswa.id_user')
                    ->select('siswa.status')
                    ->where('users.id', '=', $user['id'])
                    ->first();
                if ($data->status != 'aktif') {
                    return redirect('/login')->with(['error_status' => 'Akun Anda Belum Aktif, Silahkan Hubungi Admin']);
                }
                return redirect()->intended('dashboardsiswa')->with(['alert' => 'Selamat Datang di SIAKAD Muhammadiyah BOarding School anda sebagai siswa ']);
            } else {

                return redirect()->intended('/');
            }
        }
        return redirect('login')->with(['error_login' => 'Username atau Password Salah!']);
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return redirect('login');
    }

    public function aktivasi_akun(Request $request)
    {
        $data = DB::table('users')
            ->where('remember_token', $request['remember_token'])
            ->first();

        if (!empty($data)) {
            $user = [
                'is_aktif' => 1
            ];

            $data->update($user);

            return redirect('login')->with(['success' => 'Selamat akun anda telah aktif. Silahkan Login!']);
        } else {
            return redirect('login')->with(['error' => 'Token tidak ditemukan. Silahkan Hubungi Admin!']);
        }
    }

    public function index_lupapassword(Request $request)
    {
        return view('lupapassword');
    }

    public function forgot_password(Request $request)
    {
        $data = $request->except(['_token']);

        $user = DB::table('users')
            ->where('username', $data['username'])
            ->first();

        if (!empty($data)) {
            // data user ditemukan
            if ($user->role == 'siswa') {
                $email = DB::table('users')
                    ->join('siswa', 'users.id', 'siswa.id_user')
                    ->select('siswa.email')
                    ->where('users.id', '=', $user->id)
                    ->first();
            }

            if ($user->role == 'guru') {
                $email = DB::table('users')
                    ->join('guru', 'users.id', 'guru.id_user')
                    ->select('guru.email')
                    ->where('users.id', '=', $user->id)
                    ->first();
            }

            if (!empty($email)) {
                // data email ditemukan
                // update remember token di table user, kemudian kirim email url reset password + token
                $token = Str::random(40);
                $item = User::findOrFail($user->id);
                $data = [
                    'remember_token' => $token
                ];
                $item->update($data);

                $pesan = [
                    'url' => route('reset_password', ['token' => $token])
                ];
                FacadesMail::to($email->email)->send(new NotifyMail($pesan));
                // dd(FacadesMail::failures());
                // if (FacadesMail::failures()) {
                //     return response()->Fail('Sorry! Please try again latter');
                // } else {
                //     return response()->success('Great! Successfully send in your mail');
                // }
            }
            // ketika tidak menemukan data email
        }
        // ketika tidak menemukan data user
    }

    public function reset_password(Request $request)
    {
        $data = $request->except(['_token']);

        if (!isset($data['token'])) {
            return redirect('login')->with(['error' => 'Token tidak ditemukan!']);
        }

        $data = DB::table('users')
            ->where('remember_token', $data['token'])
            ->first();

        if (!empty($data)) {
            return view('resetpassword')->with([
                'data' => $data
            ]);
        }
        return redirect('login')->with(['error' => 'Token tidak ditemukan!']);
        // tampilan reset password, form new password + confirm password
    }

    public function new_password(Request $request)
    {
        $data = $request->except(['_token']);

        if ($data['password'] != $data['confirm-password']) {
            return redirect('reset_password')->with(['error' => 'Password tidak sama']);
        }

        $update_password = [
            'password' => bcrypt($data['password']),
            'remember_token' => null
        ];
        $item = User::findOrFail($data['id']);
        $item->update($update_password);
        return redirect('login')->with(['success' => 'Password Berhasil Di Perbarui']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
