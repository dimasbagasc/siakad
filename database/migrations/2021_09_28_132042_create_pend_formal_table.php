<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendFormalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pend_formal', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_guru');
            $table->foreign('id_guru')->references('id')->on('guru');

            $table->string('tingkat');
            $table->string('nama_sekolah');
            $table->string('akreditasi');
            $table->string('tempat');
            $table->string('nomor_ijazah');
            $table->date('tanggal');
            $table->string('pejabat_ttd');
            $table->string('gelar_depan');
            $table->string('gelar_belakang');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pend_informal');
    }
}
