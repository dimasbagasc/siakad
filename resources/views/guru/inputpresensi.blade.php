@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')
@php
use Carbon\Carbon;
setlocale(LC_TIME, 'id_ID');
\Carbon\Carbon::setLocale('id');
\Carbon\Carbon::now()->formatLocalized("%A, %d %B %Y");
@endphp

<section class="section">
    <br>
    <div class="section-header">
        <h1>Presensi Siswa</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4>
                    {{ Carbon::now()->isoFormat('dddd, D MMMM Y'); }}
                </h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>Kelas</th>
                            <td>{{$jadwal->nama_kelas}}</td>
                        </tr>
                        <tr>
                            <th>Guru Mapel</th>
                            <td>{{$jadwal->nama}}</td>
                        </tr>
                        <tr>
                            <th>Mata Pelajaran</th>
                            <td>{{$jadwal->matapelajaran}}</td>
                        </tr>
                    </table>
                    
                    <form method="POST" action="{{route('presensi.store')}}">
                        @csrf
                        <input type="hidden" value="{{$id_jadwal}}" name="id_jadwal">
                        <table class="table table-striped table-md">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NISN</th>
                                    <th>Nama</th>
                                    <th>Aksi</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($siswa as $i=> $s)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{ $s->nisn }}</td>
                                    <td>{{ $s->nama}}</td>
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio"
                                                name="presensi[{{$s->id}}][status]" id="presensi[{{$s->id}}][status]"
                                                value="hadir" @php if(isset($presensi[$s->id]['status']))
                                            {
                                            if ($presensi[$s->id]['status']=='hadir') {
                                            echo "checked";
                                            }
                                            }
                                            @endphp>
                                            <label class="form-check-label" for="presensi[{{$s->id}}][status]">
                                                Hadir
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio"
                                                name="presensi[{{$s->id}}][status]" id="presensi[{{$s->id}}][status]"
                                                value="alpha" @php if(isset($presensi[$s->id]['status']))
                                            {
                                            if ($presensi[$s->id]['status']=='alpha') {
                                            echo "checked";
                                            }
                                            }
                                            @endphp>
                                            <label class="form-check-label" for="presensi[{{$s->id}}][status]">
                                                Alpha
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio"
                                                name="presensi[{{$s->id}}][status]" id="presensi[{{$s->id}}][status]"
                                                value="ijin" @php if(isset($presensi[$s->id]['status']))
                                            {
                                            if ($presensi[$s->id]['status']=='ijin') {
                                            echo "checked";
                                            }
                                            }
                                            @endphp>
                                            <label class="form-check-label" for="presensi[{{$s->id}}][status]">
                                                Ijin
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <textarea name="presensi[{{$s->id}}][keterangan]" class="form-control">@php
                                            if(isset($presensi[$s->id]['keterangan']))
                                            {
                                                echo $presensi[$s->id]['keterangan'];
                                            }
                                        @endphp</textarea>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>



@endsection