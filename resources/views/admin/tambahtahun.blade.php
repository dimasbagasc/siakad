@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
  <br>
  <div class="section-header">
    <h1>Tambah Tahun Ajaran</h1>
  </div>

  <div class="card">
    <div class="card-body">
      <form action="{{route('tahunajaran.store')}}" method="POST">
        @csrf
        <div class="form-group">
          <label>Tahun Ajaran</label>
          <input type="text" name="tahun" class="form-control" placeholder="Tahun Ajaran" required>
        </div>
        <div class="form-group">
          <label class="d-block" for="status">Semester</label>
          <select class="form-control select2_dropdown" name="semester" id="semester" required>
            <option></option>
            <option value="ganjil">Ganjil</option>
            <option value="genap">Genap</option>
          </select>
        </div>
        <div class="form-group">
          <label class="d-block" for="status">Status</label>
          <select class="form-control select2_dropdown" name="status" id="status" required>
            <option></option>
            <option value="aktif">Aktif</option>
            <option value="nonaktif">Non Aktif</option>
          </select>
        </div>
        <div class="form-group">
          <a href="{{route('tahunajaran.index')}}" class="btn btn-icon icon-left btn-warning"> Kembali</a>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</section>

@endsection