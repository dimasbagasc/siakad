@extends('layout.master')
@extends('layout.sidebarsiswa')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Presensi</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Mata Pelajaran</th>
                                <th>Guru Mapel</th>
                                <th>Hadir</th>
                                <th>Alpha</th>
                                <th>Ijin</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($countpresensi as $p)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$p->matapelajaran}}</td>
                                <td>{{$p->nama}}</td>
                                <td>{{$p->hadir}}</td>
                                <td>{{$p->alpha}}</td>
                                <td>{{$p->ijin}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@endsection