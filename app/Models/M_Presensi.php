<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Presensi extends Model
{
    use HasFactory;

    protected $table = 'presensi';
    protected $fillable = [
        'id_rombel',
        'id_jadwal',
        'tanggal',
        'keterangan',
        'status'
    ];

    protected $hidden;
}
