@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Data Riwayat Keluarga</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('riwayatkeluarga.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label>NIK</label>
                    <input type="text" name="nik" class="form-control" placeholder="Nomor NIK" required>
                </div>
                <div class="form-group">
                    <label>NIP</label>
                    <input type="text" name="nip" class="form-control" placeholder="Nomor NIP" required>
                </div>
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" required>
                </div>
                <div class="form-group">
                    <label>Tempat Lahir</label>
                    <input type="text" name="tempat" class="form-control" placeholder="Tempat Lahir" required>
                </div>
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="date" name="tgl" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Pekerjaan</label>
                    <input type="text" name="pekerjaan" class="form-control" placeholder="Pekerjaan" required>
                </div>
                <div class="form-group">
                    <label>Perusahaan</label>
                    <input type="text" name="perusahaan" class="form-control" placeholder="Nama Perusahaan" required>
                </div>
                <div class="form-group">
                    <label>Status Perkawinan</label>
                    <select name="status_perkawinan" class="form-control" placeholder="Status Perkawinan" srequired>
                        <option></option>
                        <option value="kawin">Kawin</option>
                        <option value="belum_kawin">Belum Kawin</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Akta Nikah</label>
                    <input type="text" name="akta_nikah" class="form-control" placeholder="Nomor Akta Pernikahan"
                        required>
                </div>
                <div class="form-group">
                    <label>Tanggal Pernikahan</label>
                    <input type="date" name="tgl_menikah" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Status Hidup</label>
                    <select name="status_hidup" class="form-control" required>
                        <option></option>
                        <option value="hidup">Hidup</option>
                        <option value="mati">Mati</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Role</label>
                    <select name="role" class="form-control" required>
                        <option></option>
                        <option value="suami">Suami</option>
                        <option value="isteri">Isteri</option>
                        <option value="anak">Anak</option>
                        <option value="orangtua_kandung">Orangtua Kandung</option>
                        <option value="saudara_kandung">Saudara Kandung</option>
                        <option value="bapak_mertua">Bapak Mertua</option>
                        <option value="ibu_mertua">Ibu Mertua</option>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection