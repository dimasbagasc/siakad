@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Edit Guru Mapel</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('gurumapel.update', ['gurumapel'=>$data->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label class="d-block" for="id_guru">Guru</label>
                    <select class="form-control select2_dropdown" name="id_guru" id="id_guru">
                        <option></option>
                        @foreach ($guru as $v)
                        <option value="{{ $v->id }}" {{($v->id == $data->id_guru ? 'selected' : '')}}>{{ $v->nama }}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="d-block" for="id_mapel">Mata Pelajaran</label>
                    <select class="form-control select2_dropdown" name="id_mapel" id="id_mapel">
                        <option></option>
                        @foreach ($mapel as $k)
                        <option value="{{ $k->id }}" {{($k->id == $data->id_mapel ? 'selected' : '')}}>{{
                            $k->matapelajaran }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection