<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Deskripsi extends Model
{
    use HasFactory;

    protected $table = 'deskripsi';
    protected $fillable = [
        'id_rombel',
        'deskripsi'
    ];
    
}
