<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_rombel');
            $table->foreign('id_rombel')->references('id')->on('rombel');

            $table->unsignedBigInteger('id_gurumapel');
            $table->foreign('id_gurumapel')->references('id')->on('gurumapel');

            $table->float('np');
            $table->float('nuts');
            $table->float('nuas');
            $table->integer('bobot_np');
            $table->integer('bobot_nuts');
            $table->integer('bobot_nuas');
            $table->float('nilai_praktik');
            $table->float('nilai_porto');
            $table->float('nilai_proyek');
            $table->integer('bobot_praktik');
            $table->integer('bobot_porto');
            $table->integer('bobot_proyek');
            $table->float('nilai_sikapmapel');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai');
    }
}
