<?php

namespace App\Http\Controllers;

use App\Models\M_guru;
use App\Models\M_Sesi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class JadwalguruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahunAktif = getTahunAktif();
        $guru = M_guru::where('id_user', Auth::user()->id)->first();
        $datajadwal = DB::table('jadwal')
            ->join('gurumapel', 'jadwal.id_guru', '=', 'gurumapel.id')
            ->join('sesi', 'jadwal.id_sesi', '=', 'sesi.id')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->join('angkatan', 'jadwal.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('jadwal.*', 'guru.nama', 'sesi.sesi', 'sesi.jam_mulai', 'sesi.jam_selesai', 'mapel.matapelajaran', 'kelas.nama_kelas')
            ->where('guru.id', '=', $guru->id)
            ->where('tahunajaran.id', $tahunAktif->id)
            ->orderBy('sesi.sesi', 'asc')
            ->orderBy('jadwal.hari', 'asc')
            ->get()
            ->toArray();

        $sesi = M_Sesi::all();

        $jadwal = [];
        foreach ($datajadwal as $key => $value) {
            $jadwal[$value->sesi][$value->hari][] = $datajadwal[$key];
        }
        // echo '<pre>';
        // print_r($datajadwal);
        // exit;

        return view('guru.jadwalguru')->with([
            'jadwal' => $jadwal,
            'sesi' => $sesi,
            'tahunAktif' => $tahunAktif
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
