@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Presensi Siswa</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>Kelas</th>
                            <td>{{$jadwal->nama_kelas}}</td>
                        </tr>
                        <tr>
                            <th>Guru Mapel</th>
                            <td>{{$jadwal->nama}}</td>
                        </tr>
                        <tr>
                            <th>Mata Pelajaran</th>
                            <td>{{$jadwal->matapelajaran}}</td>
                        </tr>
                    </table>
                    <table class="table table-striped table-md">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NISN</th>
                                <th>Nama</th>
                                <th>Hadir</th>
                                <th>Alpha</th>
                                <th>Ijin</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($presensi as $i=> $s)
                            <tr>
                                <td>{{++$i}}</td>
                                <td>{{ $s->nisn }}</td>
                                <td>{{ $s->nama}}</td>
                                <td>{{$s->hadir}}</td>
                                <td>{{$s->alpha}}</td>
                                <td>{{$s->ijin}}</td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
    </div>
</section>



@endsection