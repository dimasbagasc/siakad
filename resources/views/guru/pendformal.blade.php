@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Pendidikan Formal</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">

                <a href="{{route('kepegawaian.index')}}" class="btn btn-warning fas fa-angle-left"> Kembali</a>
                <a href="{{route('pendformal.create')}}" class="btn btn-primary fa fa-plus"
                    style="position: absolute; right:25px;"> Tambah Data</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tingkat</th>
                                <th>Nama Sekolah/Perguruan Negeri</th>
                                <th>Akreditasi</th>
                                <th>Tempat</th>
                                <th>Nomor Ijazah</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($data as $formal)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$formal->tingkat }}</td>
                                <td>{{$formal->nama_sekolah}}</td>
                                <td>{{$formal->akreditasi}}</td>
                                <td>{{$formal->tempat}}</td>
                                <td>{{$formal->nomor_ijazah}}</td>
                                <td>
                                    <form method="POST"
                                        action="{{route('pendformal.destroy',['pendformal'=>$formal->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{route('pendformal.edit',['pendformal'=>$formal->id])}}"
                                            class="btn btn-icon btn-left btn-warning"><i class="far fa-edit"></i>
                                            Edit</a>
                                        <button type="submit" class="btn btn-icon btn-left btn-danger"><i
                                                class="fas fa-times"></i>
                                            Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>



@endsection