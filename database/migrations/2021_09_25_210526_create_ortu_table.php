<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrtuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ortu', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_siswa');
            $table->string('nama_ayah');
            $table->integer('nik_ayah');
            $table->string('pekerjaan_ayah');
            $table->string('nama_ibu');
            $table->integer('nik_ibu');
            $table->string('nama_wali');
            $table->string('hubungan');
            $table->string('alamat_wali');
            $table->integer('nohp');
            $table->timestamps();

            $table->foreign('id_siswa')->references('id')->on('siswa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ortu');
    }
}
