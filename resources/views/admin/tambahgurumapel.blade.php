@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Guru Mapel</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('gurumapel.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label class="d-block" for="id_guru">Guru</label>
                    <select class="form-control select2_dropdown" name="id_guru" id="id_guru">
                        <option></option>
                        @foreach ($id_guru as $v)
                        <option value="{{ $v->id }}">{{ $v->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="d-block" for="id_mapel">Mata Pelajaran</label>
                    <select class="form-control select2_dropdown" name="id_mapel" id="id_mapel">
                        <option></option>
                        @foreach ($id_mapel as $k)
                        <option value="{{ $k->id }}">{{ $k->matapelajaran }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <a href="{{route('gurumapel.index')}}" class="btn btn-icon icon-left btn-warning">Kembali</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection