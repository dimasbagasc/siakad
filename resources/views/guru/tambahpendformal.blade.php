@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Data Pendidikan Informal</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('pendformal.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label>Tingkat</label>
                    <input type="text" name="tingkat" class="form-control" placeholder="Tingkat" required>
                </div>
                <div class="form-group">
                    <label>Nama Sekolah/Perguruan Negeri</label>
                    <input type="text" name="nama_sekolah" class="form-control"
                        placeholder="Nama Sekolah / Perguruan Negeri" required>
                </div>
                <div class="form-group">
                    <label>Akreditasi</label>
                    <input type="text" name="akreditasi" class="form-control" placeholder="Akreditasi" required>
                </div>
                <div class="form-group">
                    <label>Tempat</label>
                    <input type="text" name="tempat" class="form-control" placeholder="Tempat" required>
                </div>
                <div class="form-group">
                    <label>Nomor</label>
                    <input type="text" name="nomor_ijazah" class="form-control" placeholder="Nomor" required>
                </div>
                <div class="form-group">
                    <label>Tanggal</label>
                    <input type="date" name="tanggal" class="form-control" placeholder="Tanggal Kelulusan" required>
                </div>
                <div class="form-group">
                    <label>Pejabat Penandatangan</label>
                    <input type="text" name="pejabat_ttd" class="form-control" placeholder="Pejabat Penandatangan"
                        required>
                </div>
                <div class="form-group">
                    <label>Gelar Depan</label>
                    <input type="text" name="gelar_depan" class="form-control" placeholder="Gelar Depan" required>
                </div>
                <div class="form-group">
                    <label>Gelar Belakang</label>
                    <input type="text" name="gelar_belakang" class="form-control" placeholder="Gelar Belakang" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection