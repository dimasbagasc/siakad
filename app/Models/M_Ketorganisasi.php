<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Ketorganisasi extends Model
{
    use HasFactory;

    protected $table = 'organisasi_guru';
    protected $fillable = [
        'id_guru',
        'nama_organisasi',
        'jabatan',
        'tgl_mulai',
        'tgl_selesai',
        'tempat',
        'pemimpin'
    ];
}
