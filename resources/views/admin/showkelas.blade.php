@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
  <br>
  <div class="section-header">
    <h1>Tambah Data Kelas</h1>
  </div>

  <div class="card">
    <div class="card-body">
      <form action="{{route('kelas.update',['kela'=>$data->id])}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label>Nama Kelas</label>
          <input type="text" name="nama_kelas" class="form-control" placeholder="Nama Kelas"
            value="{{$data->nama_kelas}}">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</section>

@endsection