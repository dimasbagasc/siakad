@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Data Nilai Siswa</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Mata Pelajaran</th>
                                <th>UTS</th>
                                <th>UAS</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($nilai as $n)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$n->matapelajaran}}</td>
                                <td>{{$n->nuts}}</td>
                                <td>{{$n->nuas}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                    <form method="POST" action="{{route('datanilaisiswa.deskripsi')}}">
                        @csrf
                        <input type="hidden" value="{{$data->id}}" name="id_rombel">
                        <table class="table table-striped table-md col-md-6">
                            <thead>
                                <tr>
                                    <th>Deskripsi Raport</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <textarea name="deskripsi" class="form-control" requiredl>
                                            {{(!empty($deskripsi) ? $deskripsi->deskripsi:"")}}
                                        </textarea>
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                </div>

            </div>
        </div>
    </div>
</section>



@endsection