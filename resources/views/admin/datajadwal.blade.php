@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Data Jadwal {{$tahunAktif->tahun . '('.$tahunAktif->semester.')'}}</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kelas</th>
                                <th>Tahun Ajaran</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($data as $j)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{ $j->nama_kelas }}</td>
                                <td>{{ $j->tahun.'('.$j->semester.')' }}</td>
                                <td>
                                    <a href="{{route('jadwal.create', ['id_angkatan'=>$j->id])}}"
                                        class="btn btn-icon btn-left btn-info"><i class="fas fa-info-circle"></i> Kelola
                                        Jadwal</a>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@endsection