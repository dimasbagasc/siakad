<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_guru = Auth::user()->id;

        $pend_formal = DB::table('pend_formal')
            ->join('guru', 'guru.id', 'pend_formal.id_guru')
            ->where('guru.id_user', '=', $id_guru)
            ->count();

        $pend_informal = DB::table('pend_informal')
            ->join('guru', 'guru.id', 'pend_informal.id_guru')
            ->where('guru.id_user', '=', $id_guru)
            ->count();

        $riwayat_pekerjaan = DB::table('riwayat_pekerjaan')
            ->join('guru', 'guru.id', 'riwayat_pekerjaan.id_guru')
            ->where('guru.id_user', '=', $id_guru)
            ->count();

        $penghargaan = DB::table('penghargaan')
            ->join('guru', 'guru.id', 'penghargaan.id_guru')
            ->where('guru.id_user', '=', $id_guru)
            ->count();

        $organisasi = DB::table('organisasi_guru')
            ->join('guru', 'guru.id', 'organisasi_guru.id_guru')
            ->where('guru.id_user', '=', $id_guru)
            ->count();

        $keteranganlain = DB::table('keterangan_lain')
            ->join('guru', 'guru.id', 'keterangan_lain.id_guru')
            ->where('guru.id_user', '=', $id_guru)
            ->count();

        $jumlahsiswa = DB::table('angkatan')
            ->join('walikelas', 'angkatan.id', 'walikelas.id_angkatan')
            ->join('guru', 'walikelas.id_guru', 'guru.id')
            ->where('guru.id_user', '=', $id_guru)
            ->count();

        $jumlahmapel = DB::table('mapel')
            ->join('gurumapel', 'mapel.id', 'gurumapel.id_mapel')
            ->join('guru', 'gurumapel.id_guru', 'guru.id')
            ->where('guru.id_user', '=', $id_guru)
            ->count();

        // echo $jumlahsiswa;
        // exit;
        return view('guru.guru')->with([
            'countpend_formal' => $pend_formal,
            'countpend_informal' => $pend_informal,
            'countrpekerjaan' => $riwayat_pekerjaan,
            'countpenghargaan' => $penghargaan,
            'countorganisasi_guru' => $organisasi,
            'countketerangan_lain' => $keteranganlain,
            'countjumlahsiswa' => $jumlahsiswa,
            'countjumlahmapel' => $jumlahmapel
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
