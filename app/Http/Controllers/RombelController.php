<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\M_Siswa;
use App\Models\M_Rombel;
use Illuminate\Support\Facades\Redirect;

class RombelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = DB::table('rombel')
            ->join('siswa', 'rombel.id_siswa', '=', 'siswa.id')
            ->where('id_angkatan', $request->id_angkatan)
            ->select('rombel.*', 'siswa.nisn', 'siswa.nama')
            ->get();

        $walikelas = DB::table('walikelas')
            ->join('guru', 'walikelas.id_guru', '=', 'guru.id')

            ->where('walikelas.id_angkatan', $request->id_angkatan)
            ->where('walikelas.status', 'aktif')
            ->select('guru.nama')
            ->first();

        // echo '<pre>';
        // print_r($walikelas);
        // exit;

        $angkatan = DB::table('angkatan')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->where('angkatan.id', $request->id_angkatan)
            ->select('angkatan.*', 'kelas.nama_kelas', 'tahunajaran.tahun')
            ->first();

        return view('admin.datarombel')->with([
            'data' => $data,
            'angkatan' => $angkatan,
            'walikelas' => $walikelas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // siswa yang sudah masuk ke dalam rombel
        $tahunAktif = getTahunAktif();
        $siswa_angkatan = DB::table('rombel')
            // ->where('id_angkatan', $request->id_angkatan)
            ->join('angkatan', 'rombel.id_angkatan', 'angkatan.id')
            ->select('id_siswa')
            ->where('angkatan.id_tahunajaran', $tahunAktif->id)
            ->get();

        // mengubah data ke dalam array
        $siswaArray = [];
        foreach ($siswa_angkatan as $key => $value) {
            $siswaArray[] = $value->id_siswa;
        }

        // echo '<pre>';
        // print_r($siswaArray);
        // exit;

        // siswa yang belum masuk ke dalam rombel
        $data = DB::table('siswa')
            ->whereNotIn('id', $siswaArray)
            ->select('*')
            ->get();

        return view('admin.rombelsiswa')->with([
            'data' => $data,
            'id_angkatan' => $request['id_angkatan'],
            'tahunAktif' => $tahunAktif
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);

        foreach ($data['cek'] as $key => $value) {
            $rombel = [
                'id_siswa' => $value,
                'id_angkatan' => $data['id_angkatan']
            ];

            M_Rombel::create($rombel);
        }

        return redirect('rombel?id_angkatan=' . $data['id_angkatan'])->with(['success' => 'Data Berhasil Diproses.']);
        // return Redirect::route('rombel/create?id_angkatan', array('id_angkatan' => $data['id_angkatan']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $item = M_Rombel::findOrFail($id);
        $item->delete();
        return redirect('/rombel?id_angkatan=' . $request['id_angkatan'])->with(['success' => 'Data Berhasil Di Hapus']);
    }
}
