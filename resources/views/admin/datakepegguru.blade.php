@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Detail Data Kepegawaian</h1>
    </div>


    <div class="card">
        <div class="card-body">
            <a href="{{route('guru.index')}}" class="btn btn-icon icon-left btn-warning"><i
                    class="fas fa-arrow-left"></i>Kembali</a>
            <br>
            <br>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-pendidikan-tab" data-toggle="tab" href="#nav-pendidikan"
                        role="tab" aria-controls="nav-pendidikan" aria-selected="true">Pendidikan</a>
                    <a class="nav-item nav-link" id="nav-riwayat_pekerjaan-tab" data-toggle="tab"
                        href="#nav-riwayat_pekerjaan" role="tab" aria-controls="nav-riwayat_pekerjaan"
                        aria-selected="false">Riwayat Pekerjaan</a>
                    <a class="nav-item nav-link" id="nav-penghargaan-tab" data-toggle="tab" href="#nav-penghargaan"
                        role="tab" aria-controls="nav-penghargaan" aria-selected="false">Penghargaan</a>
                    <a class="nav-item nav-link" id="nav-riwayat_keluarga-tab" data-toggle="tab"
                        href="#nav-riwayat_keluarga" role="tab" aria-controls="nav-riwayat_keluarga"
                        aria-selected="false">Riwayat Keluarga</a>
                    <a class="nav-item nav-link" id="nav-organisasi-tab" data-toggle="tab" href="#nav-organisasi"
                        role="tab" aria-controls="nav-riwayat_keluarga" aria-selected="false">Keterangan Organisasi</a>
                    <a class="nav-item nav-link" id="nav-ketlain-tab" data-toggle="tab" href="#nav-ketlain" role="tab"
                        aria-controls="nav-ketlain" aria-selected="false">Keterangan Lain</a>
                </div>
            </nav>
            <br>
            {{-- <br> --}}
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-pendidikan" role="tabpanel"
                    aria-labelledby="nav-pendidikan-tab">
                    <div class="row">


                        <h4>1. Pendidikan didalam dan luar negeri</h4>

                        {{-- <tr>
                            <td colspan="2" style="text-align:center">
                                <img src="{{URL::to('/uploads').'/'.$data->foto}}" width="30%">
                            </td>
                        </tr> --}}
                        <div class="table-responsive">
                            <table class="table table-striped table-md datatable_table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tingkat</th>
                                        <th>Nama Sekolah</th>
                                        <th>Akreditasi</th>
                                        <th>Tempat</th>
                                        {{-- <th>STTB/IJAZAH</th>
                                        <th>Gelar</th> --}}
                                        <th>Nomor</th>
                                        <th>Tanggal</th>
                                        <th>Pejabat Penandatangan</th>
                                        <th>Depan</th>
                                        <th>Belakang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $no = 1; @endphp
                                    @foreach ($pend_formal as $pf)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{ $pf->tingkat }}</td>
                                        <td>{{ $pf->nama_sekolah }}</td>
                                        <td>{{ $pf->akreditasi }}</td>
                                        <td>{{ $pf->tempat }}</td>
                                        <td>{{ $pf->nomor_ijazah }}</td>
                                        <td>{{ $pf->tanggal }}</td>
                                        <td>{{ $pf->pejabat_ttd }}</td>
                                        <td>{{ $pf->gelar_depan}}</td>
                                        <td>{{ $pf->gelar_belakang}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>


                        <h4 style="margin-top: 40px;">2. Pendidikan didalam dan luar negeri</h4>

                        <div class="table-responsive">
                            <table class="table table-striped table-md datatable_table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Kursus/Latihan</th>
                                        <th>Tanggal Mulai</th>
                                        <th>Tanggal Selesai</th>
                                        <th>Nomor</th>
                                        <th>Tempat</th>
                                        <th>Institusi Penyelenggara</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $no = 1; @endphp
                                    @foreach ($pend_informal as $pinf)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{ $pinf->nama_kursus }}</td>
                                        <td>{{ $pinf->tgl_mulai }}</td>
                                        <td>{{ $pinf->tgl_selesai }}</td>
                                        <td>{{ $pinf->nomor_kursus }}</td>
                                        <td>{{ $pinf->tempat }}</td>
                                        <td>{{ $pinf->institusi }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--1-->
                <div class="tab-pane fade" id="nav-riwayat_pekerjaan" role="tabpanel"
                    aria-labelledby="nav-riwayat_pekerjaan-tab">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-striped table-md datatable_table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Instansi</th>
                                        <th>Jabatan</th>
                                        <th>Tanggal Mulai</th>
                                        <th>Tanggal Selesai</th>
                                        <th>Gaji Pokok</th>
                                        <th>Nomor</th>
                                        <th>Tanggal</th>
                                        <th>Penandatangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $no = 1; @endphp
                                    @foreach ($riwayat_pekerjaan as $rp)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{ $rp->instansi }}</td>
                                        <td>{{ $rp->jabatan }}</td>
                                        <td>{{ $rp->tgl_mulai }}</td>
                                        <td>{{ $rp->tgl_selesai }}</td>
                                        <td>{{ $rp->gaji_pokok }}</td>
                                        <td>{{ $rp->nomor_sk }}</td>
                                        <td>{{ $rp->tgl_sk }}</td>
                                        <td>{{ $rp->pejabat_ttd }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--2-->
                <div class="tab-pane fade" id="nav-penghargaan" role="tabpanel" aria-labelledby="nav-penghargaan-tab">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-striped table-md datatable_table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Penghargaan</th>
                                        <th>Nomor</th>
                                        <th>Tanggal</th>
                                        <th>Tahun</th>
                                        <th>Instansi yang Memberikan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $no = 1; @endphp
                                    @foreach ($penghargaan as $peng)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{ $peng->nama_penghargaan }}</td>
                                        <td>{{ $peng->nomor_sk }}</td>
                                        <td>{{ $peng->tgl_sk }}</td>
                                        <td>{{ $peng->tahun }}</td>
                                        <td>{{ $peng->nama_instansi }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--3-->
                <div class="tab-pane fade" id="nav-riwayat_keluarga" role="tabpanel"
                    aria-labelledby="nav-riwayat_keluarga-tab">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-striped table-md datatable_table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIK</th>
                                        <th>NIP</th>
                                        <th>Nama</th>
                                        <th>Tempat Lahir</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Status Hubungan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $no = 1; @endphp
                                    @foreach ($riwayat_keluarga as $rk)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{ $rk->nik }}</td>
                                        <td>{{ $rk->nip }}</td>
                                        <td>{{ $rk->nama }}</td>
                                        <td>{{ $rk->tempat }}</td>
                                        <td>{{ $rk->tgl }}</td>
                                        <td>{{ $rk->role }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--4-->
                <div class="tab-pane fade" id="nav-oraganisasi" role="tabpanel" aria-labelledby="nav-organisasi-tab">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-striped table-md datatable_table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Organisasi</th>
                                        <th>Jabatan</th>
                                        <th>Tanggal Mulai</th>
                                        <th>Tanggal Selesai</th>
                                        <th>Tempat</th>
                                        <th>Pemimpin Organisasi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $no = 1; @endphp
                                    @foreach ($organisasi_guru as $og)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{ $og->nama_organisasi }}</td>
                                        <td>{{ $og->jabatan }}</td>
                                        <td>{{ $og->tgl_mulai }}</td>
                                        <td>{{ $og->tgl_selesai }}</td>
                                        <td>{{ $og->tempat }}</td>
                                        <td>{{ $og->pemimpin }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-ketlain" role="tabpanel" aria-labelledby="nav-ketlain-tab">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-striped table-md datatable_table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Keterangan</th>
                                        <th>Nomor</th>
                                        <th>Tanggal</th>
                                        <th>Pejabat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $no = 1; @endphp
                                    @foreach ($keterangan_lain as $kl)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{ $kl->nama_keterangan }}</td>
                                        <td>{{ $kl->nomor_sk }}</td>
                                        <td>{{ $kl->tgl_sk }}</td>
                                        <td>{{ $kl->pejabat }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</section>



@endsection