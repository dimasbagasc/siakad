@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Data Pendidikan Informal</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('pendinformal.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label>Nama Kursus / Latihan</label>
                    <input type="text" name="nama_kursus" class="form-control" placeholder="Nama Kursus" required>
                </div>
                <div class="form-group">
                    <label>Tanggal Mulai</label>
                    <input type="date" name="tgl_mulai" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Tanggal Selesai</label>
                    <input type="date" name="tgl_selesai" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Nomor</label>
                    <input type="text" name="nomor_kursus" class="form-control" placeholder="Nomor Kursus" required>
                </div>
                <div class="form-group">
                    <label>Tempat</label>
                    <input type="text" name="tempat" class="form-control" placeholder="Tempat Kursus" required>
                </div>
                <div class="form-group">
                    <label>Institusi Penyelenggara</label>
                    <input type="text" name="institusi" class="form-control" placeholder="Institusi Penyelenggara"
                        required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection