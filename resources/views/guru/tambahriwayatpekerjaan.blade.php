@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Data Riwayat Pekerjaan</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('riwayatpekerjaan.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label>Nama Instansi / Perusahaan</label>
                    <input type="text" name="instansi" class="form-control" placeholder="Nama Instansi / Perusahaan"
                        required>
                </div>
                <div class="form-group">
                    <label>Jabatan</label>
                    <input type="text" name="jabatan" class="form-control" placeholder="Jabatan" required>
                </div>
                <div class="form-group">
                    <label>Tanggal Mulai</label>
                    <input type="date" name="tgl_mulai" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Tanggal Selesai</label>
                    <input type="date" name="tgl_selesai" class="form-control" required>
                </div>

                <div class="form-group">
                    <label>Gaji Pokok</label>
                    <input type="text" name="gaji_pokok" class="form-control" placeholder="Gaji Pokok" required>
                </div>
                <div class="form-group">
                    <label>Nomor Surat Keputasan</label>
                    <input type="text" name="nomor_sk" class="form-control" placeholder="Nomor Surat Keputusan"
                        required>
                </div>
                <div class="form-group">
                    <label>Tanggal Surat Keputasan</label>
                    <input type="date" name="tgl_sk" class="form-control" placeholder="Tanggal Surat Keputusan"
                        required>
                </div>
                <div class="form-group">
                    <label>Pejabat Penandatangan SK</label>
                    <input type="text" name="pejabat_ttd" class="form-control"
                        placeholder="Pejabat Penandatangan Surat Keputusan" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection