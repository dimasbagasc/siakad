@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Data Wali Kelas</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('angkatan.index')}}" class="btn btn-warning fas fa-angle-left"> Kembali</a>
                <a href="{{route('walikelas.create',['id_angkatan'=>$id_angkatan])}}" class="btn btn-primary fa fa-plus"
                    style="position: absolute; right:25px;"> Tambah Data</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Guru</th>
                                {{-- <th>Kelas</th> --}}
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($data as $wk)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{ $wk->nama }}</td>
                                {{-- <td>{{ $wk->id_kelas }}</td> --}}
                                <td>
                                    <form method="POST" action="{{route('walikelas.destroy',['walikela'=>$wk->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        {{-- <a href="{{route('walikelas.edit',['walikela'=>$wk->id])}}"
                                            class="btn btn-icon icon-left btn-warning"><i class="far fa-edit"></i>
                                            Edit</a> --}}
                                        <button type="submit" class="btn btn-icon btn-left btn-danger"><i
                                                class="fas fa-times"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@endsection