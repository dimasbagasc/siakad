<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\M_Jadwal;
use App\Models\M_Sesi;
use Illuminate\Support\Facades\DB;

class DatajadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahunAktif = getTahunAktif();
        $data = DB::table('angkatan')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('angkatan.*', 'kelas.nama_kelas', 'tahunajaran.tahun', 'tahunajaran.semester')
            ->where('tahunajaran.id', $tahunAktif->id)
            ->get();

        return view('admin.datajadwal')->with([
            'data' => $data,
            'tahunAktif' => $tahunAktif
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tahunAktif = getTahunAktif();
        $angkatan = DB::table('angkatan')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('angkatan.*', 'kelas.nama_kelas', 'tahunajaran.tahun', 'tahunajaran.semester')
            ->where('angkatan.id', '=', $request['id_angkatan'])
            ->where('tahunajaran.id', $tahunAktif->id)
            ->first();

        $sesi = M_Sesi::all();

        $gurumapel = DB::table('gurumapel')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->select('gurumapel.id', DB::raw('CONCAT(mapel.matapelajaran, " - ", guru.nama) as gurumapel'))
            ->get();

        $datajadwal = DB::table('jadwal')
            ->join('gurumapel', 'jadwal.id_guru', '=', 'gurumapel.id')
            ->join('sesi', 'jadwal.id_sesi', '=', 'sesi.id')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->select('jadwal.*', 'guru.nama', 'sesi.sesi', 'sesi.jam_mulai', 'sesi.jam_selesai', 'mapel.matapelajaran')
            ->where('jadwal.id_angkatan', '=', $request['id_angkatan'])
            ->orderBy('sesi.sesi', 'asc')
            ->orderBy('jadwal.hari', 'asc')
            ->get()
            ->toArray();

        $jadwal = [];
        foreach ($datajadwal as $key => $value) {
            $jadwal[$value->sesi][$value->hari][] = $datajadwal[$key];
        }
        // echo '<pre>';
        // print_r($jadwal);
        // exit;

        return view('admin.tambahjadwal')->with([
            'angkatan' => $angkatan,
            'sesi' => $sesi,
            'gurumapel' => $gurumapel,
            'jadwal' => $jadwal
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tahunAktif = getTahunAktif();
        $data = $request->except(['_token', 'DataTables_Table_0_length']);
        $cekJadwalKelas = DB::table('jadwal')
            ->where('id_angkatan', '=', $request['id_angkatan'])
            ->where('id_sesi', '=', $request['id_sesi'])
            ->where('hari', '=', $request['hari'])
            ->get()
            ->toArray();

        $cekJadwalGuru = DB::table('jadwal')
            ->join('angkatan', 'jadwal.id_angkatan', 'angkatan.id')
            ->where('id_guru', '=', $request['id_guru'])
            ->where('id_sesi', '=', $request['id_sesi'])
            ->where('hari', '=', $request['hari'])
            ->where('angkatan.id_tahunajaran', $tahunAktif->id)
            ->get()
            ->toArray();

        // echo '<pre>';
        // print_r($cekJadwalGuru);
        // exit;

        if (!empty($cekJadwalKelas)) {
            return redirect('/jadwal/create?id_angkatan=' . $request['id_angkatan'])->with(['error_jadwal' => 'Kelas Tersebut Sudah Ada Jadwal di Hari dan Sesi Tersebut']);
        } else if (!empty($cekJadwalGuru)) {
            return redirect('/jadwal/create?id_angkatan=' . $request['id_angkatan'])->with(['error_jadwal' => 'Guru Tersebut Sudah Ada Jadwal di Hari dan Sesi Tersebut']);
        } else {
            M_Jadwal::insert($data);
            return redirect('/jadwal/create?id_angkatan=' . $request['id_angkatan']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = M_Jadwal::findOrFail($id);
        $item->delete();
        return redirect('/jadwal')->with(['success' => 'Data Berhasil Di Hapus']);
    }

    public function hapus(Request $request)
    {
        $item = M_Jadwal::findOrFail($request['id']);
        $item->delete();
        return redirect('/jadwal')->with(['success' => 'Data Berhasil Di Hapus']);
    }
}
