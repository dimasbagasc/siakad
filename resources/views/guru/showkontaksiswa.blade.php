@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Informasi Kontak</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <a href="{{route('kelasguru.index')}}" class="btn btn-icon icon-left btn-warning"><i
                        class="fas fa-arrow-left"></i>Kembali</a>
                <br>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                        <tbody>
                            <tr>
                                <h2>Biodata Siswa</h2>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:center">
                                    <img src="{{URL::to('/uploads').'/'.$data->foto}}" width="30%">
                                </td>
                            </tr>
                            <tr>
                                <th>NISN</th>
                                <td>{{$data->nisn}}</td>
                            </tr>
                            <tr>
                                <th>Nama</th>
                                <td>{{$data->nama}}</td>
                            </tr>
                            <tr>
                                <th>Tempat Lahir</th>
                                <td>{{$data->tempat_lahir}}</td>
                            </tr>
                            <tr>
                                <th>Tanggal Lahir</th>
                                <td>{{$data->tgl_lahir}}</td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td>{{$data->jk}}</td>
                            </tr>
                            <tr>
                                <th>Agama</th>
                                <td>{{$data->agama}}</td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td>{{$data->alamat}}</td>
                            </tr>
                            <tr>
                                <th>Kodepos</th>
                                <td>{{$data->kodepos}}</td>
                            </tr>
                            <tr>
                                <th>Anak ke-</th>
                                <td>{{$data->anak_ke}}</td>
                            </tr>
                            <tr>
                                <th>Jumlah Saudara</th>
                                <td>{{$data->jml_saudara}}</td>
                            </tr>
                            <tr>
                                <th>Pendidikan Terakhir</th>
                                <td>{{$data->akhir_pendidikan}}</td>
                            </tr>
                            <tr>
                                <th>Asal Sekolah</th>
                                <td>{{$data->asal_sekolah}}</td>
                            </tr>
                            <tr>
                                <th>Tahun Diterima</th>
                                <td>{{$data->thn_terima}}</td>
                            </tr>
                            <tr>
                                <th>Riwayat Penyakit</th>
                                <td>{{$data->penyakit_diderita}}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{{$data->status}}</td>
                            </tr>
                            <tr>
                                <th colspan="2">
                                    <h2>Data Orangtua</h2>
                                </th>
                            </tr>
                            <tr>
                                <th>NIK Ayah</th>
                                <td>{{$ortu->nik_ayah}}</td>
                            </tr>
                            <tr>
                                <th>Nama Ayah</th>
                                <td>{{$ortu->nama_ayah}}</td>
                            </tr>
                            <tr>
                                <th>Pekerjaan Ayah</th>
                                <td>{{$ortu->pekerjaan_ayah}}</td>
                            </tr>
                            <tr>
                                <th>NIK Ibu</th>
                                <td>{{$ortu->nik_ibu}}</td>
                            </tr>
                            <tr>
                                <th>Nama Ibu</th>
                                <td>{{$ortu->nama_ibu}}</td>
                            </tr>
                            <tr>
                                <th>Nama Wali</th>
                                <td>{{$ortu->nama_wali}}</td>
                            </tr>
                            <tr>
                                <th>Hubungan Wali</th>
                                <td>{{$ortu->hubungan}}</td>
                            </tr>
                            <tr>
                                <th>Alamat Wali</th>
                                <td>{{$ortu->alamat_wali}}</td>
                            </tr>
                            <tr>
                                <th>Nomor Hanphone Wali</th>
                                <td>{{$ortu->nohp}}</td>
                            </tr>

                        </tbody>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@endsection