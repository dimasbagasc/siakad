@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
  <br>
  <div class="section-header">
    <h1>Mata Pelajaran</h1>
  </div>

  <div class="col-12 col-md-12 col-lg-12">
    <div class="card">
      <div class="card-header">
        <a href="{{route('mapel.create')}}" class="btn btn-primary fa fa-plus" style="position: absolute; right:25px;">
          Tambah Data</a>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped table-md datatable_table">
            <thead>
              <tr>
                <th>No</th>
                <th>Mata Pelajaran</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @php $no = 1; @endphp
              @foreach ($data as $dataMapel)
              <tr>
                <td>{{$no++}}</td>
                <td>{{ $dataMapel->matapelajaran }}</td>
                <td>
                  <form method="POST" action="{{route('mapel.destroy',['mapel'=>$dataMapel->id])}}">
                    @csrf
                    @method('DELETE')
                    <a href="{{route('mapel.edit',['mapel'=>$dataMapel->id])}}"
                      class="btn btn-icon btn-left btn-warning"><i class="far fa-edit"></i> Edit</a>
                    <button type="submit" class="btn btn-icon btn-left btn-danger"><i class="fas fa-times"></i>
                      Delete</button>
                  </form>
                </td>

              </tr>

              @endforeach
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>
  </div>
</section>



@endsection