<?php

namespace Database\Seeders;

use App\Models\M_kelas;
use Illuminate\Database\Seeder;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kelas = [
            [
                'nama_kelas' => 'VIIA'
            ],
            [
                'nama_kelas' => 'VIIB'
            ],
            [
                'nama_kelas' => 'VIIC'
            ],
            [
                'nama_kelas' => 'VIID'
            ],
            [
                'nama_kelas' => 'VIIIA'
            ],
            [
                'nama_kelas' => 'VIIIB'
            ],
            [
                'nama_kelas' => 'VIIIC'
            ],
            [
                'nama_kelas' => 'VIIID'
            ],
            [
                'nama_kelas' => 'IXA'
            ],
            [
                'nama_kelas' => 'IXB'
            ],
            [
                'nama_kelas' => 'IXC'
            ],
            [
                'nama_kelas' => 'IXD'
            ]

            ];

            foreach ($kelas as $value ){
                M_kelas::create($value); 
            }
    }
}
