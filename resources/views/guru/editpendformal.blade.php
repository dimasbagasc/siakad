@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Edit Data Pendidikan Informal</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('pendformal.update',['pendformal'=>$data->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Tingkat</label>
                    <input type="text" name="tingkat" class="form-control" placeholder="Tingkat"
                        value="{{$data->tingkat}}">
                </div>
                <div class="form-group">
                    <label>Nama Sekolah/Perguruan Negeri</label>
                    <input type="text" name="nama_sekolah" class="form-control"
                        placeholder="Nama Sekolah / Perguruan Negeri" value="{{$data->nama_sekolah}}">
                </div>
                <div class="form-group">
                    <label>Akreditasi</label>
                    <input type="text" name="akreditasi" class="form-control" placeholder="Akreditasi"
                        value="{{$data->akreditasi}}">
                </div>
                <div class="form-group">
                    <label>Tempat</label>
                    <input type="text" name="tempat" class="form-control" placeholder="Tempat"
                        value="{{$data->tempat}}">
                </div>
                <div class="form-group">
                    <label>Nomor</label>
                    <input type="text" name="nomor_ijazah" class="form-control" placeholder="Nomor"
                        value="{{$data->nomor_ijazah}}">
                </div>
                <div class="form-group">
                    <label>Tanggal</label>
                    <input type="date" name="tanggal" class="form-control" placeholder="Tanggal Kelulusan"
                        value="{{$data->tanggal}}">
                </div>
                <div class="form-group">
                    <label>Pejabat Penandatangan</label>
                    <input type="text" name="pejabat_ttd" class="form-control" placeholder="Pejabat Penandatangan"
                        value="{{$data->pejabat_ttd}}">
                </div>
                <div class="form-group">
                    <label>Gelar Depan</label>
                    <input type="text" name="gelar_depan" class="form-control" placeholder="Gelar Depan"
                        value="{{$data->gelar_depan}}">
                </div>
                <div class="form-group">
                    <label>Gelar Belakang</label>
                    <input type="text" name="gelar_belakang" class="form-control" placeholder="Gelar Belakang"
                        value="{{$data->gelar_belakang}}">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection