@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Data User</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($data as $user)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->role }}</td>
                                <td>
                                    @php
                                    if ($user->role == 'siswa') {
                                    $data = DB::table('users')
                                    ->join('siswa', 'users.id', 'siswa.id_user')
                                    ->select('siswa.status')
                                    ->where('users.id', '=', $user->id)
                                    ->first();
                                    echo $data ? $data->status : '';
                                    }

                                    if ($user->role == 'guru') {
                                    $data = DB::table('users')
                                    ->join('guru', 'users.id', 'guru.id_user')
                                    ->select('guru.status')
                                    ->where('users.id', '=', $user->id)
                                    ->first();
                                    echo $data ? $data->status : '';
                                    }

                                    if ($user->role == 'admin') {
                                    echo 'aktif';
                                    }

                                    @endphp
                                </td>
                                <td>
                                    <form method="POST" action="{{route('datauser.destroy',['datauser'=>$user->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{route('datauser.show',['datauser'=>$user->id])}}"
                                            class="btn btn-icon btn-left btn-primary"><i class="far fa-eye"></i>
                                            Detail </a>
                                        {{-- <a href="{{route('datauser.edit',['datauser'=>$user->id])}}"
                                            class="btn btn-icon btn-left btn-warning"><i class="far fa-edit"></i>
                                            Edit</a> --}}
                                        <button type="submit" class="btn btn-icon btn-left btn-danger"><i
                                                class="fas fa-times"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>

@endsection