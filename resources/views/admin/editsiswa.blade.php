@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Edit Data Siswa</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-biodata-tab" data-toggle="tab" href="#nav-biodata"
                            role="tab" aria-controls="nav-biodata" aria-selected="true">Biodata</a>
                        <a class="nav-item nav-link" id="nav-ortu-tab" data-toggle="tab" href="#nav-ortu" role="tab"
                            aria-controls="nav-ortu" aria-selected="false">Data Orangtua/Wali</a>
                    </div>
                </nav>
                <form method="POST" action="{{ route('siswa.update',['siswa'=>$data->id]) }}"
                    enctype="multipart/form-data">
                    @csrf
                    @method ('PUT')
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-biodata" role="tabpanel"
                            aria-labelledby="nav-biodata-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>NISN</label>
                                        <input type="text" name="nisn" class="form-control" value="{{$data->nisn}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" name="nama" class="form-control" value="{{$data->nama}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tempat Lahir</label>
                                        <input type="text" name="tempat_lahir" class="form-control"
                                            value="{{$data->tempat_lahir}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <input type="date" name="tgl_lahir" class="form-control"
                                            value="{{$data->tgl_lahir}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Jenis Kelamin</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="jk" id="jk1"
                                                value="laki-laki" {{$data->jk == 'laki-laki' ? 'checked' : ''}}>
                                            <label class="form-check-label" for="jk1">
                                                Laki-laki
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="jk" id="jk2"
                                                value="perempuan" {{$data->jk == 'perempuan' ? 'checked' : ''}}>
                                            <label class="form-check-label" for="jk2">
                                                Perempuan
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Agama</label>
                                        <select name="agama" class="form-control">
                                            <option></option>
                                            <option value="islam" {{$data->agama == 'islam' ? 'selected' : ''}}>Islam
                                            </option>
                                            <option value="katholik" {{$data->agama == 'katholik' ? 'selected' :
                                                ''}}>Katholik</option>
                                            <option value="kristen" {{$data->agama == 'kristen' ? 'selected' :
                                                ''}}>Kristen</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea name="alamat" class="form-control" id="alamat">
                                            {{$data->alamat}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Kodepos</label>
                                        <input type="text" name="kodepos" class="form-control"
                                            value="{{$data->kodepos}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Anak Ke-</label>
                                        <input type="text" name="anak_ke" class="form-control"
                                            value="{{$data->anak_ke}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jumlah Saudara</label>
                                        <input type="text" name="jml_saudara" class="form-control"
                                            value="{{$data->jml_saudara}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pendidikan Terakhir</label>
                                        <input type="text" name="akhir_pendidikan" class="form-control"
                                            value="{{$data->akhir_pendidikan}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Asal Sekolah</label>
                                        <input type="text" name="asal_sekolah" class="form-control"
                                            value="{{$data->asal_sekolah}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tahun diterima</label>
                                        <input type="text" name="thn_terima" class="form-control"
                                            value="{{$data->thn_terima}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Riwayat Penyakit</label>
                                        <input type="text" name="penyakit_diderita" class="form-control"
                                            value="{{$data->penyakit_diderita}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select name="status" class="form-control">
                                            <option value="aktif" selected>Aktif</option>
                                            <option value="nonaktif">Non Aktif</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Foto</label>
                                        <input type="file" name="foto" class="form-control" value="{{$data->foto}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="nav-ortu" role="tabpanel" aria-labelledby="nav-akun-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>NIK Ayah</label>
                                        <input type="text" name="nik_ayah" class="form-control"
                                            value="{{$ortu->nik_ayah}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Ayah</label>
                                        <input type="text" name="nama_ayah" class="form-control"
                                            value="{{$ortu->nama_ayah}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pekerjaan Ayah</label>
                                        <input type="text" name="pekerjaan_ayah" class="form-control"
                                            value="{{$ortu->pekerjaan_ayah}}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>NIK Ibu</label>
                                        <input type="text" name="nik_ibu" class="form-control"
                                            value="{{$ortu->nik_ibu}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Ibu</label>
                                        <input type="text" name="nama_ibu" class="form-control"
                                            value="{{$ortu->nama_ibu}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Wali</label>
                                        <input type="text" name="nama_wali" class="form-control"
                                            value="{{$ortu->nama_wali}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Hubungan Wali</label>
                                        <input type="text" name="hubungan" class="form-control"
                                            value="{{$ortu->hubungan}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Alamat Wali</label>
                                        <textarea name="alamat_wali" class="form-control" id="alamat_wali">
                                            {{$ortu->alamat_wali}}
                                        </textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nomor Handphone Wali</label>
                                        <input type="text" name="nohp" class="form-control" value="{{$ortu->nohp}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>

        </div>
</section>

@endsection