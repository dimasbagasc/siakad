@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Keterangan Lain</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('kepegawaian.index')}}" class="btn btn-warning fas fa-angle-left"> Kembali</a>
                <a href="{{route('ketlain.create')}}" class="btn btn-primary fa fa-plus"
                    style="position: absolute; right:25px;"> Tambah Data</a>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Keterangan</th>
                                <th>Nomor Surat Keputusan</th>
                                <th>Tanggal</th>
                                <th>Pejabat</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($data as $kl)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$kl->nama_keterangan }}</td>
                                <td>{{$kl->nomor_sk}}</td>
                                <td>{{$kl->tgl_sk}}</td>
                                <td>{{$kl->pejabat}}</td>
                                <td>
                                    <form method="POST" action="{{route('ketlain.destroy',['ketlain'=>$kl->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{route('ketlain.edit',['ketlain'=>$kl->id])}}"
                                            class="btn btn-icon btn-left btn-warning"><i class="far fa-edit"></i>
                                            Edit</a>
                                        <button type="submit" class="btn btn-icon btn-left btn-danger"><i
                                                class="fas fa-times"></i>
                                            Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>



@endsection