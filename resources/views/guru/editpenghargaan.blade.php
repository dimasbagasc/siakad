@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Data Tanda Jasa/Penghargaan</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('penghargaan.update',['penghargaan'=>$data->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Nama Bintang/Lencana Penghargaan</label>
                    <input type="text" name="nama_penghargaan" class="form-control" placeholder="Nama Penghargaan"
                        value="{{$data->nama_penghargaan}}">
                </div>
                <div class="form-group">
                    <label>Nomor Surat Keputusan</label>
                    <input type="text" name="nomor_sk" class="form-control" placeholder="Nomor Surat Keputusan"
                        value="{{$data->nomor_sk}}">
                </div>
                <div class="form-group">
                    <label>Tanggal Surat Keputusan</label>
                    <input type="date" name="tgl_sk" class="form-control" value="{{$data->tgl_sk}}">
                </div>
                <div class="form-group">
                    <label>Tahun</label>
                    <input type="text" name="tahun" class="form-control" placeholder="Tahun" value="{{$data->tahun}}">
                </div>
                <div class="form-group">
                    <label>Nama Negara/Instansi yang memberikan</label>
                    <input type="text" name="nama_instansi" class="form-control" placeholder="Nama Instansi"
                        value="{{$data->nama_instansi}}">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection