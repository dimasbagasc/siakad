@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Data Siswa</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                {{-- <a href="{{route('')}}" class="btn btn-primary fa fa-plus">Tambah Data</a> --}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <form method="POST" action="{{ route('rombel.store') }}">
                        @csrf
                        <input type="hidden" name="id_angkatan" value="{{$id_angkatan}}" />
                        <table class="table table-striped table-md datatable_table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NISN</th>
                                    <th>Nama</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $no = 1; @endphp
                                @foreach ($data as $siswa)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{ $siswa->nisn }}</td>
                                    <td>{{ $siswa->nama }}</td>
                                    <td>
                                        <input type="checkbox" name="cek[]" value="{{$siswa->id}}"
                                            class="form-control" />
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="group">
                            <button type="submit" class="btn btn-success"> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@endsection