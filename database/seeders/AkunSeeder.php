<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class AkunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'username' => 'admin',
                'name' => 'admin',
                'role' => 'admin',
                'password' => bcrypt('admin12345'),
            ],
            [
                'username' => 'guru',
                'name' => 'guru',
                'role' => 'guru',
                'password' => bcrypt('guru12345'),
            ],
            [
                'username' => 'siswa',
                'name' => 'siswa',
                'role' => 'siswa',
                'password' => bcrypt('siswa12345'),
            ]
            ];

            foreach ($user as $key => $value){
                user::create($value);
            }
    }
}
