<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiwayatKeluargaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_keluarga', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_guru');
            $table->foreign('id_guru')->references('id')->on('guru');

            $table->string('nik');
            $table->string('nip');
            $table->string('nama');
            $table->string('tempat');
            $table->date('tgl');
            $table->string('pekerjaan');
            $table->string('perusahaan');
            $table->string('status_perkawinan');
            $table->string('akta_nikah');
            $table->date('tgl_menikah');
            $table->string('status_hidup');
            $table->string('role');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_keluarga');
    }
}
