@section('sidebar')
<aside id="sidebar-wrapper">
  <div class="sidebar-brand">
    <a href="index.html">SIAKAD</a>
  </div>
  <div class="sidebar-brand sidebar-brand-sm">
    <a href="index.html">St</a>
  </div>
  <ul class="sidebar-menu">
    <li class="menu-header">Dashboard</li>
    <li class="nav-item dropdown {{ request()->is('dashboardadmin') ? 'active' : ''}}">
      <a href="{{route('dashboardadmin.index')}}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
    </li>
    <li class="menu-header">Master Data</li>
    <li
      class="nav-item dropdown {{ request()->is('guru') || request()->is('siswa') || request()->is('kelas') || request()->is('mapel')  || request()->is('tahunajaran') || request()->is('sesi')  ? 'active' : ''}}">
      <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Master
          Data</span></a>
      <ul class="dropdown-menu">
        <li class="{{ request()->is('guru') ? 'active' : ''}}"><a class="nav-link " href="{{route('guru.index')}}">Data
            Guru</a>
        </li>
        <li class="{{ request()->is('siswa') ? 'active' : ''}}"><a class="nav-link "
            href="{{route('siswa.index')}}">Data
            Siswa</a></li>
        <li class="{{ request()->is('kelas') ? 'active' : ''}}"><a class="nav-link" href="{{route('kelas.index')}}">Data
            Kelas</a></li>
        <li class="{{ request()->is('mapel') ? 'active' : ''}}"><a class="nav-link" href="{{route('mapel.index')}}">Data
            Mata
            Pelajaran</a></li>
        <li class="{{ request()->is('tahunajaran') ? 'active' : ''}}"><a class="nav-link"
            href="{{route('tahunajaran.index')}}">Tahun Ajaran</a></li>
        <li class="{{ request()->is('sesi') ? 'active' : ''}}"><a class="nav-link"
            href="{{route('sesi.index')}}">Sesi</a></li>
      </ul>
    </li>

    <li class="menu-header">Data Angkatan</li>
    <li class="nav-item {{ request()->is('angkatan') ? 'active' : ''}}">
      <a href="{{route('angkatan.index')}}" class="nav-link "><i class="fas fa-users"></i> <span>Angkatan</span></a>
    </li>

    <li class="menu-header">Guru Mata Pelajaran</li>
    <li class="nav-item {{ request()->is('gurumapel') ? 'active' : ''}}">
      <a href="{{route('gurumapel.index')}}" class="nav-link "><i class="far fa-user"></i> <span>Guru Mata
          Pelajaran</span></a>
    </li>

    <li class="menu-header">Data Jadwal</li>
    <li class="nav-item {{ request()->is('jadwal') ? 'active' : ''}}">
      <a href="{{route('jadwal.index')}}" class="nav-link "><i class="fas fas fa-columns"></i> <span>Jadwal</span></a>
    </li>


    <li class="menu-header">Management Pengguna</li>
    <li class="nav-item {{ request()->is('datauser') ? 'active' : ''}}">
      <a href="{{route('datauser.index')}}" class="nav-link "><i class="far fa-user"></i> <span>user</span></a>
    </li>

</aside>
@endsection