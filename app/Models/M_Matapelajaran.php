<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Matapelajaran extends Model
{
    use HasFactory;

    protected $table = 'mapel';
    protected $fillable = [
        'matapelajaran'
    ];

    protected $hidden;
}
