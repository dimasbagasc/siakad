@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Edit Data Keterangan Organisasi</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('ketorganisasi.update',['ketorganisasi'=>$data->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Nama Organisasi</label>
                    <input type="text" name="nama_organisasi" class="form-control" placeholder="Nama Organisasi"
                        value="{{$data->nama_organisasi}}">
                </div>
                <div class="form-group">
                    <label>Jabatan</label>
                    <input type="text" name="jabatan" class="form-control" placeholder="Jabatan"
                        value="{{$data->jabatan}}">
                </div>
                <div class="form-group">
                    <label>Tanggal Mulai</label>
                    <input type="date" name="tgl_mulai" class="form-control" value="{{$data->tgl_mulai}}">
                </div>
                <div class="form-group">
                    <label>Tanggal Selesai</label>
                    <input type="date" name="tgl_selesai" class="form-control" value="{{$data->tgl_selesai}}">
                </div>

                <div class="form-group">
                    <label>Tempat</label>
                    <input type="text" name="tempat" class="form-control" placeholder="Tempat Organisasi"
                        value="{{$data->tempat}}">
                </div>
                <div class="form-group">
                    <label>Pemimpin Organisasi</label>
                    <input type="text" name="pemimpin" class="form-control" placeholder="Pemimpin Organisasi"
                        value="{{$data->nama_organisasi}}">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection