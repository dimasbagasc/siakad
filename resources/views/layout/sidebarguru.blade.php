@section('sidebar')
<aside id="sidebar-wrapper">
  <div class="sidebar-brand">
    <a href="index.html">SIAKAD</a>
  </div>
  <div class="sidebar-brand sidebar-brand-sm">
    <a href="index.html">St</a>
  </div>
  <ul class="sidebar-menu">
    <li class="menu-header">Dashboard</li>
    <li class="nav-item dropdown  {{ request()->is('dashboardguru') ? 'active' : ''}}">
      <a href="{{route('dashboardguru.index')}}" class=" nav-link "><i
          class=" fas fa-fire"></i><span>Dashboard</span></a>
    </li>
    <li class="menu-header">Master Data</li>
    <li
      class="nav-item dropdown {{ request()->is('kepegawaian') || request()->is('jadwalguru') || request()->is('nilai') || request()->is('presensi')  ? 'active' : ''}}">
      <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Master
          Data</span></a>
      <ul class="dropdown-menu">
        <li class="{{ request()->is('kepegawaian') ? 'active' : ''}}"><a class="nav-link"
            href="{{route('kepegawaian.index')}}">Data Kepegawaian</a></li>
        <li class="{{ request()->is('jadwalguru') ? 'active' : ''}}"><a class="nav-link"
            href="{{route('jadwalguru.index')}}">Jadwal</a></li>
        <li class="{{ request()->is('nilai') ? 'active' : ''}}"><a class="nav-link"
            href="{{route('nilai.index')}}">Nilai</a></li>
        <li class="{{ request()->is('presensi') ? 'active' : ''}}"><a class="nav-link"
            href="{{route('presensi.index')}}">Presensi Siswa</a></li>
      </ul>
    </li>

    <li class="menu-header">Rekap Presensi</li>
    <li class="nav-item {{ request()->is('presensi') ? 'active' : ''}}">
      <a href="{{route('presensi.indexallrekap')}}" class="nav-link"><i class="fas fa-book-open"></i><span>Rekap
          Presensi</span></a>
    </li>

    <li class="menu-header">Kelas</li>
    <li class="nav-item {{ request()->is('kelasguru') ? 'active' : ''}}">
      <a href="{{route('kelasguru.index')}}" class=" nav-link "><i class=" fas fa-users"></i><span>kelas</span></a>
    </li>

</aside>
@endsection