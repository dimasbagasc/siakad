@extends('layout.master')
@extends('layout.sidebarguru')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Edit Data Riwayat Keluarga</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('riwayatkeluarga.update',['riwayatkeluarga'=>$data->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>NIK</label>
                    <input type="text" name="nik" class="form-control" placeholder="Nomor NIK" value="{{$data->nik}}">
                </div>
                <div class=" form-group">
                    <label>NIP</label>
                    <input type="text" name="nip" class="form-control" placeholder="Nomor NIP" value="{{$data->nip}}">
                </div>
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap"
                        value="{{$data->nama}}">
                </div>
                <div class=" form-group">
                    <label>Tempat Lahir</label>
                    <input type="text" name="tempat" class="form-control" placeholder="Tempat Lahir"
                        value="{{$data->tempat}}">
                </div>
                <div class=" form-group">
                    <label>Tanggal Lahir</label>
                    <input type="date" name="tgl" class="form-control" value="{{$data->tgl}}">
                </div>
                <div class=" form-group">
                    <label>Pekerjaan</label>
                    <input type="text" name="pekerjaan" class="form-control" placeholder="Pekerjaan"
                        value="{{$data->pekerjaan}}">
                </div>
                <div class=" form-group">
                    <label>Perusahaan</label>
                    <input type="text" name="perusahaan" class="form-control" placeholder="Nama Perusahaan"
                        value="{{$data->perusahaan}}">
                </div>
                <div class="form-group">
                    <label>Status Perkawinan</label>
                    <select name="status_perkawinan" class="form-control" placeholder="Status Perkawinan" srequired>
                        <option></option>
                        <option value="kawin" {{$data->status_perkawinan == 'kawin' ? 'selected' : ''}}>Kawin</option>
                        <option value="belum_kawin" {{$data->status_perkawinan== 'belum kawin' ? 'selected' : ''}}>Belum
                            Kawin</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Akta Nikah</label>
                    <input type="text" name="akta_nikah" class="form-control" placeholder="Nomor Akta Pernikahan"
                        value="{{$data->akta_nikah}}" required>
                </div>
                <div class="form-group">
                    <label>Tanggal Pernikahan</label>
                    <input type="date" name="tgl_menikah" class="form-control" value="{{$data->tgl_menikah}}" required>
                </div>
                <div class="form-group">
                    <label>Status Hidup</label>
                    <select name="status_hidup" class="form-control" required>
                        <option></option>
                        <option value="hidup" {{$data->status_hidup == 'hidup' ? 'selected' : ''}}>Hidup</option>
                        <option value="mati" {{$data->status_hidup == 'mati' ? 'selected' : ''}}>Mati</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Role</label>
                    <select name="role" class="form-control" required>
                        <option></option>
                        <option value="suami" {{$data->role == 'suami' ? 'selected' : ''}}>Suami</option>
                        <option value="isteri" {{$data->role == 'isteri' ? 'selected' : ''}}>Isteri</option>
                        <option value="anak" {{$data->role == 'anak' ? 'selected' : ''}}>Anak</option>
                        <option value="orangtua_kandung" {{$data->role == 'orangtua_kandung' ? 'selected' :
                            ''}}>Orangtua Kandung</option>
                        <option value="saudara_kandung" {{$data->role == 'saudara_kandung' ? 'selected' : ''}}>Saudara
                            Kandung</option>
                        <option value="bapak_mertua" {{$data->role == 'bapak_mertua' ? 'selected' : ''}}>Bapak
                            Mertua</option>
                        <option value="ibu_mertua" {{$data->role == 'ibu_mertua' ? 'selected' : ''}}>Ibu Mertua
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {{-- dkasdkajshd --}}
            </form>
        </div>
    </div>
</section>

@endsection