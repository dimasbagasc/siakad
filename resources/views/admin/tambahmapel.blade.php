@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
  <br>
  <div class="section-header">
    <h1>Tambah Data Matapelajaran</h1>
  </div>
  <div class="card">
    <div class="card-body">
      <form action="{{route('mapel.store')}}" method="POST">
        @csrf
        <div class="form-group">
          <label>MataPelajaran</label>
          <input type="text" name="matapelajaran" class="form-control" placeholder="Nama Matapelajaran" required>
        </div>
        <div class="form-group">
          <a href="{{route('mapel.index')}}" class="btn btn-icon icon-left btn-warning">Kembali</a>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</section>

@endsection