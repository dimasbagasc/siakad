<?php

namespace App\Http\Controllers;

use App\Models\M_Siswa;
use App\Models\M_Sesi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PresensisiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tahunAktif = getTahunAktif();
        $siswa = M_Siswa::where('id_user', Auth::user()->id)->first();
        $datajadwal = DB::table('jadwal')
            ->join('gurumapel', 'jadwal.id_guru', '=', 'gurumapel.id')
            ->join('sesi', 'jadwal.id_sesi', '=', 'sesi.id')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->join('angkatan', 'jadwal.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->join('rombel', 'rombel.id_angkatan', '=', 'angkatan.id')
            ->join('siswa', 'rombel.id_siswa', '=', 'siswa.id')
            ->select('jadwal.id_guru', 'jadwal.id_angkatan', 'guru.nama', 'mapel.matapelajaran', 'kelas.nama_kelas')
            ->where('siswa.id', '=', $siswa->id)
            ->where('tahunajaran.id', $tahunAktif->id)
            // ->where('jadwal.hari', '=', date('N'))
            ->orderBy('sesi.sesi', 'asc')
            ->orderBy('jadwal.hari', 'asc')
            ->groupBy('jadwal.id_guru', 'jadwal.id_angkatan', 'guru.nama', 'mapel.matapelajaran', 'kelas.nama_kelas')
            ->get()
            ->toArray();

        $countpresensi = DB::table('presensi')
            ->selectRaw("COUNT(CASE WHEN presensi.status='hadir' THEN 1 END) AS hadir,COUNT(CASE WHEN presensi.status='alpha' THEN 1 END) AS alpha,COUNT(CASE WHEN presensi.status='ijin' THEN 1 END) AS ijin, mapel.matapelajaran as matapelajaran, guru.nama as nama")
            ->join('rombel', 'presensi.id_rombel', '=', 'rombel.id')
            ->join('siswa', 'rombel.id_siswa', '=', 'siswa.id')
            ->join('jadwal', 'presensi.id_jadwal', '=', 'jadwal.id')
            ->join('angkatan', 'jadwal.id_angkatan', '=', 'angkatan.id')
            ->join('gurumapel', 'jadwal.id_guru', '=', 'gurumapel.id')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->where('siswa.id', '=', $siswa->id)
            ->where('angkatan.id_tahunajaran', $tahunAktif->id)
            ->groupBy('mapel.matapelajaran', 'guru.nama')
            ->get();

        // echo '<pre>';
        // print_r($countpresensi);
        // exit;

        return view('siswa.presensisiswa')->with([
            'jadwal' => $datajadwal,
            'tahunAktif' => $tahunAktif,
            'countpresensi' => $countpresensi
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
