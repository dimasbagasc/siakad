<?php

use App\Models\M_Tahunajaran;

if (!function_exists("getTahunAktif")) {

    function getTahunAktif()
    {
        $data = M_Tahunajaran::where('status', 'aktif')->first();
        if (!empty($data)) {
            return $data;
        } else {
            return 0;
        }
    }
}
