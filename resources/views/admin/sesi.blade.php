@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Data Sesi</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('sesi.create')}}" class="btn btn-primary fa fa-plus"
                    style="position: absolute; right:25px;"> Tambah Data</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Sesi</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($data as $s)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{ $s->sesi . ' | ' . $s->jam_mulai .
                                    '-'.$s->jam_selesai}}</td>
                                <td>
                                    <form method="POST" action="{{route('sesi.destroy',['sesi'=>$s->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{route('sesi.edit',['sesi'=>$s->id])}}"
                                            class="btn btn-icon btn-left btn-warning"><i class="far fa-edit"></i>
                                            Edit</a>
                                        <button type="submit" class="btn btn-icon btn-left btn-danger"><i
                                                class="fas fa-times"></i>
                                            Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@endsection