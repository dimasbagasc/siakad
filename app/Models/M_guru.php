<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_guru extends Model
{
    use HasFactory;

    protected $table = 'guru';
    protected $fillable = [
        'id_user',
        'foto',
        'nama',
        'nip',
        'nik',
        'tmp_lahir',
        'tgl_lahir',
        'jk',
        'alamat',
        'hobi',
        'no_hp',
        'email',
        'agama',
        'status',
        'ket_badan'
    ];

    protected $hidden;
}
