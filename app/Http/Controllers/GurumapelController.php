<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\M_Gurumapel;
use App\Models\M_Matapelajaran;
use App\Models\M_guru;
use Illuminate\Support\Facades\DB;

class GurumapelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('gurumapel')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->select('gurumapel.*', 'mapel.matapelajaran', 'guru.nama')
            ->get();

        return view('admin.gurumapel')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mapel = M_Matapelajaran::all();
        $guru = M_guru::all();
        return view('admin.tambahgurumapel')->with([
            'id_guru' => $guru,
            'id_mapel' => $mapel
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);

        $cekData = DB::table('gurumapel')
            ->where('id_guru', $data['id_guru'])
            ->where('id_mapel', $data['id_mapel'])
            ->get();

        if (empty($cekData->first())) {
            M_Gurumapel::insert($data);
            return redirect('/gurumapel')->with(['success' => 'Data Berhasil Diproses.']);
        } else {
            return redirect('/gurumapel')->with(['warning' => 'Data Guru dan Mapel Sudah Ada!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mapel = M_Matapelajaran::all();
        $guru = M_guru::all();
        $data = M_Gurumapel::findOrFail($id);

        return view('admin.editgurumapel')->with([
            'data' => $data,
            'guru' => $guru,
            'mapel' => $mapel
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = M_Gurumapel::findOrFail($id);
        $data = $request->except(['_token']);
        $item->update($data);
        return redirect('/gurumapel')->with(['success' => 'Data Berhasil Di Perbarui']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $item = M_Gurumapel::findOrFail($id);
            $item->delete();
            return redirect('/gurumapel')->with(['success' => 'Data Berhasil Di Hapus']);
        } catch (\Throwable $th) {
            return redirect('/gurumapel')->with(['error' => 'Data Siswa Sudah Digunakan']);
        }
    }
}
