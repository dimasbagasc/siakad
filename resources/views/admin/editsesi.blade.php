@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Tambah Sesi</h1>
    </div>

    <div class="card">
        <div class="card-body">
            <form action="{{route('sesi.update', ['sesi'=>$data->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label class="d-block" for="sesi">Sesi</label>
                    <input type="text" name="sesi" class="form-control" placeholder="Sesi" value="{{$data->sesi}}">
                </div>
                <div class="form-group">
                    <label class="d-block" for="jam_mulai">Jam Mulai</label>
                    <input placeholder="Selected time" type="text" id="jam_mulai" name="jam_mulai"
                        class="form-control timepicker" value="{{$data->jam_mulai}}">
                </div>
                <div class="form-group">
                    <label class="d-block" for="jam_selesai">Jam Selesai</label>
                    <input placeholder="Selected time" type="text" id="jam_selesai" name="jam_selesai"
                        class="form-control timepicker" value="jam{{$data->jam_selesai}}">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection