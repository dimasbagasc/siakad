@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Detail Guru</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <a href="{{route('guru.index')}}" class="btn btn-icon icon-left btn-warning"><i
                        class="fas fa-arrow-left"></i>Kembali</a>
                <br>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                        <tbody>
                            <tr>
                                <h2>Biodata</h2>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:center">
                                    <img src="{{URL::to('/uploads').'/'.$data->foto}}" width="30%">
                                </td>
                            </tr>

                            <tr>
                                <th>NIK</th>
                                <td>{{$data->nik}}</td>
                            </tr>
                            <tr>
                                <th>NIP</th>
                                <td>{{$data->nip}}</td>
                            </tr>
                            <tr>
                                <th>Nama</th>
                                <td>{{$data->nama}}</td>
                            </tr>
                            <tr>
                                <th>Tempat Lahir</th>
                                <td>{{$data->tmp_lahir}}</td>
                            </tr>
                            <tr>
                                <th>Tanggal Lahir</th>
                                <td>{{$data->tgl_lahir}}</td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td>{{$data->jk}}</td>
                            </tr>
                            <tr>
                                <th>Agama</th>
                                <td>{{$data->agama}}</td>
                            </tr>
                            <tr>
                                <th>Nomor Handphone</th>
                                <td>{{$data->no_hp}}</td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td>{{$data->alamat}}</td>
                            </tr>
                            {{-- <tr>
                                <th>Keterangan Badan</th>
                                <td>{{$data->ket_badan}}</td>
                            </tr> --}}
                            <tr>
                                <th>Hobi</th>
                                <td>{{$data->hobi}}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{{$data->status}}</td>
                            </tr>
                        </tbody>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@endsection