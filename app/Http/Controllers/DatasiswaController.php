<?php

namespace App\Http\Controllers;

use App\Models\M_Siswa;
use App\Models\User;
use App\Models\M_Ortu;
use Illuminate\Http\Request;

class DatasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = M_Siswa::all();
        return view('admin.datasiswa')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tambahsiswa');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|unique:siswa',
            'username' => 'required|unique:users',
            'password' => 'required'
        ]);

        $data = $request->except(['_token']);
        $request->validate([
            'foto' => 'required|mimes:png,jpeg,jpg|max:1024',
        ]);

        $fileName = time() . '.' . $request->foto->extension();

        $res = $request->foto->move(public_path('uploads'), $fileName);

        if ($res) {
            $dataUser = [
                'name' => $data['nama'],
                'email' => $data['email'],
                'username' => $data['username'],
                'password' => bcrypt($data['password']),
                'role' => 'siswa'
            ];
            $user = User::create($dataUser);

            $dataSiswa = [
                'id_user' => $user->id,
                'nisn' => $data['nisn'],
                'email' => $data['email'],
                'foto' => $fileName,
                'nama' => $data['nama'],
                'tempat_lahir' => $data['tempat_lahir'],
                'tgl_lahir' => $data['tgl_lahir'],
                'jk' => $data['jk'],
                'agama' => $data['agama'],
                'anak_ke' => $data['anak_ke'],
                'jml_saudara' => $data['jml_saudara'],
                'akhir_pendidikan' => $data['akhir_pendidikan'],
                'asal_sekolah' => $data['asal_sekolah'],
                'thn_terima' => $data['thn_terima'],
                'penyakit_diderita' => $data['penyakit_diderita'],
                'kodepos' => $data['kodepos'],
                'alamat' => $data['alamat'],
                'status' => $data['status']
            ];
            $siswa = M_Siswa::create($dataSiswa);

            $dataOrtu = [
                'id_siswa' =>  $siswa->id,
                'nama_ayah' => $data['nama_ayah'],
                'nik_ayah' => $data['nik_ayah'],
                'pekerjaan_ayah' => $data['pekerjaan_ayah'],
                'nama_ibu' => $data['nama_ibu'],
                'nik_ibu' => $data['nik_ibu'],
                'nama_wali' => $data['nama_wali'],
                'hubungan' => $data['hubungan'],
                'alamat_wali' => $data['alamat_wali'],
                'nohp' => $data['nohp']
            ];
            M_Ortu::create($dataOrtu);
        }
        return redirect('/siswa')->with(['success' => 'Data Berhasil Di Tambahkan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = M_Siswa::findOrFail($id);
        $dataOrtu = M_Ortu::findOrFail($id);
        return view('admin.showsiswa')->with([
            'data' => $data,
            'ortu' => $dataOrtu
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = M_Siswa::findOrFail($id);
        $dataOrtu = M_Ortu::findOrFail($id);
        return view('admin.editsiswa')->with([
            'data' => $data,
            'ortu' => $dataOrtu
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = M_Siswa::findOrFail($id);
        $data = $request->except(['_token']);
        $request->validate([
            'foto' => 'mimes:png,jpeg,jpg|max:1024',
        ]);

        if ($request->hasFile('foto')) {
            $fileName = time() . '.' . $request->foto->extension();

            $res = $request->foto->move(public_path('uploads'), $fileName);
            if ($res) {
                $data['foto'] = $fileName;
            }
        }

        $item->update($data);
        return redirect('/siswa')->with(['success' => 'Data Berhasil Di Perbarui']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $item = M_Siswa::findOrFail($id);
            $item->delete();
            return redirect('/siswa')->with(['success' => 'Data Berhasil Di Hapus']);
        } catch (\Throwable $th) {
            return redirect('/siswa')->with(['error' => 'Data Siswa Sudah Digunakan']);
        }
    }
}
