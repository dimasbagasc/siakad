@extends('layout.master')
@extends('layout.sidebar')

@section('container')
<section class="section">
    <br>
    <div class="section-header">
        <h1>Data Angkatan {{$tahunAktif->tahun . '('.$tahunAktif->semester.')'}}</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('angkatan.create')}}" class="btn btn-primary fa fa-plus"
                    style="position: absolute; right:25px;"> Tambah Data</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-md datatable_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tahun Ajaran</th>
                                <th>Kelas</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($data as $angkatan)
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{ $angkatan->tahun.'('.$angkatan->semester.')' }}</td>
                                <td>{{ $angkatan->nama_kelas }}</td>
                                <td>
                                    <form method="POST"
                                        action="{{route('angkatan.destroy',['angkatan'=>$angkatan->id])}}">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{route('angkatan.edit',['angkatan'=>$angkatan->id])}}"
                                            class="btn btn-icon icon-left btn-warning"><i class="far fa-edit"></i>
                                            Edit</a>
                                        <button type="submit" class="btn btn-icon btn-left btn-danger"><i
                                                class="fas fa-times"></i> Delete</button>
                                        <a href="{{route('rombel.index', ['id_angkatan'=>$angkatan->id])}}"
                                            class="btn btn-icon btn-left btn-primary"><i class="fas fa-info-circle"></i>
                                            Kelola Siswa</a>
                                        <a href="{{route('walikelas.index',['id_angkatan'=>$angkatan->id])}}"
                                            class="btn btn-icon btn-left btn-info"><i class="fas fa-info-circle"></i>
                                            Wali Kelas</a>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@endsection