<?php

namespace App\Http\Controllers;

use App\Models\M_Angkatan;
use App\Models\M_kelas;
use App\Models\M_Tahunajaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AngkatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahunAktif = getTahunAktif();
        $data = DB::table('angkatan')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('angkatan.*', 'kelas.nama_kelas', 'tahunajaran.tahun', 'tahunajaran.semester')
            ->where('tahunajaran.id', $tahunAktif->id)
            ->get();

        return view('admin.angkatan')->with([
            'data' => $data,
            'tahunAktif' => $tahunAktif
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tahunAjaran = M_Tahunajaran::where('status', 'aktif')->get();
        $kelas = M_kelas::all();
        return view('admin.tambahangkatan')->with([
            'tahunAjaran' => $tahunAjaran,
            'kelas' => $kelas
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);
        $cek = M_Angkatan::where('id_tahunajaran', $data['id_tahunajaran'])->where('id_kelas', $data['id_kelas'])->first();
        if (!empty($cek)) {
            return redirect('/angkatan')->with(['error' => 'Data Angkatan Sudah Ada.']);
        }
        M_Angkatan::insert($data);
        return redirect('/angkatan')->with(['success' => 'Data Berhasil Di Tambahkan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tahunAjaran = M_Tahunajaran::all();
        $kelas = M_kelas::all();
        $data = M_Angkatan::findOrFail($id);

        return view('admin.showangkatan')->with([
            'data' => $data,
            'tahunAjaran' => $tahunAjaran,
            'kelas' => $kelas
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tahunAjaran = M_Tahunajaran::all();
        $kelas = M_kelas::all();
        $data = M_Angkatan::findOrFail($id);

        return view('admin.editangkatan')->with([
            'data' => $data,
            'tahunAjaran' => $tahunAjaran,
            'kelas' => $kelas
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = M_Angkatan::findOrFail($id);
        $data = $request->except(['_token']);
        $item->update($data);
        return redirect('/angkatan')->with(['success' => 'Data Berhasil Di Perbarui']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $item = M_Angkatan::findOrFail($id);
            $item->delete();
            return redirect('/angkatan')->with(['success' => 'Data Berhasil Dihapus']);
        } catch (\Throwable $th) {
            return redirect('/angkatan')->with(['error' => 'Data Angkatan Sudah Digunakan.']);
        }
    }
}
