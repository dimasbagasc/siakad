<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Cek_login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $roles)
    {
        if (!Auth::check()) {
            return redirect('login');
        }

        $user = Auth::user();

        if ($user->role == $roles) {
            return $next($request);
        }
        if ($user->role == 'admin') {
            return redirect()->intended('dashboardadmin')->with(['error_akses' => 'Maaf anda tidak memiliki akses...']);
        } elseif ($user->role == 'guru') {
            return redirect()->intended('dashboardguru')->with(['error_akses' => 'Maaf anda tidak memiliki akses...']);
        } elseif ($user->role == 'siswa') {
            return redirect()->intended('dashboardsiswa')->with(['error_akses' => 'Maaf anda tidak memiliki akses...']);
        } else {
            return redirect('login')->with(['error_akses', 'Maaf anda tidak memiliki akses...']);
        }
    }
}
