@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
  <br>
  <div class="section-header">
    <h1>Data Kelas</h1>
  </div>

  <div class="col-12 col-md-12 col-lg-12">
    <div class="card">
      <div class="card-header">
        <a href="{{route('kelas.create')}}" class="btn btn-primary fa fa-plus" style="position: absolute; right:25px;">
          Tambah Data</a>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped table-md datatable_table">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Kelas</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @php $no = 1; @endphp
              @foreach ($data as $datakelas)
              <tr>
                <td>{{$no++}}</td>
                <td>{{ $datakelas->nama_kelas }}</td>
                <td>
                  <form method="POST" action="{{route('kelas.destroy',['kela'=>$datakelas->id])}}">
                    @csrf
                    @method('DELETE')
                    <a href="{{route('kelas.edit',['kela'=>$datakelas->id])}}"
                      class="btn btn-icon btn-left btn-warning"><i class="far fa-edit"></i> Edit</a>
                    <button type="submit" class="btn btn-icon btn-left btn-danger"><i class="fas fa-times"></i>
                      Delete</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>
  </div>
</section>
@endsection