@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
  <br>
  <div class="section-header">
    <h1>Tambah Tahun Ajaran</h1>
  </div>

  <div class="card">
    <div class="card-body">
      <form action="{{route('tahunajaran.update',['tahunajaran'=>$data->id])}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label>Tahun Ajaran</label>
          <input type="text" name="tahun" class="form-control" placeholder="Tahun Ajaran" value="{{$data->tahun}}">
        </div>
        <div class="form-group">
          <label class="d-block" for="status">Semester</label>
          <select class="form-control select2_dropdown" name="semester" id="semester">
            <option></option>
            <option value="ganjil" {{ $data->semester == 'ganjil' ? 'selected' : '' }}>Ganjil</option>
            <option value="genap" {{ $data->semester == 'genap' ? 'selected' : '' }}>Genap</option>
          </select>
        </div>
        <div class="form-group">
          <label class="d-block" for="status">Status</label>
          <select class="form-control select2_dropdown" name="status" id="status" value="{{$data->status}}">
            <option></option>
            <option value="aktif" {{ $data->status == 'aktif' ? 'selected' : '' }}>Aktif</option>
            <option value="nonaktif" {{ $data->status == 'nonaktif' ? 'selected' : '' }}>Non Aktif</option>
          </select>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</section>

@endsection