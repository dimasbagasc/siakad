<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_Riwayatpekerjaan extends Model
{
    use HasFactory;

    protected $table = 'riwayat_pekerjaan';
    protected $fillable = [
        'id_guru',
        'instansi',
        'jabatan',
        'tgl_mulai',
        'tgl_selesai',
        'gaji_pokok',
        'nomor_sk',
        'tgl_sk',
        'pejabat_ttd'
    ];
}
