@extends('layout.master')
@extends('layout.sidebar')

@section('container')

<section class="section">
    <br>
    <div class="section-header">
        <h1>Edit Data Guru</h1>
    </div>

    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-biodata-tab" data-toggle="tab" href="#nav-biodata"
                            role="tab" aria-controls="nav-biodata" aria-selected="true">Biodata</a>
                </nav>
                <form method="POST" action="{{ route('guru.update', ['guru'=>$data->id]) }}"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-biodata" role="tabpanel"
                            aria-labelledby="nav-biodata-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class=" form-group">
                                        <label>NIK</label>
                                        <input type="text" name="nik" class="form-control" value="{{$data->nik}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>NIP</label>
                                        <input type="text" name="nip" class="form-control" value="{{$data->nip}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" name="nama" class="form-control" value="{{$data->nama}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tempat Lahir</label>
                                        <input type="text" name="tmp_lahir" class="form-control"
                                            value="{{$data->tmp_lahir}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <input type="date" name="tgl_lahir" class="form-control"
                                            value="{{$data->tgl_lahir}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block">Jenis Kelamin</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="jk" id="jk1"
                                                value="laki-laki" {{$data->jk == 'laki-laki' ? 'checked' : ''}}>
                                            <label class="form-check-label" for="jk1">
                                                Laki-laki
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="jk" id="jk2"
                                                value="perempuan" {{$data->jk == 'perempuan' ? 'checked' : ''}}>
                                            <label class="form-check-label" for="jk2">
                                                Perempuan
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Agama</label>
                                        <select name="agama" class="form-control">
                                            <option></option>
                                            <option value="islam" {{$data->agama == 'islam' ? 'selected' : ''}}>Islam
                                            </option>
                                            <option value="katholik" {{$data->agama == 'katholik' ? 'selected' :
                                                ''}}>Katholik</option>
                                            <option value="kristen" {{$data->agama == 'kristen' ? 'selected' :
                                                ''}}>Kristen</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nomor Handphone</label>
                                        <input type="text" name="no_hp" class="form-control" value="{{$data->no_hp}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="alamat">Alamat</label>
                                        <textarea name="alamat" class="form-control" id="alamat">
                                            {{$data->alamat}}
                                        </textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Keterangan Badan</label>
                                        <input type="text" name="ket_badan" class="form-control"
                                            value="{{$data->ket_badan}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Kegemaran / Hobi</label>
                                        <input type="text" name="hobi" class="form-control" value="{{$data->hobi}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select name="status" class="form-control">
                                            <option value="aktif" selected>Aktif</option>
                                            <option value="nonaktif">Non Aktif</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Foto</label>
                                        <input type="file" name="foto" class="form-control" value="{{$data->foto}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                </form>
            </div>

        </div>
</section>

@endsection