<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\M_nilai;
use App\Models\M_siswa;
use PDF;

class NilaisiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahunAktif = getTahunAktif();
        $siswa = M_siswa::where('id_user', Auth::user()->id)->first();
        $datanilai = DB::table('nilai')
            ->join('gurumapel', 'nilai.id_gurumapel', '=', 'gurumapel.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->join('rombel', 'nilai.id_rombel', '=', 'rombel.id')
            ->join('angkatan', 'rombel.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('mapel.matapelajaran', 'nilai.nuts', 'nilai.nuas')
            ->where('rombel.id_siswa', '=', $siswa->id)
            ->where('tahunajaran.id', $tahunAktif->id)
            ->get();

        // echo "<pre>";
        // print_r($datanilai);
        // exit;

        return view('siswa.nilaisiswa')->with([
            'nilai' => $datanilai,
            'tahunAktif' => $tahunAktif
        ]);
    }

    public function cetakpdf()
    {
        $tahunAktif = getTahunAktif();
        $siswa = M_siswa::where('id_user', Auth::user()->id)->first();
        $datasiswa = DB::table('rombel')
            ->join('siswa', 'rombel.id_siswa', '=', 'siswa.id')
            ->join('angkatan', 'rombel.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('siswa.*', 'kelas.nama_kelas', 'tahunajaran.tahun', 'tahunajaran.semester', 'rombel.id_angkatan')
            ->where('rombel.id_siswa', '=', $siswa->id)
            ->where('tahunajaran.id', $tahunAktif->id)
            ->first();

        $datawalikelas = DB::table('walikelas')
            ->join('angkatan', 'walikelas.id_angkatan', '=', 'angkatan.id')
            ->join('guru', 'walikelas.id_guru', '=', 'guru.id')
            ->select('guru.nama', 'guru.nip')
            ->where('walikelas.id_angkatan', '=', $datasiswa->id_angkatan) // id angkatan
            ->where('angkatan.id_tahunajaran', $tahunAktif->id) // tahun ajaran
            ->first();

        // echo '<pre>';
        // print_r($datawalikelas);
        // exit;

        $datanilai = DB::table('nilai')
            ->join('gurumapel', 'nilai.id_gurumapel', '=', 'gurumapel.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->join('rombel', 'nilai.id_rombel', '=', 'rombel.id')
            ->join('angkatan', 'rombel.id_angkatan', '=', 'angkatan.id')
            ->join('kelas', 'angkatan.id_kelas', '=', 'kelas.id')
            ->join('siswa', 'rombel.id_siswa', '=', 'siswa.id')
            ->join('tahunajaran', 'angkatan.id_tahunajaran', '=', 'tahunajaran.id')
            ->select('mapel.matapelajaran as matapelajaran', 'nilai.*', 'tahunajaran.tahun', 'tahunajaran.semester', 'kelas.nama_kelas')
            ->where('rombel.id_siswa', '=', $siswa->id)
            ->where('tahunajaran.id', $tahunAktif->id);

        // echo "<pre>";
        // print_r($datanilai->get()->toArray());
        // exit;

        $countpresensi = DB::table('presensi')
            ->selectRaw("COUNT(CASE WHEN presensi.status='hadir' THEN 1 END) AS hadir,COUNT(CASE WHEN presensi.status='alpha' THEN 1 END) AS alpha,COUNT(CASE WHEN presensi.status='ijin' THEN 1 END) AS ijin, mapel.matapelajaran as matapelajaran, guru.nama as nama")
            ->join('rombel', 'presensi.id_rombel', '=', 'rombel.id')
            ->join('siswa', 'rombel.id_siswa', '=', 'siswa.id')
            ->join('jadwal', 'presensi.id_jadwal', '=', 'jadwal.id')
            ->join('gurumapel', 'jadwal.id_guru', '=', 'gurumapel.id')
            ->join('guru', 'gurumapel.id_guru', '=', 'guru.id')
            ->join('mapel', 'gurumapel.id_mapel', '=', 'mapel.id')
            ->where('siswa.id', '=', $siswa->id)
            ->groupBy('mapel.matapelajaran', 'guru.nama')
            ->get();

        // echo "<pre>";
        // print_r($countpresensi);
        // exit;

        $datadeskripsi = DB::table('deskripsi')
            ->join('rombel', 'deskripsi.id_rombel', '=', 'rombel.id')
            ->join('siswa', 'rombel.id_siswa', '=', 'siswa.id')
            ->select('deskripsi.*')
            ->where('siswa.id', '=', $siswa->id)
            ->first();

        // echo '<pre>';
        // print_r(array_key_first($datanilai));
        // exit;

        $data = [
            'data' => $datanilai->get()->toArray(),
            'countdatanilai' => $datanilai->count(),
            'siswa' => $datasiswa,
            'countpresensi' => $countpresensi,
            'tahunAktif' => $tahunAktif,
            'deskripsi' => $datadeskripsi,
            'walikelas' => $datawalikelas

        ];

        $pdf = PDF::loadView('siswa.cetak', $data);

        return $pdf->stream('tesnamafile.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
